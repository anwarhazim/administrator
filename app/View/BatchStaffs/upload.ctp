<div class="content-wrapper">

<!-- Breadcrumb -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
            <li>Configuration</li>
			<li><a href="<?php echo $this->html->url('/BatchStaffs', true);?>">Batch Staff</a></li>
            <li class="active">Upload Batch Staff</li>
		</ul>
	</div>

</div>
<!-- /Breadcrumb -->
<!-- Search -->
<div class="panel panel-flat">
    <?php echo $this->Form->create('BatchStaff', array('class'=>'', 'novalidate'=>'novalidate', 'type'=>'file'));?>
    <div class="panel-body">
        <?php 
            echo $this->Session->flash(); 

            if(!empty($this->validationErrors['BatchStaff']))
            {
            ?>
                <div role="alert" class="alert alert-danger">
                        <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <?php
                            foreach ($this->validationErrors['BatchStaff'] as $errors) 
                            {
                                echo '<ul>';
                                foreach ($errors as $error) 
                                {
                                    echo '<li>'.h($error).'</li>';
                                }
                                echo '</ul>';
                            }
                        ?>
                </div>
            <?php
            }
        ?>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <label>Type <span class="text-danger">*</span></label>
                    <?php 
                        echo $this->Form->input('type_id', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=>false,
                            'options'=>$is_types,
                            'empty'=>'PLEASE SELECT...',
                            )
                        ); 
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <label>SAP File</label>
                    <?php 
                        echo $this->Form->input('attachment', array(
                            'class'=>'form-control file-styled',
                            'label'=> false,
                            'error'=>false,
                            'type'=>'file',
                            'multiple'=>'multiple',
                            'data-preview-file-type'=>'any',
                            'data-show-upload'=>false,
                            'showRemove'=>false,
                            'data-show-caption'=>false,
                            'maxFileSize'=>'2MB'
                            )
                        ); 
                    ?>
                    <span class="help-block">Accepted formats: xls 2Mb</span>
                </div>
            </div>
        </div>
        <fieldset class="content-group">
            <legend class="text-semibold">
                <i class="icon-file-text2 position-left"></i>
                Office Use Only
                <a class="control-arrow" data-toggle="collapse" data-target="#office">
                    <i class="icon-circle-down2"></i>
                </a>
            </legend>
            <div class="collapse in" id="office">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Name</label>
                        <?php 
                            echo $this->Form->input('name', array(
                                'class'=>'form-control',
                                'label'=>false,
                                'error'=>false,
                                'type'=>'text',
                                'disabled'=>'disabled',
                                'value'=>$detail['BatchStaff']['name']
                                )
                            ); 
                        ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Success</label>
                        <?php 
                            echo $this->Form->input('success', array(
                                'class'=>'form-control',
                                'label'=>false,
                                'error'=>false,
                                'type'=>'text',
                                'disabled'=>'disabled',
                                'value'=>$detail['BatchStaff']['success']
                                )
                            ); 
                        ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Fail</label>
                        <?php 
                            echo $this->Form->input('fail', array(
                                'class'=>'form-control',
                                'label'=>false,
                                'error'=>false,
                                'type'=>'text',
                                'disabled'=>'disabled',
                                'value'=>$detail['BatchStaff']['fail']
                                )
                            ); 
                        ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Total</label>
                        <?php 
                            echo $this->Form->input('total', array(
                                'class'=>'form-control',
                                'label'=>false,
                                'error'=>false,
                                'type'=>'text',
                                'disabled'=>'disabled',
                                'value'=>$detail['BatchStaff']['total']
                                )
                            ); 
                        ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Created At</label>
                        <?php 
                            echo $this->Form->input('created', array(
                                'class'=>'form-control',
                                'label'=>false,
                                'error'=>false,
                                'type'=>'text',
                                'disabled'=>'disabled',
                                'value'=>$detail['BatchStaff']['created']
                                )
                            ); 
                        ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Last Modified</label>
                        <?php 
                            echo $this->Form->input('modified', array(
                                'class'=>'form-control',
                                'label'=>false,
                                'error'=>false,
                                'type'=>'text',
                                'disabled'=>'disabled',
                                'value'=>$detail['BatchStaff']['modified']
                                )
                            ); 
                        ?>
                    </div>
                </div>
            </div>
        </fieldset>
        <div class="text-right">
            <button type="submit" class="btn btn-success legitRipple">
                Upload <i class="icon-floppy-disk position-right"></i>
            </button>
            <a class="btn btn-default position-right" href="<?php echo $this->Html->url('/BatchStaffs/upload/'.$key, true);?>">
                Reset <i class="icon-spinner11 position-right"></i>
            </a>
        </div>
    </div>
    <div class="panel-footer">
        <div class="heading-elements">
			<span class="heading-text">
				<a href="<?php echo $this->html->url('/BatchStaffs/index', true); ?>" class="btn btn-warning legitRipple">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>

</div>