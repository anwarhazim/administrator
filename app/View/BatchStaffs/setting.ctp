<!-- Breadcrumb -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
            <li>Configuration</li>
			<li><a href="<?php echo $this->html->url('/Roles', true);?>">Roles</a></li>
            <li class="active">Setting Role</li>
		</ul>
	</div>

</div>
<!-- /Breadcrumb -->
<!-- Content area -->
<div class="content">
	<!-- Main -->
	<div class="row">
		<div class="col-md-12">
			<!-- Basic layout-->
			<?php echo $this->Form->create('Role', array('class'=>'', 'novalidate'=>'novalidate'));?>
				<div class="panel panel-flat">
					<div class="panel-body">
						<div class="tabbable">
							<ul class="nav nav-tabs nav-tabs-highlight">
								<?php
									if(empty($project_selected))
									{
										$project_selected = 1;
									}

									foreach ($projects as $project_id => $project_name) 
									{
										$active = "";
										if($project_id == $project_selected )
										{
											$active = "active";
										}

										echo '<li class="'.$active.'"><a href="'. $this->html->url('/Roles/setting/'.$key.'/'.$project_id, true) .'">'.$project_name.'</a></li>';
									}
								?>
							</ul>

							<div class="tab-content">
								<div class="tab-pane fade in active">
									<?php 
										echo $this->Session->flash(); 

										if(!empty($this->validationErrors['Role']))
										{
										?>
											<div role="alert" class="alert alert-danger">
													<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
													<?php
														foreach ($this->validationErrors['Role'] as $errors) 
														{
															echo '<ul>';
															foreach ($errors as $error) 
															{
																echo '<li>'.h($error).'</li>';
															}
															echo '</ul>';
														}
													?>
											</div>
										<?php
										}
									?>
									<fieldset class="content-group">
										<legend class="text-bold">Assign <?php echo $detail['Role']['name']; ?> to Module</legend>
										<input type="hidden" id="jsfields" name="jsfields" value="<?php echo $setList ?>" />
										<div id="jstree_checkbox" class="push-down-20">
											<?php echo $tree ?>
										</div>
									</fieldset>
									<div class="text-right">
										<button type="submit" class="btn btn-success">
											Save <i class="icon-floppy-disk position-right"></i>
										</button>
										<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Roles/setting/'.$key.'/'.$project_id, true);?>">
											Reset <i class="icon-spinner11 position-right"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<div class="heading-elements">
							<span class="heading-text">
								<a class="btn btn-warning" href="<?php echo $this->Html->url('/Roles', true);?>">
									Back <i class="icon-arrow-left13 position-right"></i>
								</a>
							</span>
						</div>
					</div>
				</div>
			<?php echo $this->Form->end(); ?>
			<!-- /basic layout -->
		</div>
	</div>
	<!-- /Main -->
</div>
<!-- /Content area -->