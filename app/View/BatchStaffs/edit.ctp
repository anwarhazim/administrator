<!-- Breadcrumb -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
            <li>Configuration</li>
			<li><a href="<?php echo $this->html->url('/BatchStaffs', true);?>">Batch Staff</a></li>
            <li class="active">Edit Batch Staff</li>
		</ul>
	</div>

</div>
<!-- /Breadcrumb -->
<!-- Content area -->
<div class="content">
	<!-- Main -->
	<div class="row">
		<div class="col-md-12">
			<!-- Basic layout-->
			<?php echo $this->Form->create('BatchStaff', array('class'=>'', 'novalidate'=>'novalidate'));?>
				<div class="panel panel-flat">
					<div class="panel-body">
						<?php 
							echo $this->Session->flash(); 

							if(!empty($this->validationErrors['BatchStaff']))
							{
							?>
								<div role="alert" class="alert alert-danger">
										<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
										<?php
											foreach ($this->validationErrors['BatchStaff'] as $errors) 
											{
												echo '<ul>';
												foreach ($errors as $error) 
												{
													echo '<li>'.h($error).'</li>';
												}
												echo '</ul>';
											}
										?>
								</div>
							<?php
							}
						?>
						<fieldset class="content-group">
							<div class="col-md-12">
								<div class="form-group">
									<label>Name <span class="text-danger">*</span></label>
									<?php 
										echo $this->Form->input('name', array(
											'class'=>'form-control',
											'label'=>false,
											'error'=>false,
											'type'=>'text',
											'placeholder'=>'Name',
											'disabled'=>$disabled
											)
										); 
									?>
								</div>
							</div>
						</fieldset>
						<div class="text-right">
							<button type="submit" class="btn btn-success">
								Save <i class="icon-floppy-disk position-right"></i>
							</button>
							<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/BatchStaffs/edit/'.$key, true);?>">
								Reset <i class="icon-spinner11 position-right"></i>
							</a>
						</div>
					</div>
					<div class="panel-footer">
						<div class="heading-elements">
							<span class="heading-text">
								<a class="btn btn-warning" href="<?php echo $this->Html->url('/BatchStaffs', true);?>">
									Back <i class="icon-arrow-left13 position-right"></i>
								</a>
							</span>
						</div>
					</div>
				</div>
			<?php echo $this->Form->end(); ?>
			<!-- /basic layout -->
		</div>
	</div>
	<!-- /Main -->
</div>
<!-- /Content area -->