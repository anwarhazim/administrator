<!-- Breadcrumb -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
            <li>Configuration</li>
			<li><a href="<?php echo $this->html->url('/BatchStaffs', true);?>">Batch Staff</a></li>
            <li class="active">Listing</li>
		</ul>
	</div>

</div>
<!-- /Breadcrumb -->
<!-- Content area -->
<div class="content">
	<!-- Search -->
	<div class="row">
		<div class="col-md-12">
			<!-- Basic layout-->
			<?php echo $this->Form->create('InStaff', array('class'=>'', 'novalidate'=>'novalidate'));?>
				<div class="panel panel-flat">
					<div class="panel-body">
						<?php 
							echo $this->Session->flash(); 

							if(!empty($this->validationErrors['InStaff']))
							{
							?>
								<div role="alert" class="alert alert-danger">
										<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
										<?php
											foreach ($this->validationErrors['InStaff'] as $errors) 
											{
												echo '<ul>';
												foreach ($errors as $error) 
												{
													echo '<li>'.h($error).'</li>';
												}
												echo '</ul>';
											}
										?>
								</div>
							<?php
							}

							echo $this->Form->input('key', array(
								'type'=>'hidden',
								'label'=> false,
								'error'=> false,
								'value'=>$key
								)
							);
						?>
						<fieldset class="content-group">
							<div class="col-md-12">
								<div class="form-group">
									<label>Search</label>
									<?php 
										echo $this->Form->input('search', array(
											'class'=>'form-control',
											'label'=>false,
											'error'=>false,
											'type'=>'text',
											'placeholder'=>'Name'
											)
										); 
									?>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Start Date</label>
									<?php 
										echo $this->Form->input('start_date', array(
											'class'=>'form-control start_date', 
											'label'=>false,
											'error'=>false,
											'placeholder'=>'dd-mm-yyyy'
											)
										); 
									?>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>End Date</label>
									<?php 
										echo $this->Form->input('end_date', array(
											'class'=>'form-control end_date', 
											'label'=>false,
											'error'=>false,
											'placeholder'=>'dd-mm-yyyy'
											)
										); 
									?>
								</div>
							</div>
						</fieldset>
						<div class="text-right">
							<button type="submit" class="btn btn-primary">
								Search <i class="icon-search4 position-right"></i>
							</button>
							<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/BatchStaffs/listing/key:'.$key, true);?>">
								Reset <i class="icon-spinner11 position-right"></i>
							</a>
						</div>
					</div>
				</div>
			<?php echo $this->Form->end(); ?>
			<!-- /basic layout -->
		</div>
	</div>
	<!-- /Search -->
	<!-- Result -->
	<div class="row">
		<div class="col-md-12">
			<!-- Basic layout-->
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h5 class="panel-title">List of Staff in Batch Staff</h5>
				</div>
				<?php
					$counter = $this->Paginator->counter(array('format' =>'{:start}'));
				?>
				<div class="table-responsive">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="5%" class="text-center">No.</th>
								<th>Complete Name</th>
								<th width="9%" class="text-center">Employee No</th>
								<th width="9%" class="text-center">Execute Status</th>
								<th width="8%" class="text-center">Status</th>
								<th width="9%" class="text-center">Last Executed</th>
								<th width="20%">Executed By</th>
								<th width="15%">Note</th>
								<th width="10%" class="text-center">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
								if($details)
								{
									foreach ($details as $detail) 
									{
							?>
								<tr>
									<td style="vertical-align:top;" class="text-center"><?php echo $counter;?></td>
									<td style="vertical-align:top;"><?php echo $detail['InStaff']['complete_name'];?></td>
									<td style="vertical-align:top;" class="text-center"><?php echo $detail['InStaff']['employee_no'];?></td>
									<td style="vertical-align:top;" class="text-center"><?php echo $detail['InStaff']['status_flag'];?></td>
									<td style="vertical-align:top;" class="text-center"><?php echo $detail['InStaff']['status_error'];?></td>
									<td style="vertical-align:top;" class="text-center"><?php echo $detail['InStaff']['executed'];?></td>
									<td style="vertical-align:top;">
										<?php
											if(!empty($detail['ExecuteBy']['complete_name']))
											{ 
												echo $detail['ExecuteBy']['complete_name'];
											}
											else
											{
												echo "-";
											}
										?>
									</td>
									<td style="vertical-align:top;"><?php echo $detail['InStaff']['note'];?></td>
									<td style="vertical-align:top;" class="text-center">
										<a alt="view" href="<?php echo $this->Html->url('/BatchStaffs/view_error/'.$key.'/'.$detail['InStaff']['key']); ?>" class="btn bg-grey-300 btn-icon btn-rounded legitRipple" title="View details">
											<spam class="icon-file-text2">
											</spam>
										</a>

										<?php
											echo $this->html->link(
												'<spam class="icon-database-upload"></spam>', 
												array('controller' => 'BatchStaffs', 'action' => 'push', $detail['InStaff']['id']),
												array('class' => 'btn bg-grey-300 btn-icon btn-rounded legitRipple', 'title' => 'Push record', 'confirm' => 'Are you sure you want to push this record?
												', 'escape' => false)
											);
										?>
									</td>
								</tr>
							<?php
									$counter++;
									}
								}
								else
								{
							?>
								<tr>
									<td colspan="9">
										No data...
									</td>
								</tr>
							<?php
								}
							?>
						</tbody>
					</table>
				</div>
				<div class="panel-body">
					<span class="pull-right">
						<?php echo $this->Paginator->counter(array(
															'format' => 'Page {:page} from {:pages}, show {:current} record from
																	{:count} total, start {:start}, end at {:end}'
															));
						?>
					</span>
				</div>
				<div class="panel-footer">
					<div class="heading-elements">
						<span class="heading-text">
							<a class="btn btn-warning" href="<?php echo $this->Html->url('/BatchStaffs', true);?>">
								Back <i class="icon-arrow-left13 position-right"></i>
							</a>
						</span>
						<ul class="pagination  pull-right">
							<?php 
								echo $this->Paginator->prev('< ' . __('<'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
								echo $this->Paginator->numbers(array('currentTag'=> 'a', 'currentClass' => 'active', 'tag' => 'li', 'separator' => false));
								echo $this->Paginator->next(__('>') . ' >', array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
							?>
						</ul>
					</div>
				</div>
			</div>
			<!-- /basic layout -->
		</div>
	</div>
	<!-- /Result -->
</div>
<!-- /Content area -->