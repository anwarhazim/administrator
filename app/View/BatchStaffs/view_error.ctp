<!-- Breadcrumb -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
            <li>Configuration</li>
			<li><a href="<?php echo $this->html->url('/BatchStaffs', true);?>">Batch Staff</a></li>
            <li class="active">View Batch Staff</li>
		</ul>
	</div>

</div>
<!-- /Breadcrumb -->
<!-- Content area -->
<div class="content">
	<!-- Main -->
	<div class="row">
		<div class="col-md-12">
			<!-- Basic layout-->
			<?php echo $this->Form->create('BatchStaff', array('class'=>'', 'novalidate'=>'novalidate'));?>
				<div class="panel panel-flat">
					<div class="panel-body">
						<?php 
							echo $this->Session->flash(); 

							if(!empty($this->validationErrors['BatchStaff']))
							{
							?>
								<div role="alert" class="alert alert-danger">
										<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
										<?php
											foreach ($this->validationErrors['BatchStaff'] as $errors) 
											{
												echo '<ul>';
												foreach ($errors as $error) 
												{
													echo '<li>'.h($error).'</li>';
												}
												echo '</ul>';
											}
										?>
								</div>
							<?php
							}
						?>
						<fieldset class="content-group">
							<legend class="text-semibold">
								<i class="icon-file-text2 position-left"></i>
								Details
								<a class="control-arrow" data-toggle="collapse" data-target="#details">
									<i class="icon-circle-down2"></i>
								</a>
							</legend>
							<div class="collapse in" id="details">
								<div class="col-md-12">
									<div class="form-group">
										<div role="alert" class="alert alert-danger">
										<strong>Error</strong><br/>
											<?php
												echo $inStaff['InStaff']['note'];
											?>
										</div>
									</div>
								</div>
								<div class="col-md-9">
									<div class="form-group">
										<label>Name</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['complete_name']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Employee No</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['employee_no']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Personal Area</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['personal_area']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Personal Subarea</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['personal_subarea']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Employee Group</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['employee_group']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Employee Subgroup</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['employee_subgroup']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Payroll Area</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['payroll_area']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Employment Status</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['employment_status']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-4">
										<div class="form-group">
											<label>Entry Date</label>
											<?php 
												echo $this->Form->input('name', array(
													'class'=>'form-control',
													'label'=>false,
													'error'=>false,
													'type'=>'text',
													'placeholder'=>'Name',
													'disabled'=>$disabled,
													'value'=>$inStaff['InStaff']['entry_date']
													)
												); 
											?>
										</div>
									</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Leaving Date</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['leaving_date']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Seniority Date</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['seniority_date']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-9">
									<div class="form-group">
										<label>Organizational Unit</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['organizational_unit']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Organizational Unit ID</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['organizational_unit_id']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-9">
									<div class="form-group">
										<label>Cost Center</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['cost_center']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Cost Center Code</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['cost_center_code']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-9">
									<div class="form-group">
										<label>Position</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['position']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Position ID</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['position_id']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-9">
									<div class="form-group">
										<label>Date of Birth</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['date_of_birth']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Age</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['age']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Gender</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['gender']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Ethnic</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['ethnic']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-9">
									<div class="form-group">
										<label>group</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['group']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>group code</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['group_code']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-9">
									<div class="form-group">
										<label>Division</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['division']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Division ID</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['div_id']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-9">
									<div class="form-group">
										<label>Division</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['division']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Division ID</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['div_id']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-9">
									<div class="form-group">
										<label>Department</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['department']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Department ID</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['dept_id']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-9">
									<div class="form-group">
										<label>Section</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['section']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Section ID</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['sec_id']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-9">
									<div class="form-group">
										<label>Unit</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['unit']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Unit ID</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['unit_id']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-9">
									<div class="form-group">
										<label>Subunit</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['subunit']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Subunit ID</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['subunit_id']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Payscale Group</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['payscale_group']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Payscale Level</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['payscale_level']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Marital Status Code</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['marital_status_code']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>Reporting to</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['reporting_to']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>IC No</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['ic_no']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Date of Married</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['date_of_married']
												)
											); 
										?>
									</div>
								</div>
							</div>
						</fieldset>
						<fieldset class="content-group">
							<legend class="text-semibold">
								<i class="icon-file-text2 position-left"></i>
								Office Use Only
								<a class="control-arrow" data-toggle="collapse" data-target="#office">
									<i class="icon-circle-down2"></i>
								</a>
							</legend>
							<div class="collapse in" id="office">
								<div class="col-md-6">
									<div class="form-group">
										<label>Created At</label>
										<?php 
											echo $this->Form->input('created', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['created']
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Last Modified</label>
										<?php 
											echo $this->Form->input('modified', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled,
												'value'=>$inStaff['InStaff']['modified']
												)
											); 
										?>
									</div>
								</div>
							</div>
						</fieldset>
					</div>
					<div class="panel-footer">
						<div class="heading-elements">
							<span class="heading-text">
								<a class="btn btn-warning" href="<?php echo $this->Html->url('/BatchStaffs', true);?>">
									Back <i class="icon-arrow-left13 position-right"></i>
								</a>
							</span>
						</div>
					</div>
				</div>
			<?php echo $this->Form->end(); ?>
			<!-- /basic layout -->
		</div>
	</div>
	<!-- /Main -->
</div>
<!-- /Content area -->