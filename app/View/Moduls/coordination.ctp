<div class="content-wrapper">

<!-- Breadcrumb -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
            <li>Configuration</li>
            <li><a href="<?php echo $this->html->url('/Moduls/index', true);?>">Moduls</a></li>
			<li class="active">Coordination Modul</li>
		</ul>
	</div>

</div>
<!-- /Breadcrumb -->
<div class="panel panel-flat">
    <?php echo $this->Form->create('Modul', array('class'=>'', 'novalidate'=>'novalidate', 'type'=>'file'));?>
    <div class="panel-heading">
        <h5 class="panel-title">Modul Information </h5>
    </div>
    <div class="panel-body">
        <?php 
            echo $this->Session->flash(); 

            if(!empty($this->validationErrors['Modul']))
            {
            ?>
                <div role="alert" class="alert alert-danger">
                        <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <?php
                            foreach ($this->validationErrors['Modul'] as $errors) 
                            {
                                echo '<ul>';
                                foreach ($errors as $error) 
                                {
                                    echo '<li>'.h($error).'</li>';
                                }
                                echo '</ul>';
                            }
                        ?>
                </div>
            <?php
            }
        ?>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <div class="dd" id="nestable">
                        <ol class="dd-list">
                            <?php echo $nestable ?>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a href="<?php echo $this->html->url('/Moduls/index', true); ?>" class="btn btn-warning legitRipple">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
    <?php echo $this->Form->end(); ?>
</div>

</div>