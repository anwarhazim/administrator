<!-- Breadcrumb -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
            <li>Configuration</li>
            <li><a href="<?php echo $this->html->url('/Moduls/index', true);?>">Moduls</a></li>
            <li><a href="<?php echo $this->html->url('/Moduls/listing/key:'.$project_key, true);?>">List of Modul</a></li>
            <li class="active">View Modul</li>
		</ul>
	</div>

</div>
<!-- /Breadcrumb -->
<div class="panel panel-flat">
    <?php echo $this->Form->create('Modul', array('class'=>'', 'novalidate'=>'novalidate', 'type'=>'file'));?>
    <div class="panel-heading">
        <h5 class="panel-title">Modul Information </h5>
    </div>
    <div class="panel-body">
        <?php 
            echo $this->Session->flash(); 

            if(!empty($this->validationErrors['Modul']))
            {
            ?>
                <div role="alert" class="alert alert-danger">
                        <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <?php
                            foreach ($this->validationErrors['Modul'] as $errors) 
                            {
                                echo '<ul>';
                                foreach ($errors as $error) 
                                {
                                    echo '<li>'.h($error).'</li>';
                                }
                                echo '</ul>';
                            }
                        ?>
                </div>
            <?php
            }
        ?>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <label>Name </label>
                    <?php 
                        echo $this->Form->input('name', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text',
                            'disabled'=>'disabled'
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label>Path </label>
                    <?php 
                        echo $this->Form->input('path', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text',
                            'disabled'=>'disabled'
                            )
                        );
                    ?>
                </div>
                <div class="col-md-6">
                    <label>Icon </label>
                    <?php 
                        echo $this->Form->input('icon', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text',
                            'disabled'=>'disabled'
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label>Type </label>
                    <?php 
                        echo $this->Form->input('is_type', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'options'=>$is_type,
                            'empty'=>'PLEASE SELECT...',
                            'disabled'=>'disabled'
                            )
                        );
                    ?>
                </div>
                <div class="col-md-6">
                    <label>Dropdown? </label>
                    <?php 
                        echo $this->Form->input('is_dropdown', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'options'=>$is_dropdown,
                            'empty'=>'PLEASE SELECT...',
                            'disabled'=>'disabled'
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label>Display in the sidebar? </label>
                    <?php 
                        echo $this->Form->input('is_nav', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'options'=>$is_nav,
                            'empty'=>'PLEASE SELECT...',
                            'disabled'=>'disabled'
                            )
                        );
                    ?>
                </div>
                <div class="col-md-6">
                    <label>Display all the navigation path? </label>
                    <?php 
                        echo $this->Form->input('is_full', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'options'=>$is_full,
                            'empty'=>'PLEASE SELECT...',
                            'disabled'=>'disabled'
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a href="<?php echo $this->html->url('/Moduls/listing/key:'.$project_key, true);?>" class="btn btn-warning legitRipple">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
    <?php echo $this->Form->end(); ?>
</div>

</div>