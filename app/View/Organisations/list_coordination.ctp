<div class="content-wrapper">

<!-- Breadcrumb -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
            <li>Configuration</li>
            <li><a href="<?php echo $this->html->url('/Organisations', true);?>">Organisations</a></li>
            <li><a href="<?php echo $this->Html->url('/Organisations/coordination/batchkey:'.$batchkey, true);?>">Coordination Organisation Batch</a></li>
            <li class="active">Coordination Organisation</li>
		</ul>
	</div>

</div>
<!-- /Breadcrumb -->
<!-- Body -->
<div class="panel panel-flat">
    <?php echo $this->Form->create('Organisation', array('class'=>'', 'novalidate'=>'novalidate'));?>
    <div class="panel-body">
        <?php 
            echo $this->Session->flash(); 

            if(!empty($this->validationErrors['Organisation']))
            {
            ?>
                <div role="alert" class="alert alert-danger">
                        <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <?php
                            foreach ($this->validationErrors['Organisation'] as $errors) 
                            {
                                echo '<ul>';
                                foreach ($errors as $error) 
                                {
                                    echo '<li>'.h($error).'</li>';
                                }
                                echo '</ul>';
                            }
                        ?>
                </div>
            <?php
            }
        ?>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                   <div id="chart-container"></div>
                   <ul id='organisation_list' style='display:none;'>
                        <?php echo $str_orgs; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer">
            <div class="heading-elements">
                <span class="heading-text">
                    <a class="btn btn-warning" href="<?php echo $this->Html->url('/Organisations/coordination/batchkey:'.$batchkey, true);?>">
                        Back <i class="icon-arrow-left13 position-right"></i>
                    </a>
                </span>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<!-- /Body -->

</div>