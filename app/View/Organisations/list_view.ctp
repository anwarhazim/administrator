<div class="content-wrapper">

<!-- Breadcrumb -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
            <li>Configuration</li>
            <li><a href="<?php echo $this->html->url('/Organisations', true);?>">Organisations</a></li>
            <li><?php echo $detail['BatchOrganisation']['name']; ?></li>
            <li><a href="<?php echo $this->html->url('/Organisations/lists/batchkey:'.$batchkey, true);?>">List of Organisation</a></li>
            <li class="active">View Organisation</li>
		</ul>
	</div>

</div>
<!-- /Breadcrumb -->
<!-- Body -->
<div class="panel panel-flat">
    <?php echo $this->Form->create('Organisation', array('class'=>'', 'novalidate'=>'novalidate'));?>
    <div class="panel-body">
        <?php 
            echo $this->Session->flash(); 

            if(!empty($this->validationErrors['Organisation']))
            {
            ?>
                <div role="alert" class="alert alert-danger">
                        <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <?php
                            foreach ($this->validationErrors['Organisation'] as $errors) 
                            {
                                echo '<ul>';
                                foreach ($errors as $error) 
                                {
                                    echo '<li>'.h($error).'</li>';
                                }
                                echo '</ul>';
                            }
                        ?>
                </div>
            <?php
            }
        ?>
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label>Name</label>
                    <?php 
                        echo $this->Form->input('name', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text',
                            'disabled'=>$disabled
                            )
                        ); 
                    ?>
                </div>
                <div class="col-md-6">
                    <label>Code</label>
                    <?php 
                        echo $this->Form->input('code', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text',
                            'disabled'=>$disabled
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <label>Category</label>
                    <?php 
                        echo $this->Form->input('organisation_category_id', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=>false,
                            'options'=>$organisationcategories,
                            'empty'=>'PLEASE SELECT...',
                            'disabled'=>$disabled
                            )
                        ); 
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <label>Note</label>
                    <?php 
                        echo $this->Form->input('note', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'textarea',
                            'disabled'=>$disabled
                            )
                        ); 
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <label>Status</label>
                    <?php 
                        if(!empty($detail['Organisation']['is_active']))
                        {
                            echo '<p>'.$is_actives[$detail['Organisation']['is_active']].'</p>';
                        }
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label>Created By</label>
                    <?php 
                        echo $this->Form->input('CreatedBy.complete_name', array(
                            'class'=>'form-control', 
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text',
                            'disabled'=>$disabled
                            )
                        ); 
                    ?>
                </div>
                <div class="col-md-6">
                    <label>Last modified By</label>
                    <?php 
                        echo $this->Form->input('ModifiedBy.complete_name', array(
                            'class'=>'form-control', 
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text',
                            'disabled'=>$disabled
                            )
                        ); 
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label>Created At</label>
                    <?php 
                        echo $this->Form->input('created', array(
                            'class'=>'form-control', 
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text',
                            'disabled'=>$disabled
                            )
                        ); 
                    ?>
                </div>
                <div class="col-md-6">
                    <label>Last modified At</label>
                    <?php 
                        echo $this->Form->input('modified', array(
                            'class'=>'form-control', 
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text',
                            'disabled'=>$disabled
                            )
                        ); 
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <div class="heading-elements">
            <span class="heading-text">
                <a class="btn btn-warning" href="<?php echo $this->html->url('/Organisations/lists/batchkey:'.$batchkey, true);?>">
                    Back <i class="icon-arrow-left13 position-right"></i>
                </a>
			</span>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<!-- /Body -->

</div>