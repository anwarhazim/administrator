<div class="content-wrapper">

<!-- Breadcrumb -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
        <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
            <li>Configuration</li>
            <li><a href="<?php echo $this->html->url('/Organisations', true);?>">Organisations</a></li>
            <li><?php echo $detail['BatchOrganisation']['name']; ?></li>
            <li><a href="<?php echo $this->html->url('/Organisations/lists/batchkey:'.$batchkey, true);?>">List of Organisation</a></li>
            <li class="active">Edit Organisation</li>
		</ul>
	</div>

</div>
<!-- /Breadcrumb -->
<!-- Body -->
<div class="panel panel-flat">
    <?php echo $this->Form->create('Organisation', array('class'=>'', 'novalidate'=>'novalidate'));?>
    <div class="panel-body">
        <?php 
            echo $this->Session->flash(); 

            if(!empty($this->validationErrors['Organisation']))
            {
            ?>
                <div role="alert" class="alert alert-danger">
                        <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <?php
                            foreach ($this->validationErrors['Organisation'] as $errors) 
                            {
                                echo '<ul>';
                                foreach ($errors as $error) 
                                {
                                    echo '<li>'.h($error).'</li>';
                                }
                                echo '</ul>';
                            }
                        ?>
                </div>
            <?php
            }
        ?>
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label>Name <span class="text-danger">*</span></label>
                    <?php 
                        echo $this->Form->input('name', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text'
                            )
                        ); 
                    ?>
                </div>
                <div class="col-md-6">
                    <label>Code <span class="text-danger">*</span></label>
                    <?php 
                        echo $this->Form->input('code', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text',
                            'maxlength'=>8
                            )
                        ); 
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>Batch of Organisation</label>
                        <?php 
                            echo $this->Form->input('name', array(
                                'class'=>'form-control',
                                'label'=> false,
                                'error'=> false,
                                'type'=>'text',
                                'disabled'=>'disabled',
                                'value'=>$detail['BatchOrganisation']['name']
                                )
                            ); 
                        ?>
                    </div>
                    <div class="col-md-6">
                        <label>Category <span class="text-danger">*</span></label>
                        <?php 
                            echo $this->Form->input('organisation_category_id', array(
                                'class'=>'form-control',
                                'label'=> false,
                                'error'=>false,
                                'options'=>$organisationcategories,
                                'empty'=>'PLEASE SELECT...',
                                )
                            ); 
                        ?>
                    </div>
                </div>
            </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <label>Note</label>
                    <?php 
                        echo $this->Form->input('note', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'textarea'
                            )
                        ); 
                    ?>
                </div>
            </div>
        </div>
        <div class="text-right">
            <button type="submit" class="btn btn-success legitRipple">
                Save <i class="icon-floppy-disk position-right"></i>
            </button>
            <a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Organisations/list_edit/'.$batchkey.'/'.$key, true);?>">
                Reset <i class="icon-spinner11 position-right"></i>
            </a>
        </div>
    </div>
    <div class="panel-footer">
        <div class="heading-elements">
            <span class="heading-text">
                <a class="btn btn-warning" href="<?php echo $this->html->url('/Organisations/lists/batchkey:'.$batchkey, true);?>">
                    Back <i class="icon-arrow-left13 position-right"></i>
                </a>
			</span>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<!-- /Body -->

</div>