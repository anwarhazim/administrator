<div class="content-wrapper">

<!-- Breadcrumb -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
            <li>Configuration</li>
            <li><a href="<?php echo $this->html->url('/Organisations', true);?>">Organisations</a></li>
            <li class="active">Coordination Organisation Batch</li>
		</ul>
	</div>

</div>
<!-- /Breadcrumb -->
<!-- Search -->
<div class="panel panel-flat">
    <?php echo $this->Form->create('Organisation', array('class'=>'', 'novalidate'=>'novalidate'));?>
    <div class="panel-body">
        <?php 
            echo $this->Session->flash(); 

            if(!empty($this->validationErrors['Organisation']))
            {
            ?>
                <div role="alert" class="alert alert-danger">
                        <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <?php
                            foreach ($this->validationErrors['Organisation'] as $errors) 
                            {
                                echo '<ul>';
                                foreach ($errors as $error) 
                                {
                                    echo '<li>'.h($error).'</li>';
                                }
                                echo '</ul>';
                            }
                        ?>
                </div>
            <?php
            }

            echo $this->Form->input('batchkey', array(
                'type'=>'hidden',
                'label'=> false,
                'error'=> false,
                'value'=>$batchkey
                )
            );
        ?>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <label>Search</label>
                    <?php 
                        echo $this->Form->input('search', array(
                            'class'=>'form-control',
                            'placeholder'=>'Name/ Code', 
                            'label'=> false,
                            'error'=> false,
                            )
                        ); 
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label>Start Date</label>
                    <?php 
                        echo $this->Form->input('start_date', array(
                            'class'=>'form-control start_date', 
                            'label'=> false,
                            'error'=> false,
                            )
                        ); 
                    ?>
                </div>
                <div class="col-md-6">
                    <label>End Date</label>
                    <?php 
                        echo $this->Form->input('end_date', array(
                            'class'=>'form-control end_date', 
                            'label'=> false,
                            'error'=> false,
                            )
                        ); 
                    ?>
                </div>
            </div>
        </div>
        <div class="text-right">
            <button type="submit" class="btn btn-primary">
                Find <i class="icon-search4 position-right"></i>
            </button>
            <a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Organisations/coordination/batchkey:'.$batchkey, true);?>">
                Reset <i class="icon-spinner11 position-right"></i>
            </a>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<!-- /Search -->
<!-- Result -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h6 class="panel-title">List of Organisation</h6>
    </div>
    <div class="panel-body">
        <ul class="pagination pull-right">
            <?php 
                echo $this->Paginator->prev('< ' . __('<'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('currentTag'=> 'a', 'currentClass' => 'active', 'tag' => 'li', 'separator' => false));
                echo $this->Paginator->next(__('>') . ' >', array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
            ?>
        </ul>
        <br/>
        <br/>
        <span class="pull-right">
            <?php echo $this->Paginator->counter(array(
                                                'format' => 'Page {:page} from {:pages}, show {:current} record from
                                                        {:count} total, start {:start}, end at {:end}'
                                                ));
            ?>
        </span>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th width="5%" class="text-center">No.</th>
                    <th width="">Name</th>
                    <th width="15%" class="text-center">Code</th>
                    <th width="15%" class="text-center">Last Modified</th>
                    <th width="15%" class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $counter = $this->Paginator->counter(array('format' =>'{:start}'));
                    
                    if(!empty($details))
                    {
                        foreach ($details as $detail) 
						{
                ?>
                    <tr>
                        <td class="text-center">
                            <?php echo $counter; ?>
                        </td>
                        <td>
                            <?php echo $detail['Organisation']['name'] ?>
                        </td>
                        <td class="text-center">
                            <?php echo $detail['Organisation']['code'] ?>
                        </td>
                        </td>
                        <td class="text-center">
                            <?php echo $detail['Organisation']['modified'] ?>
                        </td>
                        <td class="text-center">
                            <a href="<?php echo $this->Html->url('/Organisations/list_coordination/'.$batchkey.'/'.$detail['Organisation']['id']);?>" class="btn bg-grey-300 btn-icon btn-rounded legitRipple" title="Coordination record">
                                <spam class="icon-tree7">
                                </spam>
                            </a>
                            <a href="<?php echo $this->Html->url('/Organisations/setup_coordination/'.$batchkey.'/'.$detail['Organisation']['id']);?>" class="btn bg-grey-300 btn-icon btn-rounded legitRipple" title="Coordination record">
                                <spam class="icon-menu4">
                                </spam>
                            </a>
                        </td>
                    </tr>
                <?php
                        $counter++;
                        }
                    }
                    else
                    {
                ?>
                    <tr>
                        <td colspan="5">No Organisation</td>
                    </tr>
                <?php
                    }
                ?>
            </tbody>
        </table>
    </div>
    <div class="panel-body">
        <span class="pull-right">
            <?php echo $this->Paginator->counter(array(
                                                'format' => 'Page {:page} from {:pages}, show {:current} record from
                                                        {:count} total, start {:start}, end at {:end}'
                                                ));
            ?>
        </span>
        <br/>
        <ul class="pagination pull-right">
            <?php 
                echo $this->Paginator->prev('< ' . __('<'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('currentTag'=> 'a', 'currentClass' => 'active', 'tag' => 'li', 'separator' => false));
                echo $this->Paginator->next(__('>') . ' >', array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
            ?>
        </ul>
    </div>
    <div class="panel-footer">
        <div class="heading-elements">
            <span class="heading-text">
                <a class="btn btn-warning" href="<?php echo $this->Html->url('/Organisations', true);?>">
                    Back <i class="icon-arrow-left13 position-right"></i>
                </a>
			</span>
        </div>
    </div>
</div>
<!-- Result -->

</div>