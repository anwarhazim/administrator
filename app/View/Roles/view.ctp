<!-- Breadcrumb -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
            <li>Configuration</li>
			<li><a href="<?php echo $this->html->url('/Roles', true);?>">Roles</a></li>
            <li class="active">View Role</li>
		</ul>
	</div>

</div>
<!-- /Breadcrumb -->
<!-- Content area -->
<div class="content">
	<!-- Main -->
	<div class="row">
		<div class="col-md-12">
			<!-- Basic layout-->
			<?php echo $this->Form->create('Role', array('class'=>'', 'novalidate'=>'novalidate'));?>
				<div class="panel panel-flat">
					<div class="panel-body">
						<?php 
							echo $this->Session->flash(); 

							if(!empty($this->validationErrors['Role']))
							{
							?>
								<div role="alert" class="alert alert-danger">
										<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
										<?php
											foreach ($this->validationErrors['Role'] as $errors) 
											{
												echo '<ul>';
												foreach ($errors as $error) 
												{
													echo '<li>'.h($error).'</li>';
												}
												echo '</ul>';
											}
										?>
								</div>
							<?php
							}
						?>
						<fieldset class="content-group">
							<legend class="text-semibold">
								<i class="icon-file-text2 position-left"></i>
								Details
								<a class="control-arrow" data-toggle="collapse" data-target="#details">
									<i class="icon-circle-down2"></i>
								</a>
							</legend>
							<div class="collapse in" id="details">
								<div class="col-md-12">
									<div class="form-group">
										<label>Name</label>
										<?php 
											echo $this->Form->input('name', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'placeholder'=>'Name',
												'disabled'=>$disabled
												)
											); 
										?>
									</div>
								</div>
							</div>
						</fieldset>
						<fieldset class="content-group">
							<legend class="text-semibold">
								<i class="icon-file-text2 position-left"></i>
								Office Use Only
								<a class="control-arrow" data-toggle="collapse" data-target="#office">
									<i class="icon-circle-down2"></i>
								</a>
							</legend>
							<div class="collapse in" id="office">
								<div class="col-md-6">
									<div class="form-group">
										<label>Created At</label>
										<?php 
											echo $this->Form->input('created', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled
												)
											); 
										?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Last Modified</label>
										<?php 
											echo $this->Form->input('modified', array(
												'class'=>'form-control',
												'label'=>false,
												'error'=>false,
												'type'=>'text',
												'disabled'=>$disabled
												)
											); 
										?>
									</div>
								</div>
							</div>
						</fieldset>
					</div>
					<div class="panel-footer">
						<div class="heading-elements">
							<span class="heading-text">
								<a class="btn btn-warning" href="<?php echo $this->Html->url('/Roles', true);?>">
									Back <i class="icon-arrow-left13 position-right"></i>
								</a>
							</span>
						</div>
					</div>
				</div>
			<?php echo $this->Form->end(); ?>
			<!-- /basic layout -->
		</div>
	</div>
	<!-- /Main -->
</div>
<!-- /Content area -->