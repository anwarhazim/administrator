<div class="content-wrapper">

<!-- Cover area -->
<div class="profile-cover">
    <div class="profile-cover-img" style="background-image: url(<?php echo $this->html->url('/app/webroot/images/backgrounds/user_material_bg.jpg', true); ?>)">
    </div>
    <div class="media">
        <div class="media-left">
            <a href="#" class="profile-thumb">
                <?php
                    if(empty($session['Gallery']))
                    { 
                        echo $this->Html->image('/app/webroot/images/placeholders/placeholder.jpg', array('class' => 'img-circle', 'style' => 'width: 120px; height: 120px;', 'alt' => $session['Personal']['complete_name'])); 
                    }
                    else
                    {
                        echo $this->Html->image($session['Path']['url'].'/app/webroot/documents/'.$session['Employee']['employee_no'].'/MEDIAS/'.$session['Gallery']['name'], array('class' => 'img-circle', 'style' => 'width: 120px; height: 120px;', 'alt' => $session['Personal']['complete_name']));
                    }
                ?>
            </a>
        </div>

        <div class="media-body">
            <h1>
                <?php echo $session['Personal']['complete_name']; ?> 
                <small class="display-block"><?php echo $session['Organisation']['name']; ?></small>
                <small class="display-block"><?php echo $session['Employee']['position']; ?></small>
            </h1>
        </div>
    </div>
</div>
<!-- /cover area -->
<!-- Toolbar -->
<div class="navbar navbar-default navbar-xs navbar-component no-border-radius-top">
    <ul class="nav navbar-nav visible-xs-block">
        <li class="full-width text-center"><a data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
    </ul>
    <!--
    <div class="navbar-collapse collapse" id="navbar-filter">
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-books"></i> User Manual <span class="caret"></span></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                    <li class="dropdown-header highlight"><i class="icon-file-eye"></i> Guidelines</li>
                    <li><a href="#" target="_blank">PRESS User Manual</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    -->
</div>
<!-- /toolbar -->

<div class="row">
    <div class="col-lg-12">
        <!-- Search -->
        <div class="panel panel-flat">
            <?php echo $this->Form->create('Employee', array('class'=>'', 'novalidate'=>'novalidate'));?>
            <div class="panel-body">
                <?php echo $this->Session->flash(); ?>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <legend class="text-bold">My Profile</legend>
                        </div>
                        <div class="col-md-6">
                            <label>Name</label>
                            <?php 
                                echo $this->Form->input('complete_name', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'type'=>'text',
                                    'value'=>$employee['Personal']['complete_name'],
                                    'disabled'=>$disabled
                                    )
                                ); 
                            ?>
                        </div>
                        <div class="col-md-6">
                            <label>IC No.</label>
                            <?php 
                                echo $this->Form->input('ic_no', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'type'=>'text',
                                    'value'=>$employee['Personal']['ic_no'],
                                    'disabled'=>$disabled
                                    )
                                ); 
                            ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Staff No.</label>
                            <?php 
                                echo $this->Form->input('employee_no', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'type'=>'text',
                                    'value'=>$employee['Employee']['employee_no'],
                                    'disabled'=>$disabled
                                    )
                                ); 
                            ?>
                        </div>
                        <div class="col-md-6">
                            <label>Email</label>
                            <?php 
                                echo $this->Form->input('email', array(
                                    'class'=>'form-control',
                                    'label'=> false,
                                    'error'=> false,
                                    'type'=>'text',
                                    'value'=>$employee['Contact']['email'],
                                    'disabled'=>$disabled
                                    )
                                ); 
                            ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <legend class="text-bold">Role Information</legend>
                        </div>
                        <div class="col-md-12">
                            <?php 
                                echo $this->Form->input('roles', array(
                                    'class'=>'checkbox', 
                                    'type' => 'select',
                                    'multiple' => 'checkbox', 
                                    'label'=>false,
                                    'required'=>'required',
                                    'options'=>$roles,
                                    'selected' => $employee['RoleList'],
                                    'div' => false,
                                    'disabled'=>$disabled,
                                    )
                                );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
        <!-- /Search -->
    </div>
</div>

</div>