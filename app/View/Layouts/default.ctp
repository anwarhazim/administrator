
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Administrator | Prasarana Administrator</title>

	<!-- Global stylesheets -->
	<link rel="stylesheet" type="text/css" href="<?php echo $this->html->url('/app/webroot/css/icons/icomoon/styles.css', true); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->html->url('/app/webroot/css/bootstrap.css', true); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->html->url('/app/webroot/css/core.css', true); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->html->url('/app/webroot/css/components.css', true); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->html->url('/app/webroot/css/colors.css', true); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->html->url('/app/webroot/css/orgchart/jquery.orgchart.css', true); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->html->url('/app/webroot/css/jstree/jstree.min.css', true); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->html->url('/app/webroot/css/nestable.css', true); ?>">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	 <script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/loaders/pace.min.js', true); ?>"></script>
    <script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/core/libraries/jquery.min.js', true); ?>"></script>
    <script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/core/libraries/bootstrap.min.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/loaders/blockui.min.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/ui/drilldown.js', true); ?>"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/ui/nicescroll.min.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/media/fancybox.min.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/forms/selects/select2.min.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/core/libraries/jasny_bootstrap.min.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/forms/styling/uniform.min.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/forms/styling/switchery.min.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/forms/selects/bootstrap_multiselect.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/ui/moment/moment.min.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/pickers/daterangepicker.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/pickers/anytime.min.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/pickers/pickadate/picker.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/pickers/pickadate/picker.date.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/pickers/pickadate/picker.time.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/ui/fullcalendar/fullcalendar.min.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/orgchart/jquery.orgchart.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/nestable/jquery.nestable.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/jstree/jstree.min.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/forms/inputs/touchspin.min.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/core/libraries/jquery_ui/interactions.min.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/notifications/pnotify.min.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/forms/inputs/touchspin.min.js', true); ?>"></script>

	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/core/libraries/jquery_ui/interactions.min.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/core/libraries/jquery_ui/widgets.min.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/core/libraries/jquery_ui/effects.min.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/plugins/extensions/mousewheel.min.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/core/libraries/jquery_ui/globalize/globalize.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/core/libraries/jquery_ui/globalize/cultures/globalize.culture.de-DE.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/core/libraries/jquery_ui/globalize/cultures/globalize.culture.ja-JP.js', true); ?>"></script>

	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/app.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/demo_pages/gallery.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/demo_pages/mail_list.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/demo_pages/form_input_groups.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/demo_pages/components_popups.js', true); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->html->url('/app/webroot/js/templete.js', true); ?>"></script>
	<!-- /Theme JS files -->
</head>

<body>
	<!-- hidden value -->
		<input type='hidden' name='baseUrl' id='baseUrl' value="<?php echo $this->Html->url('/', true); ?>"/>
	<!-- hidden value -->							
	
	<!-- Main navbar -->
		<?php echo $this->element('navbar'); ?>
	<!-- /main navbar -->


	<!-- Second navbar -->
		<?php echo $this->element('sidebar'); ?>
	<!-- /second navbar -->

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
				<?php echo $this->fetch('content'); ?>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
	<!-- Warning modal -->
    <div id="modal_logout" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-warning">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h6 class="modal-title"><span class="icon-exit2"></span> Leave site?</h6>
                </div>

                <div class="modal-body">
                    <h6 class="text-semibold">Are you sure you want to Log Out?</h6>
                    <p>Changes you made may not be saved. Please save your work.</p>
                </div>

                <div class="modal-footer">
                    <a class="btn btn-warning" href="<?php echo $this->Html->url('/Users/logout', true); ?>">Yes</a>
                    <button type="button" class="btn btn-link" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /warning modal -->
	<!-- Footer -->
	<div class="footer text-muted">
	&copy; 2020 <a href="#">Administrator | Prasarana Administrator</a> by <a href="http://www.myrapid.com.my/corporate-information" target="_blank">Prasarana</a> powered by D&T Division
	</div>
	<!-- /footer -->

</body>
</html>
