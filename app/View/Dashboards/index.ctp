<!-- Breadcrumb -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
            <li><a href="#">Dashboards</a></li>
		</ul>
	</div>

</div>
<!-- /Breadcrumb -->
<!-- Search -->
<div class="panel panel-flat">
    <?php echo $this->Form->create('User', array('class'=>'', 'novalidate'=>'novalidate'));?>
    <div class="panel-body">
        <?php 
            echo $this->Session->flash(); 

            if(!empty($this->validationErrors['User']))
            {
            ?>
                <div role="alert" class="alert alert-danger">
                        <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <?php
                            foreach ($this->validationErrors['User'] as $errors) 
                            {
                                echo '<ul>';
                                foreach ($errors as $error) 
                                {
                                    echo '<li>'.h($error).'</li>';
                                }
                                echo '</ul>';
                            }
                        ?>
                </div>
            <?php
            }
        ?>
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label>Start Date</label>
                    <?php 
                        echo $this->Form->input('start_date', array(
                            'class'=>'form-control start_date', 
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text'
                            )
                        ); 
                    ?>
                </div>
                <div class="col-md-6">
                    <label>End Date</label>
                    <?php 
                        echo $this->Form->input('end_date', array(
                            'class'=>'form-control end_date', 
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text'
                            )
                        ); 
                    ?>
                </div>
            </div>
        </div>
        <div class="text-right">
            <button type="submit" class="btn btn-primary">
                Find <i class="icon-search4 position-right"></i>
            </button>
            <a class="btn btn-default position-right" href="<?php echo $this->Html->url('/', true);?>">
                Reset <i class="icon-spinner11 position-right"></i>
            </a>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<!-- /Search -->
<!-- Result -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h6 class="panel-title">Overall</h6>
    </div>
    <div class="panel-body">
        
    </div>
    <div class="panel-footer">
        <div class="heading-elements">
            <span class="heading-text">
			</span>
        </div>
    </div>
</div>
<!-- Result -->