<!-- Basic modal -->
<div id="<?='danger'.$id ?>" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title"><?php echo $header_label ?></h5>
            </div>

            <div class="modal-body">
                <p><?php echo $message ?></p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Tidak</button>
                <a class="btn btn-danger" href="<?= $this->Html->url(array('action' => $action.'/'.$id));?>">
                    Hapus
                </a>
            </div>
        </div>
    </div>
</div>
<!-- /basic modal -->