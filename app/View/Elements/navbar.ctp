<div class="navbar navbar-inverse bg-pink-800">
    <div class="navbar-header">
        <a class="navbar-brand" href="<?php echo $this->html->url('/', true); ?>">
            <?php echo $this->Html->image('/app/webroot/images/logos/logo_administrator_light_white.png', array('alt' => 'Administrator | Prasarana Administrator')); ?>
        </a>

        <ul class="nav navbar-nav pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
        </ul>
    </div>

    <ul class="nav navbar-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="icon-link2"></i>
                <span class="visible-xs-inline-block position-right">Quick Links</span>
            </a>
            
            <div class="dropdown-menu dropdown-content">
                <div class="dropdown-content-heading">
                    Quick Links
                </div>

                <ul class="media-list dropdown-content-body width-350">
                    <?php
                        echo $session['Project'];
                    ?>
                </ul>
            </div>
        </li>
    </ul>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav navbar-right">

            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <?php 
                        if(empty($session['Gallery']))
                        {
                            echo $this->Html->image('/app/webroot/images/placeholders/placeholder.jpg', array('class' => 'img-circle img-xs'));
                        }
                        else
                        {
                            //echo $this->Html->image('/app/webroot/documents/'.$session['Staff']['employee_no'].'/MEDIAS/'.$session['Gallery']['name'], array('class' => 'img-circle img-xs'));
                    ?>
                         <img src="<?php echo $this->html->url($session['Path']['url'].'/app/webroot/documents/'.$session['Employee']['employee_no'].'/MEDIAS/'.$session['Gallery']['name'], true); ?>" class="img-circle img-xs" alt="<?php echo $session['Personal']['complete_name'];?>" />
                    <?php
                        }
                    ?>
                    <span><?php echo $session['Personal']['complete_name']; ?></span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li>
                        <a href="<?php echo $this->html->url('/Users/profile', true); ?>">
                        <i class="icon-user-plus"></i> My Profile</a>
                    </li>
                    <li>
                        <a href="<?php echo $this->html->url('/Users/password', true); ?>">
                            <i class="icon-key"></i> Change Password
                        </a>
                    </li>
                    <li>
                        <a href="#" data-toggle="modal" data-target="#modal_logout">
                            <i class="icon-switch2"></i> Logout
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>