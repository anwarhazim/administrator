<div class="navbar navbar-default" id="navbar-second">
    <ul class="nav navbar-nav no-border visible-xs-block">
        <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second-toggle"><i class="icon-menu7"></i></a></li>
    </ul>

    <div class="navbar-collapse collapse" id="navbar-second-toggle">
        <ul class="nav navbar-nav">
            <li>
                <a href="<?php echo $this->html->url('/', true); ?>">
                    <i class="icon-home4 position-left"></i> Home Page
                </a>
            </li>
            <?php echo $session['Sidebar']; ?>
            </li>
        </ul>

    </div>
</div>