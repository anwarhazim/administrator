<div class="content-wrapper">

<!-- Breadcrumb -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
            <li>Configuration</li>
            <li><a href="<?php echo $this->html->url('/Users/index', true);?>">Users</a></li>
            <li class="active">Assign User</li>
		</ul>
	</div>

</div>
<!-- /Breadcrumb -->
<!-- Search -->
<div class="panel panel-flat">
    <?php echo $this->Form->create('User', array('class'=>'', 'novalidate'=>'novalidate'));?>
    <div class="panel-body">
        <?php 
            echo $this->Session->flash(); 

            if(!empty($this->validationErrors['User']))
            {
            ?>
                <div role="alert" class="alert alert-danger">
                        <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <?php
                            foreach ($this->validationErrors['User'] as $errors) 
                            {
                                echo '<ul>';
                                foreach ($errors as $error) 
                                {
                                    echo '<li>'.h($error).'</li>';
                                }
                                echo '</ul>';
                            }
                        ?>
                </div>
            <?php
            }
        ?>
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label>Name </label>
                    <?php 
                        echo $this->Form->input('name', array(
                            'class'=>'form-control',
                            'label'=>false,
                            'error'=>false,
                            'type'=>'text',
                            'placeholder'=>'Name',
                            'disabled'=>'disabled',
                            'value'=>$employee['Personal']['complete_name']
                            )
                        ); 
                    ?>
                </div>
                <div class="col-md-6">
                    <label>Username </label>
                    <?php 
                        echo $this->Form->input('username', array(
                            'class'=>'form-control',
                            'label'=>false,
                            'error'=>false,
                            'type'=>'text',
                            'placeholder'=>'Username',
                            'disabled'=>'disabled',
                            )
                        ); 
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <label>Roles </label>
                    <?php 
                        echo $this->Form->input('roles', array(
                            'class'=>'checkbox', 
                            'type' => 'select',
                            'multiple' => 'checkbox', 
                            'label'=>false,
                            'required'=>'required',
                            'options'=>$roles,
                            'selected' => $role_selected,
                            'div' => false,
                            'disabled'=>$disabled,
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
        <div class="text-right">
            <button type="submit" class="btn btn-success legitRipple">
                Save <i class="icon-floppy-disk position-right"></i>
            </button>
            <a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Users/assign/'.$key, true);?>">
                Reset <i class="icon-spinner11 position-right"></i>
            </a>
        </div>
    </div>
    <div class="panel-footer">
        <div class="heading-elements">
			<span class="heading-text">
				<a href="<?php echo $this->html->url('/Users/index', true); ?>" class="btn btn-warning legitRipple">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>

</div>