<div class="content-wrapper">

<!-- Breadcrumb -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
            <li>Configuration</li>
            <li><a href="<?php echo $this->html->url('/Contacts', true);?>">Contacts</a></li>
            <li class="active">Upload Contact</li>
		</ul>
	</div>

</div>
<!-- /Breadcrumb -->
<!-- Body -->
<div class="panel panel-flat">
    <?php echo $this->Form->create('Contact', array('class'=>'', 'novalidate'=>'novalidate', 'type'=>'file'));?>
    <div class="panel-body">
        <?php 
            echo $this->Session->flash(); 

            if(!empty($this->validationErrors['Contact']))
            {
            ?>
                <div role="alert" class="alert alert-danger">
                        <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <?php
                            foreach ($this->validationErrors['Contact'] as $errors) 
                            {
                                echo '<ul>';
                                foreach ($errors as $error) 
                                {
                                    echo '<li>'.h($error).'</li>';
                                }
                                echo '</ul>';
                            }
                        ?>
                </div>
            <?php
            }
        ?>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <label>MS Excel File</label>
                    <?php 
                        echo $this->Form->input('attachment', array(
                            'class'=>'form-control file-styled',
                            'label'=> false,
                            'error'=>false,
                            'type'=>'file',
                            'multiple'=>'multiple',
                            'data-preview-file-type'=>'any',
                            'data-show-upload'=>false,
                            'showRemove'=>false,
                            'data-show-caption'=>false,
                            'accept'=>'application/vnd.ms-excel',
                            'maxFileSize'=>'2MB'
                            )
                        ); 
                    ?>
                    <span class="help-block">Accepted formats: xls 2Mb</span>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-success">
                        Upload <i class="icon-floppy-disk position-right"></i>
                    </button>
                    <a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Contacts/upload/', true);?>">
                        Reset <i class="icon-spinner11 position-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <div class="heading-elements">
            <span class="heading-text">
                <a class="btn btn-warning" href="<?php echo $this->Html->url('/Contacts', true);?>">
                    Back <i class="icon-arrow-left13 position-right"></i>
                </a>
			</span>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<!-- /Body -->

</div>