<!-- Breadcrumb -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
            <li>Configuration</li>
            <li class="active">Projects</li>
		</ul>
	</div>

</div>
<!-- /Breadcrumb -->
<!-- Search -->
<div class="panel panel-flat">
    <?php echo $this->Form->create('Project', array('class'=>'', 'novalidate'=>'novalidate'));?>
    <div class="panel-body">
        <?php 
            echo $this->Session->flash(); 

            if(!empty($this->validationErrors['Project']))
            {
            ?>
                <div role="alert" class="alert alert-danger">
                        <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <?php
                            foreach ($this->validationErrors['Project'] as $errors) 
                            {
                                echo '<ul>';
                                foreach ($errors as $error) 
                                {
                                    echo '<li>'.h($error).'</li>';
                                }
                                echo '</ul>';
                            }
                        ?>
                </div>
            <?php
            }
        ?>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <label>Search</label>
                    <?php 
                        echo $this->Form->input('search', array(
                            'class'=>'form-control',
                            'placeholder'=>'Name', 
                            'label'=> false,
                            'error'=> false,
                            )
                        ); 
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label>Start Date</label>
                    <?php 
                        echo $this->Form->input('start_date', array(
                            'class'=>'form-control start_date', 
                            'label'=> false,
                            'error'=> false,
                            'placeholder'=>'dd-mm-yyyy'
                            )
                        ); 
                    ?>
                </div>
                <div class="col-md-6">
                    <label>End Date</label>
                    <?php 
                        echo $this->Form->input('end_date', array(
                            'class'=>'form-control end_date', 
                            'label'=> false,
                            'error'=> false,
                            'placeholder'=>'dd-mm-yyyy'
                            )
                        ); 
                    ?>
                </div>
            </div>
        </div>
        <div class="text-right">
            <button type="submit" class="btn btn-primary">
                Find <i class="icon-search4 position-right"></i>
            </button>
            <a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Projects/index', true);?>">
                Reset <i class="icon-spinner11 position-right"></i>
            </a>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<!-- /Search -->
<!-- Result -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h6 class="panel-title">List of Project</h6>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th width="5%" class="text-center">No.</th>
                    <th width="">Name</th>
                    <th width="15%" class="text-center">Icon</th>
                    <th width="15%" class="text-center">Color</th>
                    <th width="15%" class="text-center">Last Modified</th>
                    <th width="20%" class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $counter = $this->Paginator->counter(array('format' =>'{:start}'));
                    
                    if(!empty($details))
                    {
                        foreach ($details as $detail) 
						{
                ?>
                    <tr>
                        <td valign="top" class="text-center">
                            <?php echo $counter; ?>
                        </td>
                        <td>
                            <?php echo $detail['Project']['name'] ?>
                        </td>
                        <td class="text-center">
                            <span class="<?php echo $detail['Project']['icon'] ?>"></span>
                        </td>
                        <td class="text-center">
                            <span class="label bg-<?php echo $detail['Project']['color'] ?>"><?php echo $detail['Project']['color'] ?></span>
                        </td>
                        <td class="text-center">
                            <?php echo $detail['Project']['modified'] ?>
                        </td>
                        <td class="text-center">
                            <a href="<?php echo $this->Html->url('/Projects/view/'.$detail['Project']['id']);?>" class="btn bg-grey-300 btn-icon btn-rounded legitRipple" title="View record">
                                <spam class="icon-file-text">
                                </spam>
                            </a>
                            
                            <a href="<?php echo $this->Html->url('/Projects/edit/'.$detail['Project']['id']);?>" class="btn bg-grey-300 btn-icon btn-rounded legitRipple" title="Edit record">
                                <spam class="icon-pencil">
                                </spam>
                            </a>
                            
                            <?php
                                echo $this->html->link(
                                                    '<spam class="icon-bin"></spam>', 
                                                    array('controller' => 'Projects', 'action' => 'delete', $detail['Project']['id']),
                                                    array('class' => 'btn bg-grey-300 btn-icon btn-rounded legitRipple', 'title' => 'Delete record', 'confirm' => 'Are you sure you want to delete this record?', 'escape' => false)
                                                );
                            ?>
                        </td>
                    </tr>
                <?php
                        $counter++;
                        }
                    }
                    else
                    {
                ?>
                    <tr>
                        <td colspan="6">No data</td>
                    </tr>
                <?php
                    }
                ?>
            </tbody>
        </table>
    </div>
    <div class="panel-body">
        <span class="pull-right">
            <?php echo $this->Paginator->counter(array(
                                                'format' => 'Page {:page} from {:pages}, show {:current} record from
                                                        {:count} total, start {:start}, end at {:end}'
                                                ));
            ?>
        </span>
    </div>
    <div class="panel-footer">
        <div class="heading-elements">
            <span class="heading-text">
                <a class="btn btn-primary" href="<?php echo $this->Html->url('/Projects/add', true);?>">
                    Add New Project <i class="icon-plus3 position-right"></i>
                </a>
			</span>
            <ul class="pagination  pull-right">
                <?php 
                    echo $this->Paginator->prev('< ' . __('<'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                    echo $this->Paginator->numbers(array('currentTag'=> 'a', 'currentClass' => 'active', 'tag' => 'li', 'separator' => false));
                    echo $this->Paginator->next(__('>') . ' >', array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                ?>
            </ul>
        </div>
    </div>
</div>
<!-- Result -->