<!-- Breadcrumb -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
            <li>Configuration</li>
            <li><a href="<?php echo $this->html->url('/Projects/index', true);?>">Projects</a></li>
			<li class="active">View Project</li>
		</ul>
	</div>

</div>
<!-- /Breadcrumb -->
<div class="panel panel-flat">
    <?php echo $this->Form->create('Project', array('class'=>'', 'novalidate'=>'novalidate', 'type'=>'file'));?>
    <div class="panel-heading">
        <h5 class="panel-title">Project Information </h5>
    </div>
    <div class="panel-body">
        <?php 
            echo $this->Session->flash(); 

            if(!empty($this->validationErrors['Project']))
            {
            ?>
                <div role="alert" class="alert alert-danger">
                        <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <?php
                            foreach ($this->validationErrors['Project'] as $errors) 
                            {
                                echo '<ul>';
                                foreach ($errors as $error) 
                                {
                                    echo '<li>'.h($error).'</li>';
                                }
                                echo '</ul>';
                            }
                        ?>
                </div>
            <?php
            }
        ?>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <label>Name </label>
                    <?php 
                        echo $this->Form->input('name', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text',
                            'disabled'=>'disabled'
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-3">
                    <label>Code </label>
                    <?php 
                        echo $this->Form->input('code', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text',
                            'disabled'=>'disabled'
                            )
                        );
                    ?>
                </div>
                <div class="col-md-3">
                    <label>Icon </label>
                    <?php 
                        echo $this->Form->input('icon', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text',
                            'disabled'=>'disabled'
                            )
                        );
                    ?>
                </div>
                <div class="col-md-3">
                    <label>Color </label>
                    <?php 
                        echo $this->Form->input('color', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text',
                            'disabled'=>'disabled'
                            )
                        );
                    ?>
                </div>
                <div class="col-md-3">
                    <label>URL </label>
                    <?php 
                        echo $this->Form->input('url', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text',
                            'disabled'=>'disabled'
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <label>Description </label>
                    <?php 
                        echo $this->Form->input('desc', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'textarea',
                            'disabled'=>'disabled'
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <label>Note </label>
                    <?php 
                        echo $this->Form->input('note', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'textarea',
                            'disabled'=>'disabled'
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
        <legend class="text-bold">Office Use Only</legend>
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label>Created By </label>
                    <?php 
                        echo $this->Form->input('name', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text',
                            'disabled'=>'disabled',
                            'value'=>$detail['CreatedBy']['complete_name']
                            )
                        );
                    ?>
                </div>
                <div class="col-md-6">
                    <label>Modified By </label>
                    <?php 
                        echo $this->Form->input('icon', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text',
                            'disabled'=>'disabled',
                            'value'=>$detail['ModifiedBy']['complete_name']
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label>Created At </label>
                    <?php 
                        echo $this->Form->input('created', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text',
                            'disabled'=>'disabled',
                            )
                        );
                    ?>
                </div>
                <div class="col-md-6">
                    <label>Last Modified </label>
                    <?php 
                        echo $this->Form->input('modified', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text',
                            'disabled'=>'disabled',
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a href="<?php echo $this->html->url('/Projects/index', true); ?>" class="btn btn-warning legitRipple">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
    <?php echo $this->Form->end(); ?>
</div>