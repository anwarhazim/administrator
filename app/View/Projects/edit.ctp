<!-- Breadcrumb -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
            <li>Configuration</li>
            <li><a href="<?php echo $this->html->url('/Projects/index', true);?>">Projects</a></li>
			<li class="active">Edit Project</li>
		</ul>
	</div>

</div>
<!-- /Breadcrumb -->
<div class="panel panel-flat">
    <?php echo $this->Form->create('Project', array('class'=>'', 'novalidate'=>'novalidate', 'type'=>'file'));?>
    <div class="panel-heading">
        <h5 class="panel-title">Project Information </h5>
    </div>
    <div class="panel-body">
        <?php 
            echo $this->Session->flash(); 

            if(!empty($this->validationErrors['Project']))
            {
            ?>
                <div role="alert" class="alert alert-danger">
                        <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <?php
                            foreach ($this->validationErrors['Project'] as $errors) 
                            {
                                echo '<ul>';
                                foreach ($errors as $error) 
                                {
                                    echo '<li>'.h($error).'</li>';
                                }
                                echo '</ul>';
                            }
                        ?>
                </div>
            <?php
            }
        ?>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <label>Name <span class="text-danger">*</span></label>
                    <?php 
                        echo $this->Form->input('name', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text'
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-3">
                    <label>Code <span class="text-danger">*</span></label>
                    <?php 
                        echo $this->Form->input('code', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text'
                            )
                        );
                    ?>
                </div>
                <div class="col-md-3">
                    <label>Icon <span class="text-danger">*</span></label>
                    <?php 
                        echo $this->Form->input('icon', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text'
                            )
                        );
                    ?>
                </div>
                <div class="col-md-3">
                    <label>Color <span class="text-danger">*</span></label>
                    <?php 
                        echo $this->Form->input('color', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text'
                            )
                        );
                    ?>
                </div>
                <div class="col-md-3">
                    <label>URL <span class="text-danger">*</span></label>
                    <?php 
                        echo $this->Form->input('url', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'text'
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <label>Description <span class="text-danger">*</span></label>
                    <?php 
                        echo $this->Form->input('desc', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'textarea'
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <label>Note </label>
                    <?php 
                        echo $this->Form->input('note', array(
                            'class'=>'form-control',
                            'label'=> false,
                            'error'=> false,
                            'type'=>'textarea'
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
        <div class="text-right">
            <button type="submit" class="btn btn-success legitRipple">
                Save <i class="icon-floppy-disk position-right"></i>
            </button>
            <a class="btn btn-default position-right" href="<?php echo $this->Html->url('/Projects/edit/'.$key, true);?>">
                Reset <i class="icon-spinner11 position-right"></i>
            </a>
        </div>
    </div>
    <div class="panel-footer">
		<div class="heading-elements">
			<span class="heading-text">
				<a href="<?php echo $this->html->url('/Projects/index', true); ?>" class="btn btn-warning legitRipple">
					Back <i class="icon-arrow-left13 position-right"></i>
				</a>
			</span>
		</div>
	</div>
    <?php echo $this->Form->end(); ?>
</div>