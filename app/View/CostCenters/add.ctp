<!-- Breadcrumb -->
<div class="page-header page-header-default">

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
            <li><a href="<?php echo $this->html->url('/', true);?>">Home</a></li>
            <li>Configuration</li>
			<li><a href="<?php echo $this->html->url('/CostCenters', true);?>">Cost Centers</a></li>
            <li class="active">Add New Cost Center</li>
		</ul>
	</div>

</div>
<!-- /Breadcrumb -->
<!-- Content area -->
<div class="content">
	<!-- Main -->
	<div class="row">
		<div class="col-md-12">
			<!-- Basic layout-->
			<?php echo $this->Form->create('CostCenter', array('class'=>'', 'novalidate'=>'novalidate'));?>
				<div class="panel panel-flat">
					<div class="panel-body">
						<?php 
							echo $this->Session->flash(); 

							if(!empty($this->validationErrors['CostCenter']))
							{
							?>
								<div role="alert" class="alert alert-danger">
										<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
										<?php
											foreach ($this->validationErrors['CostCenter'] as $errors) 
											{
												echo '<ul>';
												foreach ($errors as $error) 
												{
													echo '<li>'.h($error).'</li>';
												}
												echo '</ul>';
											}
										?>
								</div>
							<?php
							}
						?>
						<fieldset class="content-group">
							<div class="col-md-6">
								<div class="form-group">
									<label>Name <span class="text-danger">*</span></label>
									<?php 
										echo $this->Form->input('name', array(
											'class'=>'form-control',
											'label'=>false,
											'error'=>false,
											'type'=>'text',
											'placeholder'=>'Name'
											)
										); 
									?>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>Code <span class="text-danger">*</span></label>
									<?php 
										echo $this->Form->input('code', array(
											'class'=>'form-control',
											'label'=>false,
											'error'=>false,
											'type'=>'text',
											'placeholder'=>'Code'
											)
										); 
									?>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>Company Code <span class="text-danger">*</span></label>
									<?php 
										echo $this->Form->input('company_code', array(
											'class'=>'form-control',
											'label'=>false,
											'error'=>false,
											'type'=>'text',
											'placeholder'=>'Company Code'
											)
										); 
									?>
								</div>
							</div>
						</fieldset>
						<fieldset class="content-group">
							<div class="col-md-12">
								<div class="form-group">
									<label>Description </label>
									<?php 
										echo $this->Form->input('description', array(
											'class'=>'form-control',
											'label'=>false,
											'error'=>false,
											'type'=>'text',
											'placeholder'=>'Description'
											)
										); 
									?>
								</div>
							</div>
						</fieldset>
						<fieldset class="content-group">
							<div class="col-md-12">
								<div class="form-group">
									<label>Note </label>
									<?php 
										echo $this->Form->input('note', array(
											'class'=>'form-control',
											'label'=>false,
											'error'=>false,
											'type'=>'textarea',
											)
										); 
									?>
								</div>
							</div>
						</fieldset>
						<div class="text-right">
							<button type="submit" class="btn btn-success">
								Save <i class="icon-floppy-disk position-right"></i>
							</button>
							<a class="btn btn-default position-right" href="<?php echo $this->Html->url('/CostCenters/add', true);?>">
								Reset <i class="icon-spinner11 position-right"></i>
							</a>
						</div>
					</div>
					<div class="panel-footer">
						<div class="heading-elements">
							<span class="heading-text">
								<a class="btn btn-warning" href="<?php echo $this->Html->url('/CostCenters', true);?>">
									Back <i class="icon-arrow-left13 position-right"></i>
								</a>
							</span>
						</div>
					</div>
				</div>
			<?php echo $this->Form->end(); ?>
			<!-- /basic layout -->
		</div>
	</div>
	<!-- /Main -->
</div>
<!-- /Content area -->