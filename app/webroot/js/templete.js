$(function() 
{
    var baseURL = "";
        
    if($("#baseUrl").length != 0) 
    {
        baseUrl = $("#baseUrl").val();
    }
});

$(function(){
    $("#nestable").nestable({
        maxDepth: 10,
    }).on("change", function() {
      var json_text = $(".dd").nestable("serialize");
      $.post(window.location.pathname, {
          data: json_text
      }, function(response){
          //alert(response);
      });
    });
});

$(function() 
{
    if($("#chart-container").length != 0) 
    {
        var oc = $('#chart-container').orgchart({
            'data' : $('#organisation_list'),
            'pan': true,
            'zoom': true
        });
    
        oc.$chartContainer.on('touchmove', function(event) {
        event.preventDefault();
        });
    }
});
      

$(function()
{    
    $(document).ready(function () {

        // Month and year menu
        $(".datepicker-menus").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy',
            minDate: 0,
            isRTL: $('html').attr('dir') == 'rtl' ? true : false
        });

        if($(".date").length != 0) 
        {
            $(".date").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-mm-yy',
                yearRange: "-50:+0",
            });
        }

        if($(".start_date").length != 0 && $(".end_date").length != 0) 
        {
            $(".start_date").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-mm-yy',
                yearRange: "-50:+0",
                onSelect: function (selected) {
                    var d=new Date(selected.split("-").reverse().join("-"));
                    var dd=d.getDate();
                    var mm=d.getMonth()+1;
                    var yy=d.getFullYear();
                    var newdate = yy+"/"+mm+"/"+dd;
                    var dt = new Date(newdate);
                    dt.setDate(dt.getDate() + 1);
                    $(".end_date").datepicker("option", "minDate", dt);
                }
            });
            $(".end_date").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-mm-yy',
                yearRange: "-50:+0",
                onSelect: function (selected) {
                    var d=new Date(selected.split("-").reverse().join("-"));
                    var dd=d.getDate();
                    var mm=d.getMonth()+1;
                    var yy=d.getFullYear();
                    var newdate = yy+"/"+mm+"/"+dd;
                    var dt = new Date(newdate);
                    dt.setDate(dt.getDate() - 1);
                    $(".start_date").datepicker("option", "maxDate", dt);
                }
            });
        }

    //     if($(".pickatime").length != 0) 
    //     {
    //         $('.pickatime').pickatime();
    //     }

    //     if($(".date").length != 0) 
    //     {
    //         $('.date').pickadate({
    //             selectYears: true,
    //             selectMonths: true,
    //             format: 'dd-mm-yyyy',
    //         });
    //     }

    //     if($(".start_date").length != 0 && $(".end_date").length != 0) 
    //     {
    //         // Year selector
    //         $('.start_date').pickadate({
    //             selectYears: true,
    //             selectMonths: true,
    //             format: 'dd-mm-yyyy',
    //         });

    //         $('.end_date').pickadate({
    //             selectYears: true,
    //             selectMonths: true,
    //             format: 'dd-mm-yyyy',
    //         });

    //         var from_$input = $('.start_date').pickadate(),
    //         from_picker = from_$input.pickadate('picker')

    //         var to_$input = $('.end_date').pickadate(),
    //             to_picker = to_$input.pickadate('picker')


    //         // Check if there’s a “from” or “to” date to start with.
    //         if ( from_picker.get('value') ) {
    //             to_picker.set('min', from_picker.get('select'))
    //         }
    //         if ( to_picker.get('value') ) {
    //             from_picker.set('max', to_picker.get('select'))
    //         }

    //         // When something is selected, update the “from” and “to” limits.
    //         from_picker.on('set', function(event) {
    //             if ( event.select ) {
    //             to_picker.set('min', from_picker.get('select'))    
    //             }
    //             else if ( 'clear' in event ) {
    //             to_picker.set('min', false)
    //             }
    //         })
    //         to_picker.on('set', function(event) {
    //             if ( event.select ) {
    //             from_picker.set('max', to_picker.get('select'))
    //             }
    //             else if ( 'clear' in event ) {
    //             from_picker.set('max', false)
    //             }
    //         })
    //     }

    //     if($(".start_date_1").length != 0 && $(".end_date_1").length != 0) 
    //     {
    //         // Year selector
    //         $('.start_date_1').pickadate({
    //             selectYears: true,
    //             selectMonths: true,
    //             format: 'dd-mm-yyyy',
    //         });

    //         $('.end_date_1').pickadate({
    //             selectYears: true,
    //             selectMonths: true,
    //             format: 'dd-mm-yyyy',
    //         });

    //         var from_$input = $('.start_date_1').pickadate(),
    //         from_picker = from_$input.pickadate('picker')

    //         var to_$input = $('.end_date_1').pickadate(),
    //             to_picker = to_$input.pickadate('picker')


    //         // Check if there’s a “from” or “to” date to start with.
    //         if ( from_picker.get('value') ) {
    //             to_picker.set('min', from_picker.get('select'))
    //         }
    //         if ( to_picker.get('value') ) {
    //             from_picker.set('max', to_picker.get('select'))
    //         }

    //         // When something is selected, update the “from” and “to” limits.
    //         from_picker.on('set', function(event) {
    //             if ( event.select ) {
    //             to_picker.set('min', from_picker.get('select'))    
    //             }
    //             else if ( 'clear' in event ) {
    //             to_picker.set('min', false)
    //             }
    //         })
    //         to_picker.on('set', function(event) {
    //             if ( event.select ) {
    //             from_picker.set('max', to_picker.get('select'))
    //             }
    //             else if ( 'clear' in event ) {
    //             from_picker.set('max', false)
    //             }
    //         })
    //     }

    //     if($(".start_date_2").length != 0 && $(".end_date_2").length != 0) 
    //     {
    //         // Year selector
    //         $('.start_date_2').pickadate({
    //             selectYears: true,
    //             selectMonths: true,
    //             format: 'dd-mm-yyyy',
    //         });

    //         $('.end_date_2').pickadate({
    //             selectYears: true,
    //             selectMonths: true,
    //             format: 'dd-mm-yyyy',
    //         });

    //         var from_$input = $('.start_date_2').pickadate(),
    //             from_picker_2 = from_$input.pickadate('picker')

    //         var to_$input = $('.end_date_2').pickadate(),
    //             to_picker_2 = to_$input.pickadate('picker')


    //         // Check if there’s a “from” or “to” date to start with.
    //         if ( from_picker_2.get('value') ) {
    //             to_picker_2.set('min', from_picker_2.get('select'))
    //         }
    //         if ( to_picker_2.get('value') ) {
    //             from_picker_2.set('max', to_picker_2.get('select'))
    //         }

    //         // When something is selected, update the “from” and “to” limits.
    //         from_picker_2.on('set', function(event) {
    //             if ( event.select ) {
    //             to_picker_2.set('min', from_picker_2.get('select'))    
    //             }
    //             else if ( 'clear' in event ) {
    //             to_picker_2.set('min', false)
    //             }
    //         })
    //         to_picker_2.on('set', function(event) {
    //             if ( event.select ) {
    //             from_picker_2.set('max', to_picker_2.get('select'))
    //             }
    //             else if ( 'clear' in event ) {
    //             from_picker_2.set('max', false)
    //             }
    //         })
    //     }

    // });

    });
    
    /*
    $(document).ready(function () {

        if($(".select-search").length != 0) 
        {
            $('.select-search').select2();
        }

        if($(".date").length != 0) 
        {
            $(".date").AnyTime_picker({
                format: "%d-%m-%Z",
                firstDOW: 1
            });
        }

        if($(".start_date").length != 0 && $(".end_date").length != 0) 
        {
            // Options
            var oneDay = 24*60*60*1000;
            var rangeDemoFormat = "%d-%m-%Z";
            var rangeDemoConv = new AnyTime.Converter({format:rangeDemoFormat});

            // Start date
            $(".start_date").AnyTime_picker({
                format: rangeDemoFormat
            });

            // On value change
            $(".start_date").change(function(e) {
                try {
                    var fromDay = rangeDemoConv.parse($(".start_date").val()).getTime();

                    var dayLater = new Date(fromDay+oneDay);
                        dayLater.setHours(0,0,0,0);

                    var ninetyDaysLater = new Date(fromDay+(90*oneDay));
                        ninetyDaysLater.setHours(23,59,59,999);

                    // End date
                    $(".end_date")
                    .AnyTime_noPicker()
                    .removeAttr("disabled")
                    .val(rangeDemoConv.format(dayLater))
                    .AnyTime_picker({
                        earliest: dayLater,
                        format: rangeDemoFormat,
                        latest: ninetyDaysLater
                    });
                }

                catch(e) {

                    // Disable End date field
                    $(".end_date").val("").attr("disabled","disabled");
                }
            });
        }
    });
    */

     // Checkbox Plugin
  $("#jstree_checkbox").jstree(
    {
        "checkbox" : {
            "keep_selected_style" : false,
            "three_state" : false,
        },
        "plugins" : [ "checkbox", "search" ]
    }).bind('check_node.jstree', function(e, data) 
    {
        var currentNode = data.rslt.obj.attr("id");
        var parentNode = data.inst._get_parent(data.rslt.obj).attr("id");
        jQuery.jstree._reference($("#jstree_checkbox")).check_node('#'+parentNode);
    });
  
    var to = false;
      $('#jstree_checkbox_search').keyup(function () 
      {
        if(to) { clearTimeout(to); }
        to = setTimeout(function () {
          var v = $('#jstree_checkbox_search').val();
          $('#jstree_checkbox').jstree(true).search(v);
        }, 250);
      });
  
    $('#jstree_checkbox').on("changed.jstree", function (e, data) 
    {
      document.getElementById('jsfields').value = data.selected;
    });
  
    if($( '#jsfields' ).length)
    {
        var val = $( '#jsfields' ).val();
        var valArray = val.split(",");
  
        var setList = [];
        for (i = 0; i < valArray.length; i++) { 
            setList.push("#" + valArray[i]);
        }
  
        $('#jstree_checkbox').jstree("check_node", setList);
    }

    if($("#updates").length != 0) 
    {
        $.ajax({
        type: "POST",
        url: baseUrl+"Applicants/getUpdateByStaffId/",
        success: function(result)
        {
            var $update = $('#updates');
            $update.empty();
            $.each(JSON.parse(result),function(key, value) 
            {
                $update.append(value);
            });

        }
        });
    }

    if($("#update_count").length != 0) 
      {
          $.ajax({
          type: "POST",
          url: baseUrl+"Applicants/getUpdateCountByStaffId/",
          success: function(result)
            {
              if(result > 0)
              {
                var $update = $('#update_count');
                $update.addClass("badge bg-danger-400");
                $update.text(result);
              }
            }
          });
      }

      if($("#notification_count").length != 0) 
      {
          $.ajax({
          type: "POST",
          url: baseUrl+"Notifications/getNotificationCountByStaffId/",
          success: function(result)
            {
              if(result > 0)
              {
                var $update = $('#notification_count');
                $update.addClass("badge bg-danger-400");
                $update.text(result);
              }
            }
          });
      }

      if($("#notifications").length != 0) 
      {
          $.ajax({
          type: "POST",
          url: baseUrl+"Notifications/getNotificationByStaffId/",
          success: function(result)
            {
                var $notification = $('#notifications');
                $notification.empty();
                $.each(JSON.parse(result),function(key, value) 
                {
                    $notification.append(value);
                });
            }
          });
      }
});

