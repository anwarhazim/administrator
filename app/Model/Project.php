<?php

App::uses('AuthComponent', 'Controller/Component');

class Project extends AppModel
{
	public $validate = array(
        'name' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'The Name is required.'
                )
			),
		'icon' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'The Icon is required.'
				)
			),
		'code' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'The Code is required.'
				)
			),
		'color' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'The Code is required.'
				)
			),	
		'url' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'The URL is required.'
				)
			),	
		'desc' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'The Description is required.'
				)
			),
	);

	public $belongsTo = array(
		'CreatedBy' => array(
			'className' => 'Employee',
			'fields' => array('personal_id', 'employee_no'),
			'foreignKey' => 'created_by',
		),
		'ModifiedBy' => array(
			'className' => 'Employee',
			'fields' => array('personal_id', 'employee_no'),
			'foreignKey' => 'modified_by',
		)
    );


    public function beforeSave($options = array())
	{
		if (!empty($this->data[$this->alias]['name']))
		{
			$this->data[$this->alias]['name'] = strtoupper($this->data[$this->alias]['name']);
		}

		// fallback to our parent
		return parent::beforeSave($options);
	}
}
