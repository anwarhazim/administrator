<?php

App::uses('AuthComponent', 'Controller/Component');

class Organisation extends AppModel 
{
    public $actsAs = array('Tree');

    public $validate = array(
        'name' => array(
                    'notBlank' => array(
                            'rule' => 'notBlank',
                            'message' => 'The Name field is required.'
                        )
                    ),
        'code' => array(
                    'notBlank' => array(
                            'rule' => 'notBlank',
                            'message' => 'The Code field is required.'
                        ),
                    'Numeric' => array(
                            'rule' => 'numeric',
                            'message' => 'The Code. must be in number. Please try again!',
                        ),
                    'Maxlength' => array(
                            'rule' => array('maxLength', 8),
                            'message' => 'Maximum 8 digits only in Code. Please try again!',
                        ),
                    'Minlength' => array(
                            'rule' => array('minLength', 8),
                            'message' => 'Minimum 8 digits only in Code. Please try again!',
                        ),
                    ),
        // 'organisation_category_id' => array(
        //             'notBlank' => array(
        //                     'rule' => 'notBlank',
        //                     'message' => 'Please select Category from the list'
        //                 )
        //             ),
    );

    public $belongsTo = array(
        'BatchOrganisation' => array(
			'className' => 'BatchOrganisation',
			'fields' => array('id','name', 'is_active'),
			'foreignKey' => 'batch_organisation_id',
        ),
    );
 
    public function beforeSave($options = array()) 
	{
        if (!empty($this->data[$this->alias]['name']))
		{
			$this->data[$this->alias]['name'] = strtoupper($this->data[$this->alias]['name']);
        }

		return parent::beforeSave($options);
    }
}