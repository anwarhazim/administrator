<?php

App::uses('AuthComponent', 'Controller/Component');

class Medical extends AppModel
{
	public $actsAs = array('Tree');

    // public $validate = array(
    //     'name' => array(
    //         'notBlank' => array(
    //                 'rule' => 'notBlank',
    //                 'message' => 'The Name field is required.'
    //             )
	// 		),
	// 	'gender_id' => array(
	// 		'notBlank' => array(
	// 				'rule' => 'notBlank',
	// 				'message' => 'Please select a Gender.'
	// 			),
	// 		),
    //     'medical_status_id' => array(
    //         'notBlank' => array(
    //                 'rule' => 'notBlank',
    //                 'message' => 'Please select a Status.'
    //             ),
    //         ),
    // );
    
    public $belongsTo = array(
        'Gender' => array(
			'className' => 'Gender',
			'fields' => array('id', 'name'),
			'foreignKey' => 'gender_id',
		),
        'Relationship' => array(
			'className' => 'Relationship',
			'fields' => array('id', 'name'),
			'foreignKey' => 'relationship_id',
		),
        'MedicalStatus' => array(
			'className' => 'MedicalStatus',
			'fields' => array('id', 'name'),
			'foreignKey' => 'medical_status_id',
		),
		'OrganisationType' => array(
			'className' => 'OrganisationType',
			'fields' => array('id', 'name'),
			'foreignKey' => 'organisation_type_id',
		),
		'CreatedBy' => array(
			'className' => 'Employee',
			'fields' => array('id', 'complete_name'),
			'foreignKey' => 'created_by',
		),
		'ModifiedBy' => array(
			'className' => 'Employee',
			'fields' => array('id', 'complete_name'),
			'foreignKey' => 'modified_by',
		)
    );
    
    public function beforeSave($options = array())
	{
        if (!empty($this->data[$this->alias]['name']))
		{
			$this->data[$this->alias]['name'] = strtoupper($this->data[$this->alias]['name']);
		}

		if (!empty($this->data[$this->alias]['dob']))
		{
			$this->data[$this->alias]['dob'] = date("Y-m-d", strtotime($this->data[$this->alias]['dob']));
        }
        
		// fallback to our parent
		return parent::beforeSave($options);
	}
}
