<?php

App::uses('AuthComponent', 'Controller/Component');

class Contact extends AppModel 
{
    public $actsAs = array('Tree');
    
    public $belongsTo = array(
		'Status' => array(
			'className' => 'Status',
			'fields' => array('name'),
			'foreignKey' => 'status_id',
		),
    );

    public function beforeSave($options = array()) 
	{
        if (!empty($this->data[$this->alias]['current_address_1']))
		{
			$this->data[$this->alias]['current_address_1'] = strtoupper($this->data[$this->alias]['current_address_1']);
        }

        if (!empty($this->data[$this->alias]['current_address_2']))
		{
			$this->data[$this->alias]['current_address_2'] = strtoupper($this->data[$this->alias]['current_address_2']);
        }

        if (!empty($this->data[$this->alias]['current_address_3']))
		{
			$this->data[$this->alias]['current_address_3'] = strtoupper($this->data[$this->alias]['current_address_3']);
        }
        
		return parent::beforeSave($options);
	}
}