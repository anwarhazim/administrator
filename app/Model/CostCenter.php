<?php

App::uses('AuthComponent', 'Controller/Component');

class CostCenter extends AppModel 
{
	public $validate = array(
        'name' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'The Name field is required.'
            ),
        ),
        'code' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'The Came field is required.'
            ),
        ),
        // 'company_code' => array(
        //     'notBlank' => array(
        //             'rule' => 'notBlank',
        //             'message' => 'The Company Code field is required.'
        //     ),
        // ),
    );
	
    public function beforeSave($options = array()) 
	{
		if (!empty($this->data[$this->alias]['name']))
		{
			$this->data[$this->alias]['name'] = strtoupper($this->data[$this->alias]['name']);
        }
        
		return parent::beforeSave($options);
	}
}