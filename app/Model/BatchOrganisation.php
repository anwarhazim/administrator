<?php

App::uses('AuthComponent', 'Controller/Component');

class BatchOrganisation extends AppModel 
{
    public $validate = array(
        'name' => array(
                    'notBlank' => array(
                            'rule' => 'notBlank',
                            'message' => 'Name field is required.'
                        )
                    ),
        'start_date' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'Start Date field is required.'
                )
            ),
    );

    public $belongsTo = array(
		'CreatedBy' => array(
			'className' => 'Employee',
			'foreignKey' => 'created_by',
        ),
        'ModifiedBy' => array(
			'className' => 'Employee',
			'foreignKey' => 'modified_by',
		),
	);
    
    public function beforeSave($options = array()) 
	{
        if (!empty($this->data[$this->alias]['name']))
		{
			$this->data[$this->alias]['name'] = strtoupper($this->data[$this->alias]['name']);
        }
        
        if (!empty($this->data[$this->alias]['start_date']))
		{
			$this->data[$this->alias]['start_date'] = date("Y-m-d", strtotime($this->data[$this->alias]['start_date']));
        }

        if (!empty($this->data[$this->alias]['end_date']))
		{
			$this->data[$this->alias]['end_date'] = date("Y-m-d", strtotime($this->data[$this->alias]['end_date']));
        }
		
		return parent::beforeSave($options);
    }
}