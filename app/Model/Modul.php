<?php

App::uses('AuthComponent', 'Controller/Component');

class Modul extends AppModel 
{
	public $actsAs = array('Tree');

	public $validate = array(
        'name' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'The Name field is required'
                )
        ),
        'path' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'This Path field is required'
                )
        ),
        'is_type' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'Please select a Type option'
                )
        ),
        'is_dropdown' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'Please select a Dropdown option'
                )
        ),
        'is_nav' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'Please select a Navigation option'
                )
        ),
        'is_full' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'Please select a Full option'
                )
        ),
        'is_active' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'Please select a Active option'
                )
        )
    );
	
    public function beforeSave($options = array()) 
	{		
		// fallback to our parent
		return parent::beforeSave($options);
	}
}