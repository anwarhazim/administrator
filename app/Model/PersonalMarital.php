<?php

App::uses('AuthComponent', 'Controller/Component');

class PersonalMarital extends AppModel 
{

	public $validate = array(
        'marital_id' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'Please select a Marital Status.'
            ),
        ),
    );
	
    public function beforeSave($options = array()) 
	{
		return parent::beforeSave($options);
	}
}