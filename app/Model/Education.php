<?php

App::uses('AuthComponent', 'Controller/Component');

class Education extends AppModel 
{
    public $actsAs = array('Tree');
    
    public $belongsTo = array(
        'EducationLevel' => array(
			'className' => 'EducationLevel',
			'fields' => array('id','name'),
			'foreignKey' => 'education_level_id',
        ),
		'Qualification' => array(
			'className' => 'Qualification',
			'fields' => array('id','name'),
			'foreignKey' => 'qualification_id',
        ),
		'Major' => array(
			'className' => 'Major',
			'fields' => array('id','name'),
			'foreignKey' => 'major_id',
        ),
		'Status' => array(
			'className' => 'Status',
			'fields' => array('id','name'),
			'foreignKey' => 'status_id',
		),
		'CreatedBy' => array(
			'className' => 'Employee',
			'foreignKey' => 'created_by',
		),
		'ModifiedBy' => array(
			'className' => 'Employee',
			'foreignKey' => 'modified_by',
		)
    );

    public $hasMany = array(
        'Attachment' => array(
			'className' => 'Attachment',
			'fields' => array('id', 'name', 'path', 'modul_id', 'key_id'),
			'conditions' => array('modul_id' => 8),
            'foreignKey' => 'key_id',
        )
    );

    public function create_NotBlank($files)
    {
        foreach ($files['attachments'] as $file) 
        {
            if(empty($file['name']))
            {
                return false;
                break;
            }
        }

        return true;
    }

    public function create_NotFormat($files)
    {
        $check = false;

        foreach ($files['attachments'] as $file) 
        {
            if(!empty($file['name']))
            {
                $file_parts = pathinfo($file['name']);
                $supportedFileTypes =  array('gif', 'jpg', 'png', 'jpeg', 'bmp');
                if(!in_array(strtolower($file_parts['extension']), $supportedFileTypes)) 
                {
                    $check = false;
                    break;
                }

                $check = true;
            }
            else
            {
                $check = false;
            }
        }

        return $check;
    }

    public function update_NotFormat($files)
    {
        $check = true;

        foreach ($files['attachments'] as $file) 
        {
            if(!empty($file['name']))
            {
                $file_parts = pathinfo($file['name']);
                $supportedFileTypes =  array('gif', 'jpg', 'png', 'jpeg', 'bmp');
                if(!in_array(strtolower($file_parts['extension']), $supportedFileTypes)) 
                {
                    $check = false;
                    break;
                }

                $check = true;
            }
        }

        return $check;
    }

    public function NotSize($files)
    {
        $totalFileSize = 0;
        foreach ($files['attachments'] as $file) 
        {
            if(!empty($file['size']))
            {
                $totalFileSize = $totalFileSize + $file['size'];
            }
        }

        $maxFileSize = 20 * 1024 * 1024 /* 20MB */;        

        if ($totalFileSize >= $maxFileSize) 
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public function beforeSave($options = array()) 
	{
		if (!empty($this->data[$this->alias]['institution']))
		{
			$this->data[$this->alias]['institution'] = strtoupper($this->data[$this->alias]['institution']);
        }
        
        if (!empty($this->data[$this->alias]['duration_from']))
		{
			$this->data[$this->alias]['duration_from'] = date("Y-m-d", strtotime($this->data[$this->alias]['duration_from']));
        }
        
        if (!empty($this->data[$this->alias]['duration_to']))
		{
			$this->data[$this->alias]['duration_to'] = date("Y-m-d", strtotime($this->data[$this->alias]['duration_to']));
        }
        
		return parent::beforeSave($options);
	}
}