<?php

App::uses('AuthComponent', 'Controller/Component');

class UserRole extends AppModel 
{
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'fields' => array('id','username','status_id','is_active', 'modified', 'created'),
			'foreignKey' => 'user_id',
		),
		'Role' => array(
			'className' => 'Role',
			'fields' => array('name'),
			'foreignKey' => 'role_id',
		),
	);

	public function getStaffByRole($role_id = null)
	{
		//$Staff = ClassRegistry::init('Staff');

		$data = array();

		// $hristeams = $this->find('all', array(
		// 								'conditions' => array('User.is_active' => 1, 'UserRole.role_id ' => $role_id)
		// 							));
		
		// if(!empty($hristeams))
		// {
		// 	$i = 0;
		// 	foreach ($hristeams as $hristeam) 
		// 	{
		// 		$staff = $Staff->find('first', array(
		// 									'conditions' => array('Staff.is_active' => 1, 'Staff.user_id ' => $hristeam['User']['id'])
		// 								));
				
		// 		$data['HRIS'][$i]['id'] = $staff['Staff']['id'];
		// 		$data['HRIS'][$i]['email'] = $staff['Staff']['email'];
				
		// 		$i++;
		// 	}
		// }

		return $data;
	}

	public function getRoleByUserId($user_id = null)
	{
		//$Staff = ClassRegistry::init('Staff');

		$data = array();

		// $roles = $this->find('all', array(
		// 								'conditions' => array('User.is_active' => 1, 'UserRole.user_id ' => $user_id)
		// 							));
		
		// if(!empty($roles))
		// {
		// 	$i = 0;
		// 	foreach ($roles as $role) 
		// 	{				
		// 		$data[$i] = $role['UserRole']['role_id'];
		// 		$i++;
		// 	}
		// }

		return $data;
	}

	public function beforeSave($options = array()) 
	{
		return parent::beforeSave($options);
	}

}