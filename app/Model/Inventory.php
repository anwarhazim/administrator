<?php

App::uses('AuthComponent', 'Controller/Component');

class Inventory extends AppModel
{
	public $validate = array(
        'name' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'The Name field is required.'
                )
			),
		'disciple_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select Disciple.'
				)
			),
		'category_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select Category.'
				)
			),
	);

	public $belongsTo = array(
		'Disciple' => array(
			'className' => 'Disciple',
			'fields' => array('id', 'name'),
			'foreignKey' => 'disciple_id',
		),
		'Category' => array(
			'className' => 'Category',
			'fields' => array('id', 'name'),
			'foreignKey' => 'category_id',
		),
		// 'CreatedBy' => array(
		// 	'className' => 'Staff',
		// 	'fields' => array('id', 'name'),
		// 	'foreignKey' => 'created_by',
		// ),
		// 'ModifiedBy' => array(
		// 	'className' => 'Staff',
		// 	'fields' => array('id', 'name'),
		// 	'foreignKey' => 'modified_by',
		// )
	);

	public $hasMany = array(
        'Documents' => array(
            'className' => 'Document',
            'foreignKey' => 'inventory_id',
        )
    );

    public function beforeSave($options = array())
	{
		if (!empty($this->data[$this->alias]['name']))
		{
			$this->data[$this->alias]['name'] = strtoupper($this->data[$this->alias]['name']);
		}

		// fallback to our parent
		return parent::beforeSave($options);
	}
}
