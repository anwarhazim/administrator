<?php

App::uses('AuthComponent', 'Controller/Component');

class Death extends AppModel 
{
    public $actsAs = array('Tree');
    
	public $validate = array(
        'name' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'The Name field is required.'
            ),
        ),
        'ic_no' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'The IC No. field is required.'
			),
			'Numeric' => array(
				'rule' => 'numeric',
				'message' => 'Please insert number only in IC No. Please try again!',
			),
			'Maxlength' => array(
				'rule' => array('maxLength', 12),
				'message' => 'Maximum 12 digits only in IC No. Please try again!',
			),
			'Minlength' => array(
				'rule' => array('minLength', 12),
				'message' => 'Minimum 12 digits only in IC No. Please try again!',
			),
			'Unique'    => array(
				'rule'	=> array('create_icNo'),
				'on' => 'create',
				'message' => 'IC No. already been used. Please try again!',
				'last' => false,
			),
			'Update' => array(
				'on' => 'update',
				'rule' => array('update_icNo'),
				'message' => 'IC No. already been used. Please try again!',
				'last' => false,
			),
		),
        'relationship_id' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'Please select a Relationship.'
            ),
        ),
        'date_of_death' => array(
            'notBlank' => array(
                    'rule' => 'notBlank',
                    'message' => 'The Date of Death field is required.'
            ),
        ),
        'attachments' => array(
			'CreateNotBlank'    => array(
                'rule'      => array('create_NotBlank'),
                'on' => 'create',
                'message' => 'Please specify a file to upload.',
				'last' => false,
            ),
            'CreateNotFormat'    => array(
                'rule'      => array('create_NotFormat'),
                'on' => 'create',
                'message' => 'Your file format is invalid. Only .gif, .bmp, .jpeg, .jpg and .png files are allowed. Please try again!
                ',
				'last' => false,
			),
            'UpdateNotFormat'    => array(
                'rule'      => array('update_NotFormat'),
                'on' => 'update',
                'message' => 'Your file format is invalid. Only .gif, .bmp, .jpeg, .jpg and .png files are allowed. Please try again!
                ',
				'last' => false,
			),
            'NotSize'    => array(
                'rule'      => array('NotSize'),
                'message' => 'Your file must not exceed 20MB. Please try again!
                ',
				'last' => false,
			),
		),
    );
    
    public $belongsTo = array(
		'Relationship' => array(
			'className' => 'Relationship',
			'fields' => array('name'),
			'foreignKey' => 'relationship_id',
		),
		'Status' => array(
			'className' => 'Status',
			'fields' => array('name'),
			'foreignKey' => 'status_id',
		),
		'DeletedBy' => array(
			'className' => 'Employee',
			'fields' => array('complete_name'),
			'foreignKey' => 'deleted_by',
		),
		'CreatedBy' => array(
			'className' => 'Employee',
			'fields' => array('complete_name'),
			'foreignKey' => 'created_by',
		),
		'ModifiedBy' => array(
			'className' => 'Employee',
			'fields' => array('complete_name'),
			'foreignKey' => 'modified_by',
		)
    );

    public $hasMany = array(
        'Attachment' => array(
			'className' => 'Attachment',
			'fields' => array('id', 'name', 'path', 'modul_id', 'key_id'),
			'conditions' => array('modul_id' => 26),
            'foreignKey' => 'key_id',
        )
    );

    public function create_icNo()
	{
		return ($this->find('count', array('conditions' =>array('Death.ic_no' => $this->data[$this->alias]['ic_no'], 'Death.is_active' => 1, 'Death.is_deleted' => 99))) == 0);
	}

	public function update_icNo() 
	{
        return ($this->find('count', array('conditions' =>array('Death.ic_no' => $this->data[$this->alias]['ic_no'], 'Death.id !=' => $this->data[$this->alias]['id'], 'Death.is_active' => 1, 'Death.is_deleted' => 99))) == 0);
	}

    public function create_NotBlank($files)
    {
        foreach ($files['attachments'] as $file) 
        {
            if(empty($file['name']))
            {
                return false;
                break;
            }
        }

        return true;
    }

    public function create_NotFormat($files)
    {
        $check = false;

        foreach ($files['attachments'] as $file) 
        {
            if(!empty($file['name']))
            {
                $file_parts = pathinfo($file['name']);
                $supportedFileTypes =  array('gif', 'jpg', 'png', 'jpeg', 'bmp');
                if(!in_array(strtolower($file_parts['extension']), $supportedFileTypes)) 
                {
                    $check = false;
                    break;
                }

                $check = true;
            }
            else
            {
                $check = false;
            }
        }

        return $check;
    }

    public function update_NotFormat($files)
    {
        $check = true;

        foreach ($files['attachments'] as $file) 
        {
            if(!empty($file['name']))
            {
                $file_parts = pathinfo($file['name']);
                $supportedFileTypes =  array('gif', 'jpg', 'png', 'jpeg', 'bmp');
                if(!in_array(strtolower($file_parts['extension']), $supportedFileTypes)) 
                {
                    $check = false;
                    break;
                }

                $check = true;
            }
        }

        return $check;
    }

    public function NotSize($files)
    {
        $totalFileSize = 0;
        foreach ($files['attachments'] as $file) 
        {
            if(!empty($file['size']))
            {
                $totalFileSize = $totalFileSize + $file['size'];
            }
        }

        $maxFileSize = 20 * 1024 * 1024 /* 20MB */;        

        if ($totalFileSize >= $maxFileSize) 
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public function findIfUpdateByStaffId($staff_id = null)
    {
        $update = true;

        $data = $this->find('count', array(
                                'conditions' => array(
                                    'Death.staff_id' => $staff_id,
                                    'Death.is_active' => 1,
                                    'NOT' => array( 'Death.status_id' => array(1,2,10) )
                                ),
                            ));

        if($data > 0)
        {
            $update = false;
        }

        return $update;
    }
	
    public function beforeSave($options = array()) 
	{
		if (!empty($this->data[$this->alias]['name']))
		{
			$this->data[$this->alias]['name'] = strtoupper($this->data[$this->alias]['name']);
        }
        
        if (!empty($this->data[$this->alias]['date_of_death']))
		{
			$this->data[$this->alias]['date_of_death'] = date("Y-m-d", strtotime($this->data[$this->alias]['date_of_death']));
        }
        
		return parent::beforeSave($options);
	}
}