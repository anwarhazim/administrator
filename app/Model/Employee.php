<?php

App::uses('AuthComponent', 'Controller/Component');

class Employee extends AppModel
{
	public $actsAs = array('Tree');

	public $validate = array(
		'employee_no' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'The Employee No. field is required.'
				),
			'Numeric' => array(
				'rule' => 'numeric',
				'message' => 'The Employee No. field can only contain numbers. Please try again!',
				),
			'Maxlength' => array(
				'rule' => array('maxLength', 8),
				'message' => 'Maximum 8 digits only in Employee No. Please try again!',
				),
			'Minlength' => array(
				'rule' => array('minLength', 8),
				'message' => 'Minimum 8 digits only in Employee No. Please try again!',
				),
			'Unique'    => array(
				'rule'	=> array('create_EmployeeNo'),
				'on' => 'create',
				'message' => 'Employee No. already been used. Please try again!',
				'last' => false,
				),
			'Update' => array(
				'on' => 'update',
				'rule' => array('update_EmployeeNo'),
				'message' => 'Employee No. already been used. Please try again!',
				'last' => false,
				),
			),
		'is_type' => array(
			'notBlank' => array(
						'rule' => 'notBlank',
						'message' => 'Please enter is_type.'
				),
			),
		// 'is_flag' => array(
		// 	'notBlank' => array(
		// 				'rule' => 'notBlank',
		// 				'message' => 'Please enter is_flag.'
		// 		),
		// 	),
		// 'is_active' => array(
		// 	'notBlank' => array(
		// 				'rule' => 'notBlank',
		// 				'message' => 'Please enter is_active.'
		// 		),
		// 	),
		'status_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select a Status.'
				),
			),
		'employment_status_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select a Employment Status.'
				),
			),
		'employee_group_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select a Employee Group.'
				),
			),
		'employee_subgroup_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select a Employee Subgroup.'
				),
			),
		'personal_area_id' => array(
				'notBlank' => array(
						'rule' => 'notBlank',
						'message' => 'Please select a Personal Area.'
					),
				),
		'personal_subarea_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select a Personal Subarea.'
				),
			),
		'payroll_area_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select a Payroll Area.'
				),
			),
		'entry_date' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please enter Entry Date.'
				),
			),
		'seniority_date' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please enter Seniority Date.'
				),
			),
		'organizational_unit_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select an Organisation Unit.'
				),
			),
		'organizational_unit_code' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please enter Organisation Unit Code.'
				),
			),
		'cost_center_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select a Cost Center.'
				),
			),
		'cost_center_code' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please enter Cost Center Code.'
				),
			),
		// 'position_id' => array(
		// 	'notBlank' => array(
		// 			'rule' => 'notBlank',
		// 			'message' => 'Please select a Position.'
		// 		),
		// 	),
		// 'position_code' => array(
		// 	'notBlank' => array(
		// 			'rule' => 'notBlank',
		// 			'message' => 'Please enter Position Code.'
		// 		),
		// 	),
		'job_designation_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select a Job Designation.'
				),
			),	
		'job_grade_id' => array(
			'notBlank' => array(
					'rule' => 'notBlank',
					'message' => 'Please select a Job Grade.'
				),
			),
	);

	public $belongsTo = array(
		'Personal' => array(
			'className' => 'Personal',
			'foreignKey' => 'personal_id',
		),
		'Organisation' => array(
			'className' => 'Organisation',
			'foreignKey' => 'organizational_unit_id',
		),
		'PersonalArea' => array(
			'className' => 'PersonalArea',
			'foreignKey' => 'personal_area_id',
		),
		'PersonalSubarea' => array(
			'className' => 'PersonalSubarea',
			'foreignKey' => 'personal_subarea_id',
		),
		'EmployeeGroup' => array(
			'className' => 'EmployeeGroup',
			'foreignKey' => 'employee_group_id',
		),
		'EmployeeSubgroup' => array(
			'className' => 'EmployeeSubgroup',
			'foreignKey' => 'employee_subgroup_id',
		),
		'PayrollArea' => array(
			'className' => 'PayrollArea',
			'foreignKey' => 'payroll_area_id',
		),
		'Status' => array(
			'className' => 'Status',
			'foreignKey' => 'status_id',
		),
		'CostCenter' => array(
			'className' => 'CostCenter',
			'foreignKey' => 'cost_center_id',
		),
		'JobDesignation' => array(
			'className' => 'JobDesignation',
			'foreignKey' => 'job_designation_id',
		),
		'JobGrade' => array(
			'className' => 'JobGrade',
			'foreignKey' => 'job_grade_id',
		),
	);


	public function create_EmployeeNo()
	{
		return ($this->find('count', array('conditions' =>array('Employee.employee_no' => $this->data[$this->alias]['employee_no'], 'Employee.is_active' => 1))) == 0);
	}

	public function update_EmployeeNo()
	{
        return ($this->find('count', array('conditions' =>array('Employee.employee_no' => $this->data[$this->alias]['employee_no'], 'Employee.id !=' => $this->data[$this->alias]['id'], 'Employee.is_active' => 1))) == 0);
	}

	public function beforeSave($options = array())
	{
		if (!empty($this->data[$this->alias]['entry_date']))
		{
			$this->data[$this->alias]['entry_date'] = date("Y-m-d", strtotime($this->data[$this->alias]['entry_date']));
		}

		if (!empty($this->data[$this->alias]['leaving_date']))
		{
			$this->data[$this->alias]['leaving_date'] = date("Y-m-d", strtotime($this->data[$this->alias]['leaving_date']));
		}

		if (!empty($this->data[$this->alias]['seniority_date']))
		{
			$this->data[$this->alias]['seniority_date'] = date("Y-m-d", strtotime($this->data[$this->alias]['seniority_date']));
		}

		return parent::beforeSave($options);
	}

}
