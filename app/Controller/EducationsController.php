<?php
class EducationsController extends AppController 
{

	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter() 
    {
        parent::beforeFilter();
    }
    
    public function upload()
    {
        $this->loadModel('Personal');
        $this->loadModel('Employee');
        $this->loadModel('Education');
        $this->loadModel('EducationLevel');
        $this->loadModel('Qualification');
        $this->loadModel('Major');
        $this->loadModel('Country');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        if($this->request->is('post') || $this->request->is('put'))
        {
            App::import('Vendor', 'Spreadsheet_Excel_Reader', array('file' => 'excelreader'.DS.'excel_reader.php'));
            $excel = new PhpExcelReader; 

            $data = $this->request->data;

            if($data['Education']['attachment']['error'] == 0)
            {
                $excel->read($data['Education']['attachment']['tmp_name']);
                $cells = $excel->sheets[0]['cells'];
                $counter = 0;
                for ($i=2; $i <= count($cells); $i++) 
                {
                    $employee_no = isset($cells[$i][1]) ? $cells[$i][1] : '';
                    $duration_from = isset($cells[$i][2]) ? $cells[$i][2] : '';
                    $duration_to = isset($cells[$i][3]) ? $cells[$i][3] : '';
                    $institution = isset($cells[$i][4]) ? $cells[$i][4] : '';
                    $country = isset($cells[$i][5]) ? $cells[$i][5] : '';
                    $qualification = isset($cells[$i][6]) ? $cells[$i][6] : '';
                    $major = isset($cells[$i][7]) ? $cells[$i][7] : '';
                    $result = isset($cells[$i][9]) ? $cells[$i][9] : '';

                    $employee_information = $this->Employee->find('first',
                                                array(
                                                    'conditions' => array(
                                                                        'Employee.employee_no' => $employee_no,
                                                                        'Employee.is_active' => 1,
                                                                    ),
                                                ));

                    if(!empty($employee_information))
                    {
                        $education = array();
                        $education['Education']['personal_id'] = $employee_information['Employee']['personal_id'];
                        $education['Education']['is_checked'] = 99;
                        $education['Education']['is_deleted'] = 99;
                        $education['Education']['is_type'] = 1;
                        $education['Education']['is_flag'] = 1;
                        $education['Education']['is_active'] = 1;
                        $education['Education']['status_id'] = 10;

                        if(!empty($duration_from))
                        {
                            $duration_from = $this->Utility->getDateFormatFromString($duration_from);

                            $education['Education']['duration_from'] = date("Y-m-d", strtotime($duration_from));
                        }

                        if(!empty($duration_to))
                        {
                            $duration_to = $this->Utility->getDateFormatFromString($duration_to);

                            $education['Education']['duration_to'] = date("Y-m-d", strtotime($duration_to));
                        }

                        $education['Education']['institution'] = $institution;

                        if(!empty($country))
                        {
                            $country_information = $this->Country->find('first',
                                                                            array(
                                                                                'conditions' => array(
                                                                                                    'Country.name' => $country,
                                                                                                ),
                                                                            ));

                            if(!empty($country_information))
                            {
                                $education['Education']['country_id'] = $country_information['Country']['id'];
                            }
                        }

                        if(!empty($qualification))
                        {
                            $qualification_information = $this->Qualification->find('first',
                                                                                        array(
                                                                                            'conditions' => array(
                                                                                                                'Qualification.name' => $qualification,
                                                                                                            ),
                                                                                        ));

                            if(!empty($qualification_information))
                            {
                                $education['Education']['education_level_id'] = $qualification_information['Qualification']['education_level_id'];
                                $education['Education']['qualification_id'] = $qualification_information['Qualification']['id'];
                            }
                        }

                        if(!empty($major))
                        {
                            $major_information = $this->Major->findById($major);
                            if(!empty($major_information))
                            {
                                $education['Education']['major_id'] = $major_information['Major']['id'];
                            }
                        }

                        if(!empty($result))
                        {
                            $education['Education']['result'] = $result;
                        }

                        $education['Education']['created'] = date('Y-m-d H:i:s');
                        $education['Education']['modified'] = date('Y-m-d H:i:s');

                        $this->Education->create();
                        $this->Education->save($education);
                    }
                }


                $logs = array();
                $logs['Log']['employee_id'] = $employee['Employee']['id'];
                $logs['Log']['action_id'] = '3'; // add
                $logs['Log']['path'] = $this->here; //get current path
                $logs['Log']['project_id'] = '2'; //set project id
                $logs['Log']['created_by'] = $employee['Employee']['id'];
                $logs['Log']['created'] = date('Y-m-d H:i:s');
                $logs['Log']['modified_by'] = $employee['Employee']['id'];
                $logs['Log']['modified'] = date('Y-m-d H:i:s');

                $this->Log->create();
                $this->Log->save($logs);

                $this->Session->setFlash('Information successfully upload.', 'success');
                $this->redirect(array('action' => 'upload'));
            }
        }
    }
}