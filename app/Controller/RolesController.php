<?php
App::uses('AppController', 'Controller');

class RolesController extends AppController 
{
	public $components = array('RequestHandler', 'Paginator', 'Session');
	public $helpers = array('Html', 'Form', 'Session');
	
	public function index()
	{
        $this->loadModel('Employee');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        $conditions = array();

        $conditions['order'] = array('Role.id'=> 'ASC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Role'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters,
            // we'll redirect to that page
            return $this->redirect($filter_url);
        }
        else
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "search")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Role.name LIKE' => '%' . $value . '%')
                        );
                    }

					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Role.modified) >=' => date("Y-m-d", strtotime($value))
                        );

                    }

                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Role.modified) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc
                    $this->request->data['Role'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate();

        for ($i=0; $i < count($details); $i++)
        {
            $details[$i]['Role']['modified'] = date("d-m-Y",strtotime($details[$i]['Role']['modified']));

            $details[$i]['Role']['created'] = date("d-m-Y",strtotime($details[$i]['Role']['created']));

            $details[$i]['Role']['id'] = $this->Utility->encrypt($details[$i]['Role']['id'], 'rOlE@');
        }

        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // view
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '2'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['created'] = date('Y-m-d H:i:s');
        $logs['Log']['modified_by'] = $employee['Employee']['id'];
        $logs['Log']['modified'] = date('Y-m-d H:i:s');

        $this->Log->create();
        $this->Log->save($logs);

        $this->set(compact('details'));
	}

	public function add()
	{
        $this->loadModel('Employee');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);
        
		if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

			$this->Role->set($data);
            if($this->Role->validates())
            {
				$data['Role']['modified_by'] = $employee['Employee']['id'];
				$data['Role']['modified'] = date('Y-m-d H:i:s');
				$data['Role']['created_by'] = $employee['Employee']['id'];
				$data['Role']['created'] = date('Y-m-d H:i:s');

                $this->Role->create();
                $this->Role->save($data);
                
                $logs = array();
                $logs['Log']['employee_id'] = $employee['Employee']['id'];
                $logs['Log']['action_id'] = '3'; // add
                $logs['Log']['path'] = $this->here; //get current path
                $logs['Log']['project_id'] = '2'; //set project id
                $logs['Log']['created_by'] = $employee['Employee']['id'];
                $logs['Log']['created'] = date('Y-m-d H:i:s');
                $logs['Log']['modified_by'] = $employee['Employee']['id'];
                $logs['Log']['modified'] = date('Y-m-d H:i:s');

                $this->Log->create();
                $this->Log->save($logs);

                $this->Session->setFlash('Information successfully saved.', 'success');
                $this->redirect(array('action' => 'add'));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully saved.', 'error');
            }
        }

        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // view
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '2'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['created'] = date('Y-m-d H:i:s');
        $logs['Log']['modified_by'] = $employee['Employee']['id'];
        $logs['Log']['modified'] = date('Y-m-d H:i:s');

        $this->Log->create();
        $this->Log->save($logs);

	}

	public function view($key = null)
	{
        $this->loadModel('Employee');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'rOlE@');

        $detail = $this->Role->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

		$detail['Role']['modified'] = date("d-m-Y",strtotime($detail['Role']['modified']));
		$detail['Role']['created'] = date("d-m-Y",strtotime($detail['Role']['created']));

		$this->request->data = $detail;
		
        $disabled = "disabled";
        
        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // view
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '2'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['created'] = date('Y-m-d H:i:s');
        $logs['Log']['modified_by'] = $employee['Employee']['id'];
        $logs['Log']['modified'] = date('Y-m-d H:i:s');

        $this->Log->create();
        $this->Log->save($logs);

        $this->set(compact('key', 'disabled'));
	}

	public function edit($key = null)
	{
		$this->loadModel('Employee');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'rOlE@');

        $detail = $this->Role->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
		}

		if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $data['Role']['id'] = $id;

			$this->Role->set($data);
            if($this->Role->validates())
            {
				$data['Role']['modified_by'] = $employee['Employee']['id'];
				$data['Role']['modified'] = date('Y-m-d H:i:s');

                $this->Role->create();
                $this->Role->save($data);
                
                $logs = array();
                $logs['Log']['employee_id'] = $employee['Employee']['id'];
                $logs['Log']['action_id'] = '4'; // edit
                $logs['Log']['path'] = $this->here; //get current path
                $logs['Log']['project_id'] = '2'; //set project id
                $logs['Log']['created_by'] = $employee['Employee']['id'];
                $logs['Log']['created'] = date('Y-m-d H:i:s');
                $logs['Log']['modified_by'] = $employee['Employee']['id'];
                $logs['Log']['modified'] = date('Y-m-d H:i:s');

                $this->Log->create();
                $this->Log->save($logs);

                $this->Session->setFlash('Information successfully updated.', 'success');
                $this->redirect(array('action' => 'edit/'.$key));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully updated.', 'error');
            }
        }
        else
        {
            $this->request->data = $detail;
        }
		
        $disabled = "";
        
        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // view
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '2'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['created'] = date('Y-m-d H:i:s');
        $logs['Log']['modified_by'] = $employee['Employee']['id'];
        $logs['Log']['modified'] = date('Y-m-d H:i:s');

        $this->Log->create();
        $this->Log->save($logs);

        $this->set(compact('key', 'disabled'));
		
    }
    
    public function setting($key = null, $project_selected = null)
    {
        $this->loadModel('Modul');
        $this->loadModel('Setting');
        $this->loadModel('Project');
        $this->loadModel('Utility');
        $this->loadModel('Employee');
        $this->loadModel('Log');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if(empty($project_selected))
        {
            $project_selected = 1;
        }

        $id = $this->Utility->decrypt($key, 'rOlE@');

        $detail = $this->Role->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }
        else
        {
            if ($this->request->is('post') || $this->request->is('put')) 
            {
                $jsfields = $this->request->data('jsfields');

                $settings = $this->Setting->find('all',
                            array(
                                'conditions' => array(
                                                        'Setting.role_id' => $detail['Role']['id'],
                                                        'Setting.project_id' => $project_selected
                                                    ),
                            ));
                
                if($settings)
                {
                    $this->Setting->deleteAll(array('Setting.role_id' => $id, 'Setting.project_id' => $project_selected), false);
                }

                if($jsfields)
                {

                    $jsArray = explode(',', $jsfields);

                    foreach ($jsArray as $modul_id) 
                    {
                        $setting = array();
                        $setting['Setting']['role_id'] = $id;
                        $setting['Setting']['modul_id'] = $modul_id;
                        $setting['Setting']['project_id'] = $project_selected;

                        $this->Setting->create();
                        $this->Setting->save($setting);
                    }
                }

                $logs = array();
                $logs['Log']['employee_id'] = $employee['Employee']['id'];
                $logs['Log']['action_id'] = '4'; // edit
                $logs['Log']['path'] = $this->here; //get current path
                $logs['Log']['project_id'] = '2'; //set project id
                $logs['Log']['created_by'] = $employee['Employee']['id'];
                $logs['Log']['created'] = date('Y-m-d H:i:s');
                $logs['Log']['modified_by'] = $employee['Employee']['id'];
                $logs['Log']['modified'] = date('Y-m-d H:i:s');

                $this->Log->create();
                $this->Log->save($logs);

                $this->Session->setFlash('Information successfully updated.', 'success');
                $this->redirect(array('action' => 'setting/'.$key.'/'.$project_selected));
            }
        }

        $moduls = $this->Modul->find('threaded',
                                            array(
                                                'conditions' => array(
                                                                    'Modul.is_active' => 1,
                                                                    'Modul.project_id' => $project_selected
                                                                ),
                                                'contain' => false,
                                                'order' => array('Modul.order ASC'),
                                        ));

        $tree = '';
        $setList = '';
        $sets = '';

        $tree = $this->Utility->getTreeModul($moduls, count($moduls), '');

        $settings = $this->Setting->find('list',
                                            array(
                                            'conditions' => array(
                                                                    'Setting.role_id' => $id,
                                                                    'Setting.project_id' => $project_selected
                                                                ),
                                            'fields' => 'Setting.modul_id',
                                        ));

        foreach ($settings as $id)
        {
            $setList .= $sets . '' . $id . '';
            $sets = ',';
        }

        $projects = $this->Project->find('list');

        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // view
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '2'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['created'] = date('Y-m-d H:i:s');
        $logs['Log']['modified_by'] = $employee['Employee']['id'];
        $logs['Log']['modified'] = date('Y-m-d H:i:s');

        $this->Log->create();
        $this->Log->save($logs);

        $this->set(compact('key', 'projects', 'project_selected', 'tree', 'setList', 'detail'));
    }

	public function delete($key = null)
	{
		$this->loadModel('Personal');
        $this->loadModel('Employee');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

		if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'rOlE@');

        $detail = $this->Role->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
		}
		else
		{
			if($this->Role->delete($id))
			{
                $logs = array();
                $logs['Log']['employee_id'] = $employee['Employee']['id'];
                $logs['Log']['action_id'] = '5'; // delete
                $logs['Log']['path'] = $this->here; //get current path
                $logs['Log']['project_id'] = '2'; //set project id
                $logs['Log']['created_by'] = $employee['Employee']['id'];
                $logs['Log']['created'] = date('Y-m-d H:i:s');
                $logs['Log']['modified_by'] = $employee['Employee']['id'];
                $logs['Log']['modified'] = date('Y-m-d H:i:s');

                $this->Log->create();
                $this->Log->save($logs);

				$this->Session->setFlash('Information successfully deleted.', 'success');
				$this->redirect(array('action' => '/'));
			}
			else
			{
				$this->Session->setFlash('Error! Information not successfully deleted. Please try again!', 'error');
				$this->redirect(array('action' => '/'));
			}
		}
		
	}
}
