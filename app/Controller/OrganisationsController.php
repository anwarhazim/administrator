<?php
class OrganisationsController extends AppController 
{

	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter() 
    {
        parent::beforeFilter();
        //$this->Auth->allow('index', 'add', 'edit', 'view', 'upload', 'delete', 'active', 'lists', 'list_view', 'list_edit', 'list_delete', 'coordination', 'list_coordination', 'setup_coordination', 'setup_coordination');
	}
	
    public function index()
    {
        $this->loadModel('BatchOrganisation');
        $this->loadModel('Personal');
        $this->loadModel('Employee');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        $conditions = array();

        $conditions['conditions'][] = array();

        $conditions['order'] = array('BatchOrganisation.created'=> 'DESC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['BatchOrganisation'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters, 
            // we'll redirect to that page
            return $this->redirect($filter_url);
        } 
        else 
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "search")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('BatchOrganisation.name LIKE' => '%' . $value . '%')
                        );
                    } 
                    
					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(BatchOrganisation.start_date) >=' => date("Y-m-d", strtotime($value))
                        );

                    }
					
                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(BatchOrganisation.end_date) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc                 
                    $this->request->data['BatchOrganisation'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate('BatchOrganisation');

        $is_actives = array(
                            1 => "<span class='label label-success'>Active</span>",
                            99 => "<span class='label label-default'>Inactive</span>"
                        );

        for ($i=0; $i < count($details); $i++) 
        { 
            if(!empty($details[$i]['BatchOrganisation']['start_date']))
            {
                $details[$i]['BatchOrganisation']['start_date'] = date("d-m-Y",strtotime($details[$i]['BatchOrganisation']['start_date']));
            }
            else
            {
                $details[$i]['BatchOrganisation']['start_date'] = '-';
            }

            if(!empty($details[$i]['BatchOrganisation']['end_date']))
            {
                $details[$i]['BatchOrganisation']['end_date'] = date("d-m-Y",strtotime($details[$i]['BatchOrganisation']['end_date']));
            }
            else
            {
                $details[$i]['BatchOrganisation']['end_date'] = '-';
            }

            if(!empty($details[$i]['BatchOrganisation']['is_active']))
            {
                $details[$i]['BatchOrganisation']['status'] = $is_actives[$details[$i]['BatchOrganisation']['is_active']];
            }
            
            $details[$i]['BatchOrganisation']['id'] = $this->Utility->encrypt($details[$i]['BatchOrganisation']['id'], 'bThOrG');
		}

        $this->set(compact('details'));
    }

    public function add()
    {
        $this->loadModel('BatchOrganisation');
        $this->loadModel('Personal');
        $this->loadModel('Employee');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $this->BatchOrganisation->set($data);
            if($this->BatchOrganisation->validates())
            {
                $data['BatchOrganisation']['is_active'] = 99;
                // $data['BatchOrganisation']['created_by'] = $staff['Staff']['id'];
                // $data['BatchOrganisation']['modified_by'] = $staff['Staff']['id'];

                $this->BatchOrganisation->create();
                $this->BatchOrganisation->save($data);

                $this->Session->setFlash('Information successfully saved.', 'success');
                $this->redirect(array('action' => 'add'));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully saved. Please try again!', 'error');
            }
        }
    }

    public function edit($key = null)
    {
        $this->loadModel('BatchOrganisation');
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'bThOrG');

        $detail = $this->BatchOrganisation->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $this->BatchOrganisation->set($data);
            if($this->BatchOrganisation->validates())
            {
                $data['BatchOrganisation']['id'] = $detail['BatchOrganisation']['id'];
                // $data['BatchOrganisation']['created_by'] = $staff['Staff']['id'];
                // $data['BatchOrganisation']['modified_by'] = $staff['Staff']['id'];

                $this->BatchOrganisation->create();
                $this->BatchOrganisation->save($data);

                $this->Session->setFlash('Information successfully saved.', 'success');
                $this->redirect(array('action' => 'edit/'.$key));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully saved. Please try again!', 'error');
            }
        }
        else
        {
            if(!empty($detail['BatchOrganisation']['start_date']))
            {
                $detail['BatchOrganisation']['start_date'] = date("d-m-Y",strtotime($detail['BatchOrganisation']['start_date']));
            }

            if(!empty($detail['BatchOrganisation']['end_date']))
            {
                $detail['BatchOrganisation']['end_date'] = date("d-m-Y",strtotime($detail['BatchOrganisation']['end_date']));
            }

            $this->request->data = $detail;
        }

        $this->set(compact('key', 'detail'));
    }

    public function view($key = null)
    {
        $this->loadModel('BatchOrganisation');
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'bThOrG');

        $detail = $this->BatchOrganisation->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {

        }
        else
        {
            if(!empty($detail['BatchOrganisation']['start_date']))
            {
                $detail['BatchOrganisation']['start_date'] = date("d-m-Y",strtotime($detail['BatchOrganisation']['start_date']));
            }

            if(!empty($detail['BatchOrganisation']['end_date']))
            {
                $detail['BatchOrganisation']['end_date'] = date("d-m-Y",strtotime($detail['BatchOrganisation']['end_date']));
            }

            if(!empty($detail['BatchOrganisation']['created']))
            {
                $detail['BatchOrganisation']['created'] = date("d-m-Y",strtotime($detail['BatchOrganisation']['created']));
            }

            if(!empty($detail['BatchOrganisation']['modified']))
            {
                $detail['BatchOrganisation']['modified'] = date("d-m-Y",strtotime($detail['BatchOrganisation']['modified']));
            }

            $this->request->data = $detail;
        }

        $is_actives = array(
                            1 => "<span class='label label-success'>Active</span>",
                            99 => "<span class='label label-default'>Inactive</span>"
                        );

        $disabled = 'disabled';

        $this->set(compact('key', 'detail', 'is_actives', 'disabled'));
    }

    public function upload($key = null)
    {
        $this->loadModel('BatchOrganisation');
        $this->loadModel('OrganisationCategory');
        $this->loadModel('Utility');

        ini_set('memory_limit', '256M');
        ini_set('max_execution_time', '0');

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'bThOrG');

        $detail = $this->BatchOrganisation->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            App::import('Vendor', 'Spreadsheet_Excel_Reader', array('file' => 'excelreader'.DS.'excel_reader.php'));
            $excel = new PhpExcelReader; 

            $data = $this->request->data;

            if($data['Organisation']['attachment']['error'] == 0)
            {
                $excel->read($data['Organisation']['attachment']['tmp_name']);
                $cells = $excel->sheets[0]['cells'];
                $counter = 0;
                for ($i=2; $i <= count($cells); $i++) 
                {
                    $code = isset($cells[$i][1]) ? $cells[$i][1] : '';
                    $category_code = isset($cells[$i][2]) ? $cells[$i][2] : '';
                    $name = isset($cells[$i][3]) ? $cells[$i][3] : '';
                    $report_to = isset($cells[$i][4]) ? $cells[$i][4] : '';
                    $active = isset($cells[$i][5]) ? $cells[$i][5] : '';
                    
                    $organisation = array();

                    $organisation = $this->Organisation->find('first',
                                                                array(
                                                                    'conditions' => array(
                                                                                        'Organisation.batch_organisation_id' => $detail['BatchOrganisation']['id'], 
                                                                                        'Organisation.is_active' => 1,
                                                                                        'Organisation.code' => $code,
                                                                                    ),
                                                                ));

                    if(empty($organisation))
                    {
                        $category = $this->OrganisationCategory->find('first',
                                                                        array(
                                                                            'conditions' => array(
                                                                                                'OrganisationCategory.SAP_code' => $category_code,
                                                                                            ),
                                                                        ));

                        $organisation['Organisation']['name'] = $name;
                        $organisation['Organisation']['code'] = $code;
                        $organisation['Organisation']['batch_organisation_id'] = $detail['BatchOrganisation']['id'];
                        
                        if(!empty($category))
                        {
                            $organisation['Organisation']['organisation_category_id'] = $category['OrganisationCategory']['id'];
                        }

                        $organisation['Organisation']['is_active'] = $active;
                        
                        if($active == 1)
                        {
                            $organisation['Organisation']['is_deleted'] = 99;
                        }
                        else
                        {
                            $organisation['Organisation']['is_deleted'] = 1;
                            $organisation['Organisation']['deleted'] = date('Y-m-d H:i:s');
                        }

                        $organisation['Organisation']['order'] = $counter;

                        $this->Organisation->create();
                        $this->Organisation->save($organisation);
                        
                        $counter++;
                    }
                    else
                    {
                        $update = array();

                        if(!empty($report_to))
                        {
                            $parent = $this->Organisation->find('first',
                                                            array(
                                                                'conditions' => array(
                                                                                    'Organisation.batch_organisation_id' => $detail['BatchOrganisation']['id'], 
                                                                                    'Organisation.is_active' => 1,
                                                                                    'Organisation.code' => $report_to,
                                                                                ),
                                                            ));


                            $category = $this->OrganisationCategory->find('first',
                                                            array(
                                                                'conditions' => array(
                                                                                    'OrganisationCategory.SAP_code' => $category_code,
                                                                                ),
                                                            ));

                            if(!empty($parent))
                            {
                                $update['Organisation']['id'] = $organisation['Organisation']['id'];
                                $update['Organisation']['parent_id'] = $parent['Organisation']['id'];
                                $update['Organisation']['name'] = $name;
                                $update['Organisation']['code'] = $code;

                                if(!empty($category))
                                {
                                    $update['Organisation']['organisation_category_id'] = $category['OrganisationCategory']['id'];
                                }
                                
                                $update['Organisation']['is_active'] = $active;
                                if($active == 1)
                                {
                                    $update['Organisation']['is_deleted'] = 99;
                                }
                                else
                                {
                                    $update['Organisation']['is_deleted'] = 1;
                                    $update['Organisation']['deleted'] = date('Y-m-d H:i:s');
                                }

                                $organisation['Organisation']['order'] = $counter;
                            } 

                            if(!empty($update))
                            {
                                $this->Organisation->create();
                                $this->Organisation->save($update);
                            }    

                            $counter++;
                        }
                        else
                        {
                            $update['Organisation']['id'] = $organisation['Organisation']['id'];
                            $update['Organisation']['parent_id'] = $parent['Organisation']['id'];
                            $update['Organisation']['name'] = $name;
                            $update['Organisation']['code'] = $code;

                            $category = $this->OrganisationCategory->find('first',
                                                            array(
                                                                'conditions' => array(
                                                                                    'OrganisationCategory.SAP_code' => $category_code,
                                                                                ),
                                                            ));

                            if(!empty($category))
                            {
                                $update['Organisation']['organisation_category_id'] = $category['OrganisationCategory']['id'];
                            }
                            
                            $update['Organisation']['is_active'] = $active;
                            if($active == 1)
                            {
                                $update['Organisation']['is_deleted'] = 99;
                            }
                            else
                            {
                                $update['Organisation']['is_deleted'] = 1;
                                $update['Organisation']['deleted'] = date('Y-m-d H:i:s');
                            }

                            $organisation['Organisation']['order'] = $counter;

                            if(!empty($update))
                            {
                                $this->Organisation->create();
                                $this->Organisation->save($update);
                            } 
                        }
                    }
                }

                $this->Session->setFlash('Information successfully upload.', 'success');
                $this->redirect(array('action' => 'upload/'.$key));
            }
        }

        $this->set(compact('key', 'detail'));
    }

    public function delete($key = null)
    {
        $this->loadModel('BatchOrganisation');
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'bThOrG');

        $detail = $this->BatchOrganisation->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        if($detail['BatchOrganisation']['is_active'] == 1)
        {
            $this->Session->setFlash('You cannot delete active data. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        $count = $this->Organisation->find('count', array(
                                                        'conditions' => array(
                                                                            'Organisation.batch_organisation_id' => $detail['BatchOrganisation']['id']
                                                                        )
                                                    ));

        if($count > 0)
        {
            $this->Session->setFlash('You cannot delete organisation batch that have organisation in it. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        if($this->BatchOrganisation->delete($detail['BatchOrganisation']['id']))
        {
            $this->Session->setFlash('Information successfully deleted.', 'success');
            $this->redirect(array('action' => '/'));
        }
        else
        {
            $this->Session->setFlash('Error! Information not successfully deleted. Please try again!', 'error');
            $this->redirect(array('action' => '/'));
        }

        $this->autoRender = false;
    }

    public function active($key = null)
    {
        $this->loadModel('BatchOrganisation');
        $this->loadModel('Employee');
        $this->loadModel('Utility');

        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', '0'); // for infinite time of execution 

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'bThOrG');

        $detail = $this->BatchOrganisation->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        //<----- active batch organisation START------->
        $batchorganisations = $this->BatchOrganisation->find('all', array(
                                                            'conditions' => array(
                                                                array('BatchOrganisation.id NOT' => $detail['BatchOrganisation']['id']),
                                                            )
                                                        ));
        if(!empty($batchorganisations))
        {
            foreach ($batchorganisations as $batchorganisation) 
            {
                $data = array();
                $data['BatchOrganisation']['id'] = $batchorganisation['BatchOrganisation']['id'];
                $data['BatchOrganisation']['is_active'] = 99;
                $data['BatchOrganisation']['modified_by'] = $employee['Employee']['id'];

                $this->BatchOrganisation->create();
                $this->BatchOrganisation->save($data);
            }
        }

        $data = array();
        $data['BatchOrganisation']['id'] = $detail['BatchOrganisation']['id'];
        $data['BatchOrganisation']['is_active'] = 1;
        $data['BatchOrganisation']['modified_by'] = $employee['Employee']['id'];

        $this->BatchOrganisation->create();
        $this->BatchOrganisation->save($data);
        

        //<----- active batch organisation END ------>

        //<----- get all active staff and start change organisation to btach active START ------> 
        $employee_lists = $this->Employee->find('list', array(
                                                                'conditions' => array(
                                                                    array('Employee.is_active' => 1),
                                                                )
                                                ));

        //<----- Unset Employee validation START ------> 
        unset($this->Employee->validate['employee_no']);
        unset($this->Employee->validate['is_type']);
        unset($this->Employee->validate['status_id']);
        unset($this->Employee->validate['employment_status_id']);
        unset($this->Employee->validate['employee_group_id']);
        unset($this->Employee->validate['employee_subgroup_id']);
        unset($this->Employee->validate['personal_area_id']);
        unset($this->Employee->validate['personal_subarea_id']);
        unset($this->Employee->validate['payroll_area_id']);
        unset($this->Employee->validate['entry_date']);
        unset($this->Employee->validate['seniority_date']);
        unset($this->Employee->validate['organizational_unit_id']);
        unset($this->Employee->validate['organizational_unit_code']);
        unset($this->Employee->validate['cost_center_id']);
        unset($this->Employee->validate['cost_center_code']);
        unset($this->Employee->validate['job_designation_id']);
        unset($this->Employee->validate['job_grade_id']);
        //<----- Unset Employee validation END ------> 

        foreach ($employee_lists as $employee_list => $value) 
        {

            $information =  $this->Employee->findById($value);
            if(!empty($information))
            {
                $update = array();

                $update['Employee']['id'] = $information['Employee']['id'];
                if(!empty($information['Employee']['organizational_unit_code']))
                {
                    $organisation = $this->Organisation->find('first', array(
                                                                            'conditions' => array(
                                                                                array(
                                                                                        'Organisation.code' => $information['Employee']['organizational_unit_code'],
                                                                                        'BatchOrganisation.is_active' => 1,
                                                                                    ),
                                                                            )
                                                            ));
                    
                    if(!empty($organisation))
                    {
                        $update['Employee']['organizational_unit_id'] = $organisation['Organisation']['id'];

                        $this->Employee->create();
                        $this->Employee->save($update);
                    }
                }
            }
        }
        //<----- get all active staff and start change organisation to btach active END ------>

        $this->Session->setFlash('Information successfully updated.', 'success');
        $this->redirect(array('action' => '/'));

        $this->autoRender = false;
    }

    public function add_organisation($batchkey = null)
    {
        $this->loadModel('BatchOrganisation');
        $this->loadModel('OrganisationCategory');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        if(empty($batchkey))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($batchkey, 'bThOrG');

        $detail = $this->BatchOrganisation->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $parent = array();
            if(!empty($data['Organisation']['parent_code']))
            {
                $parent = $this->Organisation->find('first', array(
                                                                'conditions' => array(
                                                                                'Organisation.batch_organisation_id' => $detail['BatchOrganisation']['id'], 
                                                                                'Organisation.is_active' => 1,
                                                                                'Organisation.is_deleted' => 99,
                                                                                'Organisation.code' => $data['Organisation']['parent_code'],
                                                                                )
                                                            ));
                if(empty($parent))
                {
                    $data['Organisation']['parent_code'] = Null;
                    $this->Organisation->validator()
                                                ->add('parent_code', 'required', array(
                                                    'rule' => array('notBlank'),
                                                    'message' => 'Orgasanition parent not active or exist'
                                                ));
                }
            }

            $this->Organisation->set($data);
            if($this->Organisation->validates())
            {


                if(!empty($parent))
                {
                    $data['Organisation']['parent_id'] = $parent['Organisation']['id'];
                }

                $data['Organisation']['batch_organisation_id'] = $detail['BatchOrganisation']['id'];
                $data['Organisation']['is_active'] = 1;
                $data['Organisation']['is_deleted'] = 99;
                $data['Organisation']['modified_by'] = $employee['Employee']['id'];
                $data['Organisation']['created_by'] = $employee['Employee']['id'];

                $this->Organisation->create();
                $this->Organisation->save($data);

                $this->Session->setFlash('Information successfully saved.', 'success');
                $this->redirect(array('action' => 'add_organisation/'.$batchkey));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully saved. Please try again!', 'error');
            }
        }

        $organisationcategories = $this->OrganisationCategory->find('list');

        $this->set(compact('batchkey', 'organisationcategories', 'detail'));
    }

    public function lists()
    {
        $this->loadModel('BatchOrganisation');
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        $filter_url['action'] = $this->request->params['action'];

        $batchkey = "";

        if(!empty($this->params['named']['batchkey']))
        {
            $batchkey = $this->params['named']['batchkey'];
        }

        if(empty($batchkey))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        $id = $this->Utility->decrypt($batchkey, 'bThOrG');

        $detail = $this->BatchOrganisation->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        $conditions = array();

        $conditions['conditions'][] = array(
                                            'Organisation.batch_organisation_id' => $detail['BatchOrganisation']['id'],
                                            'Organisation.is_deleted' => 99,
                                        );

        $conditions['order'] = array('Organisation.order'=> 'ASC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Organisation'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters, 
            // we'll redirect to that page
            return $this->redirect($filter_url);
        } 
        else 
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "search")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Organisation.name LIKE' => '%' . $value . '%')
                        );

                        $conditions['conditions']['OR'][] = array(
                            array('Organisation.code LIKE' => '%' . $value . '%')
                        );
                    } 
                    
					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Organisation.modified) >=' => date("Y-m-d", strtotime($value))
                        );

                    }
					
                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Organisation.modified) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc                 
                    $this->request->data['Organisation'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate();

        for ($i=0; $i < count($details); $i++) 
        {
            if(!empty($details[$i]['Organisation']['modified']))
            {
                $details[$i]['Organisation']['modified'] = date("d-m-Y",strtotime($details[$i]['Organisation']['modified']));
            }
            else
            {
                $details[$i]['Organisation']['modified'] = '-';
            }

            $details[$i]['Organisation']['id'] = $this->Utility->encrypt($details[$i]['Organisation']['id'], 'oRgaNisatiOn');
        }
        
        $this->set(compact('batchkey', 'detail', 'details'));
    }

    public function list_view($batchkey = null, $key = null)
    {
        $this->loadModel('BatchOrganisation');
        $this->loadModel('OrganisationCategory');
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        if(empty($batchkey))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'oRgaNisatiOn');

        $detail = $this->Organisation->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {

        }
        else
        {
            if(!empty($detail['Organisation']['created']))
            {
                $detail['Organisation']['created'] = date("d-m-Y",strtotime($detail['Organisation']['created']));
            }

            if(!empty($detail['Organisation']['modified']))
            {
                $detail['Organisation']['modified'] = date("d-m-Y",strtotime($detail['Organisation']['modified']));
            }

            $this->request->data = $detail;
        }

        $is_actives = array(
                            1 => "<span class='label label-success'>Active</span>",
                            99 => "<span class='label label-default'>Inactive</span>"
                        );

        $organisationcategories = $this->OrganisationCategory->find('list');

        $disabled = 'disabled';

        $this->set(compact('batchkey', 'key', 'detail', 'organisationcategories', 'is_actives', 'disabled'));
    }

    public function list_edit($batchkey = null, $key = null)
    {
        $this->loadModel('BatchOrganisation');
        $this->loadModel('OrganisationCategory');
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        if(empty($batchkey))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'oRgaNisatiOn');

        $detail = $this->Organisation->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $this->Organisation->set($data);
            if($this->Organisation->validates())
            {
                $data['Organisation']['id'] = $detail['Organisation']['id'];
                // $data['Organisation']['modified_by'] = $staff['Staff']['id'];

                $this->Organisation->create();
                $this->Organisation->save($data);

                $this->Session->setFlash('Information successfully saved.', 'success');
                $this->redirect(array('action' => 'list_edit/'.$batchkey.'/'.$key));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully saved. Please try again!', 'error');
            }
        }
        else
        {
            $this->request->data = $detail;
        }

        $organisationcategories = $this->OrganisationCategory->find('list');

        $this->set(compact('batchkey', 'key', 'detail', 'organisationcategories'));
    }

    public function list_delete($batchkey = null, $key = null)
    {
        $this->loadModel('BatchOrganisation');
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        if(empty($batchkey))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'oRgaNisatiOn');

        $detail = $this->Organisation->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/lists/batchkey:'.$batchkey));
        }

        $data = array();
        $data['Organisation']['id'] = $detail['Organisation']['id'];
        $data['Organisation']['is_deleted'] = 1;
        $data['Organisation']['deleted'] = date('Y-m-d H:i:s');
        // $data['Organisation']['deleted_by'] = $staff['Staff']['id'];

        $this->Organisation->create();
        $this->Organisation->save($data);

        $this->Session->setFlash('Information successfully deleted.', 'success');
        $this->redirect(array('action' => '/lists/batchkey:'.$batchkey));
       

        $this->autoRender = false;
    }

    public function coordination()
    {
        $this->loadModel('BatchOrganisation');
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        $filter_url['action'] = $this->request->params['action'];

        $batchkey = "";

        if(!empty($this->params['named']['batchkey']))
        {
            $batchkey = $this->params['named']['batchkey'];
        }

        if(empty($batchkey))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        $id = $this->Utility->decrypt($batchkey, 'bThOrG');

        $detail = $this->BatchOrganisation->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        $conditions = array();

        $conditions['conditions'][] = array(
                                            'Organisation.batch_organisation_id' => $detail['BatchOrganisation']['id'],
                                            'Organisation.parent_id' => "",
                                            'Organisation.is_deleted' => 99,
                                        );

        $conditions['order'] = array('Organisation.created'=> 'ASC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Organisation'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters, 
            // we'll redirect to that page
            return $this->redirect($filter_url);
        } 
        else 
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "search")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Organisation.name LIKE' => '%' . $value . '%')
                        );

                        $conditions['conditions']['OR'][] = array(
                            array('Organisation.code LIKE' => '%' . $value . '%')
                        );
                    } 
                    
					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Organisation.modified) >=' => date("Y-m-d", strtotime($value))
                        );

                    }
					
                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Organisation.modified) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc                 
                    $this->request->data['Organisation'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate();

        for ($i=0; $i < count($details); $i++) 
        {
            if(!empty($details[$i]['Organisation']['modified']))
            {
                $details[$i]['Organisation']['modified'] = date("d-m-Y",strtotime($details[$i]['Organisation']['modified']));
            }
            else
            {
                $details[$i]['Organisation']['modified'] = '-';
            }

            $details[$i]['Organisation']['id'] = $this->Utility->encrypt($details[$i]['Organisation']['id'], 'oRgaNisatiOn');
        }
        
        $this->set(compact('batchkey', 'detail', 'details'));
    }

    public function list_coordination($batchkey = null, $key = null)
    {
        $this->loadModel('BatchOrganisation');
        $this->loadModel('OrganisationCategory');
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        if(empty($batchkey))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'oRgaNisatiOn');
        
        $detail = $this->Organisation->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $batch_id = $this->Utility->decrypt($batchkey, 'bThOrG');

        $batch = $this->BatchOrganisation->findById($batch_id);

        if(empty($batch))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {

        }
        else
        {
            $this->request->data = $detail;
        }


        $organisations = $this->Organisation->find('threaded',
                                            array(
                                                'conditions' => array(
                                                                    'Organisation.batch_organisation_id' => $batch['BatchOrganisation']['id'],
                                                                    'Organisation.is_deleted' => 99,
                                                                ),
                                                'contain' => false,
                                                'order' => array('Organisation.order ASC'),
                                        ));
        $str_orgs = "";
        $str_orgs = $this->Utility->getULdatasource($organisations, $str_orgs, 'Organisation');

        $this->set(compact('batchkey', 'key', 'detail', 'str_orgs'));
    }

    public function setup_coordination($batchkey = null, $key = null)
    {
        $this->loadModel('BatchOrganisation');
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        if(empty($batchkey))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'oRgaNisatiOn');
        
        $detail = $this->Organisation->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $batch_id = $this->Utility->decrypt($batchkey, 'bThOrG');

        $batch = $this->BatchOrganisation->findById($batch_id);

        if(empty($batch))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if ($this->request->is('post') || $this->request->is('put')) 
        {
            $data = $this->request->data;
            $this->Utility->getNestableSave($data, count($data), 'Organisation', '', 1);
        }


        $organisations = $this->Organisation->find('threaded',
                                            array(
                                                'conditions' => array(
                                                                    'Organisation.batch_organisation_id' => $batch['BatchOrganisation']['id'],
                                                                    'Organisation.is_deleted' => 99,
                                                                ),
                                                'contain' => false,
                                                'order' => array('Organisation.order ASC'),
                                        ));
        

        $nestable = "";

        $nestable = $this->Utility->getNestable($organisations, count($organisations), 'Organisation', "", 'name');
    
        $this->set(compact('batchkey', 'key', 'detail', 'nestable'));
    }

}