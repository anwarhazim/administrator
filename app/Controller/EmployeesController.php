<?php
class EmployeesController extends AppController 
{
	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter() 
    {
        parent::beforeFilter();
        $this->Auth->allow('login', 'forgot', 'setup');
    }
    
    public function index()
    {
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        $conditions = array();

        $conditions['conditions'][] = array();

        $conditions['order'] = array('Employee.id'=> 'DESC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Employee'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters, 
            // we'll redirect to that page
            return $this->redirect($filter_url);
        } 
        else 
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "search")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Personal.complete_name LIKE' => '%' . $value . '%')
                        );

                        $conditions['conditions']['OR'][] = array(
                            array('Employee.employee_no LIKE' => '%' . $value . '%')
                        );
                    } 
                    
					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Employee.modified) >=' => date("Y-m-d", strtotime($value))
                        );

                    }
					
                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Employee.modified) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc                 
                    $this->request->data['Employee'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate('Employee');

        for ($i=0; $i < count($details); $i++) 
        { 
            if(!empty($details[$i]['Employee']['entry_date']))
            {
                $details[$i]['Employee']['entry_date'] = date("d-m-Y",strtotime($details[$i]['Employee']['entry_date']));
            }
            else
            {
                $details[$i]['Employee']['entry_date'] = '-';
            }

            if(!empty($details[$i]['Employee']['leaving_date']))
            {
                $details[$i]['Employee']['leaving_date'] = date("d-m-Y",strtotime($details[$i]['Employee']['leaving_date']));
            }
            else
            {
                $details[$i]['Employee']['leaving_date'] = '-';
            }

            switch ($details[$i]['Employee']['is_active']) 
            {
                case 1:
                    $details[$i]['Status']['name'] = "ACTIVE";
                    break;
                case 99:
                    $details[$i]['Status']['name'] = "INACTIVE";
                    break;
            }
             

            $details[$i]['Employee']['modified'] = date("d-m-Y",strtotime($details[$i]['Employee']['modified']));
            
            $details[$i]['Personal']['id'] = $this->Utility->encrypt($details[$i]['Personal']['id'], 'uSeR@');
        }

        $this->set(compact('details'));
    }

    public function assign($key = null)
    {
        $this->loadModel('Employee');
        $this->loadModel('Personal');
        $this->loadModel('Status');
        $this->loadModel('Role');
        $this->loadModel('UserRole');
		$this->loadModel('Utility');

        $role_selected = array();
        
        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'uSeR@');

        $personal = $this->Personal->findById($id);

        $detail = $this->User->findById($personal['Personal']['user_id']);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }
        
        foreach ($detail['UserRole'] as $userrole) 
        {
            $role_selected[] = $userrole['role_id'];
        }

		if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $this->User->set($data);
            if($this->User->validates())
            {
                $user_roles = array();
                $user_roles = $data['User']['roles'];

                $userrole_excluded = array();

                foreach ($detail['UserRole'] as $userrole) 
                {
                    $userrole_excluded[] = $userrole['role_id'];
                }

                $max = count($userrole_excluded);
                for ($i = 0; $i < $max; $i++) 
                { 
                    if (!in_array($userrole_excluded[$i], $user_roles)) 
                    {
                        $this->UserRole->deleteAll(array('UserRole.user_id' => $detail['User']['id'], 'UserRole.role_id' => $userrole_excluded[$i]), false);
                    }
                }

                $max = count($user_roles);
                for ($i = 0; $i < $max; $i++) 
                { 
                    if (in_array($data['User']['roles'][$i], $role_selected)) 
                    {
                        unset($data['User']['roles'][$i]);
                    }
                }
        
                $role_choose = array();
                foreach ($data['User']['roles'] as $role_id) 
                {
                    if (!in_array($role_id, $role_choose)) 
                    {
                        $role = array();
                        $role['UserRole']['user_id'] = $detail['User']['id'];
                        $role['UserRole']['role_id'] = $role_id;
                        $role['UserRole']['created_by'] = 1;
                        $role['UserRole']['modified_by'] = 1;

                        $this->UserRole->create();
                        $this->UserRole->save($role);

                        $role_choose[] = $role_id;
                    }
                }
            }

            $this->Session->setFlash('Information successfully updated.', 'success');
            $this->redirect(array('action' => 'assign/'.$key));
            
        }
        else
        {
            $this->request->data = $detail;
        }

        $employee = $this->Employee->find('first', array(
                                        'conditions' => array(
                                                            'Employee.is_active' => 1,
                                                            'Employee.personal_id' => $personal['Personal']['id']
                                                        )
                                    ));
		
        $disabled = "";

        $roles = $this->Role->find('list');
        
        $this->set(compact('key', 'disabled', 'employee', 'roles', 'role_selected'));
    }

    public function upload()
    {
        $this->loadModel('Employee');
        $this->loadModel('Personal');
        $this->loadModel('Status');
        $this->loadModel('Role');
        $this->loadModel('UserRole');
        $this->loadModel('Utility');
        
        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        $role_selected = array();
        $userrole_excluded = array();

        if($this->request->is('post') || $this->request->is('put'))
        {
            App::import('Vendor', 'Spreadsheet_Excel_Reader', array('file' => 'excelreader'.DS.'excel_reader.php'));
            $excel = new PhpExcelReader; 

            $data = $this->request->data;

            $user_roles = array();
            $user_roles[] = $data['User']['role_id'];

            if($data['User']['attachment']['error'] == 0)
            {
                $excel->read($data['User']['attachment']['tmp_name']);
                $cells = $excel->sheets[0]['cells'];
                $counter = 0;
                
                for ($i=2; $i <= count($cells); $i++) 
                {
                    $employee_no = isset($cells[$i][1]) ? $cells[$i][1] : '';

                    $staff = array();

                    $staff = $this->Employee->find('first',
                                                    array(
                                                        'conditions' => array(
                                                                            'Employee.employee_no' => $employee_no,
                                                                            'Employee.is_active' => 1,
                                                                        ),
                                                    ));
                    if(!empty($staff))
                    {
                        $personal = $this->Personal->findById($staff['Employee']['personal_id']);
                        $user = $this->User->findById($personal['Personal']['user_id']);
                        
                        $userrole_excluded = array();
                        $role_selected = array();

                        foreach ($user['UserRole'] as $userrole) 
                        {
                            $userrole_excluded[] = $userrole['role_id'];
                        }

                        foreach ($user['UserRole'] as $userrole) 
                        {
                            $role_selected[] = $userrole['role_id'];
                        }
        
                        $max_roles = count($user_roles);
                        for ($k = 0; $k < $max_roles; $k++) 
                        { 
                            if (in_array($user_roles[$k], $role_selected)) 
                            {
                                unset($user_roles[$k]);
                            }
                        }
                        
                        $role_choose = array();
                        foreach ($user_roles as $role_id) 
                        {
                            if (!in_array($role_id, $role_choose)) 
                            {
                                $role = array();
                                $role['UserRole']['user_id'] = $user['User']['id'];
                                $role['UserRole']['role_id'] = $role_id;
                                $role['UserRole']['created_by'] = $employee['Employee']['id'];
                                $role['UserRole']['modified_by'] = $employee['Employee']['id'];
                                
                                $this->UserRole->create();
                                $this->UserRole->save($role);
        
                                $role_choose[] = $role_id;
                            }
                        }
                    }
                }

                $this->Session->setFlash('Information successfully upload adn edit.', 'success');
                $this->redirect(array('action' => 'upload'));
            }
        }

        $roles = $this->Role->find('list');

        $this->set(compact('key', 'roles'));
    }
	
	public function login()
	{
        $this->loadModel('Log');
        $this->loadModel('Utility');
		
		if($this->request->is('post') || $this->request->is('put'))
        {
			if ($this->Auth->login()) 
            { 
                $person = $this->Auth->user();
                
				$employee = $this->Utility->getUserInformation($person['id']);

                $session = "";

                $data = array();
                $data['User']['id'] = $person['id'];
                $data['User']['session'] = $session;

                $this->User->create();
                $this->User->save($data);

                $logs = array();
                $logs['Log']['employee_id'] = $employee['Employee']['id'];
                $logs['Log']['action_id'] = '1'; // login
                $logs['Log']['path'] = $this->here; //get current path
                $logs['Log']['project_id'] = '1'; //set project id
                $logs['Log']['created_by'] = $employee['Employee']['id'];
                $logs['Log']['created'] = date('Y-m-d H:i:s');
                $logs['Log']['modified_by'] = $employee['Employee']['id'];
                $logs['Log']['modified'] = date('Y-m-d H:i:s');

                $this->Log->create();
                $this->Log->save($logs);

                $this->Session->setFlash('Welcome, '.$employee['Personal']['complete_name'], 'success');
                $this->redirect(array('controller' => 'Dashboards', 'action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('You have entered an invalid Staff No. or Password. Please try again!', 'error');
			}
		}

		$this->layout = 'blank';
	}

	public function forgot()
    {
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $check = $this->Staff->find('first', array(
													'conditions' => array(
																		'User.is_active' => 1, 
																		'User.status_id' => 10,
																		'Staff.email' => $data['User']['email']
																	)
												));

            if(!empty($check))
            {
                $session = $this->Utility->generateRandomString();
                $session = $this->Utility->signature($session.$check['Staff']['id']);

                $reset = array();
                $reset['User']['id'] = $check['Staff']['user_id'];
                $reset['User']['session'] = $session;

                $this->User->create();
                $this->User->save($reset);

                $baseURL = Router::url('/', true);

                $subject = 'Reset Password PRESS';
                $from = 'press.admin@prasarana.com.my';
                $to = $check['Staff']['email'];

                $headers = "From: " . strip_tags($check['Staff']['email']) . "\r\n";
                $headers .= "Reply-To: ". strip_tags($check['Staff']['email']) . "\r\n";
                $headers .= "MIME-Version: 1.0\r\n";
                $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                $message = '<p>Please click <a href="'.$baseURL.'Users/setup/' . $session . '">here</a> to reset your new password.</p>';
                $message .= '<p>For any enquiries, please contact PRESS System Administrator.</p>';

                $Email = new CakeEmail('smtp');
                $Email->from(array( $from => 'PRESS'));
                $Email->to($to);
                $Email->template('default', null);
                $Email->emailFormat('html');
                $Email->subject($subject);
                $Email->viewVars(array('baseURL' => $baseURL, 'message' => $message,));
                $Email->send();

                $this->Session->setFlash('A link has been sent to  '.$check['Staff']['email'], 'success');
                $this->redirect(array('action' => 'forgot'));
            }
            else
            {
                $this->Session->setFlash('Your email address is not in our record.', 'error');
            }
            
        }
        
        $this->layout = 'blank';
	}
	
	public function setup($key = null)
    {
        $this->loadModel('Staff');
        $this->loadModel('Utility');

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $detail = $this->User->find('first', array(
													'conditions' => array(
																	'User.is_active' => 1, 
																	'User.session' => $key
																	)
												));
        

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $data['User']['id'] = $detail['User']['id'];

            $this->User->set($data);
            if($this->User->validates())
            {
                $data['User']['session'] = '';
                $this->User->create();
                $this->User->save($data);

                $this->Session->setFlash('Your new password successfully updated.', 'success');
                $this->redirect(array('action' => '/'));
            }
            else
            {
                $this->Session->setFlash('Error! Permission denied. Please try again!', 'error');
            }
            
        }
        
        $this->layout = 'blank';
    }

	public function logout()
    {
        $this->redirect($this->Auth->logout());
    }

	public function profile()
	{
        $this->loadModel('Personal');
        $this->loadModel('Employee');
        $this->loadModel('Gender');
        $this->loadModel('Ethnic');
        $this->loadModel('Marital');
        $this->loadModel('State');
        $this->loadModel('JobDesignation');
        $this->loadModel('JobGrade');
        $this->loadModel('Organisation');
        $this->loadModel('Position');
        $this->loadModel('PersonalArea');
        $this->loadModel('PersonalSubarea');
        $this->loadModel('EmployeeGroup');
        $this->loadModel('EmployeeSubgroup');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        if(empty($employee))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect(array('controller' => 'Notifications', 'action' => 'index'));
        }

        if(!empty($employee['Personal']['date_of_birth']))
        {
            $employee['Personal']['date_of_birth'] =  date("d-m-Y", strtotime($employee['Personal']['date_of_birth']));
        }

        if(!empty($employee['Employee']['seniority_date']))
        {
            $employee['Employee']['seniority_date'] =  date("d-m-Y", strtotime($employee['Employee']['seniority_date']));
        }

        if(!empty($employee['Employee']['entry_date']))
        {
            $employee['Employee']['entry_date'] =  date("d-m-Y", strtotime($employee['Employee']['entry_date']));
        }

        if(!empty($employee['Employee']['leaving_date']))
        {
            $employee['Employee']['leaving_date'] =  date("d-m-Y", strtotime($employee['Employee']['leaving_date']));
        }

        $roles = array();

        foreach ($employee['Roles'] as $role) 
        {
            $roles[$role['id']] = $role['name'];
        }

        $jobdesignations = $this->JobDesignation->find('list');
        $jobgrades = $this->JobGrade->find('list');
        $positions = $this->Position->find('list');
        $personalareas = $this->PersonalArea->find('list');
        $personalsubareas = $this->PersonalSubarea->find('list');
        $employeegroups = $this->EmployeeGroup->find('list');
        $employeesubgroups = $this->EmployeeSubgroup->find('list');
        $genders = $this->Gender->find('list');
        $ethnics = $this->Ethnic->find('list');
        $marital = $this->Marital->find('list');
        $states = $this->State->find('list');

        $organisationunit = $this->Organisation->findById($employee['Employee']['organizational_unit_id']);

        $organisations = $this->Organisation->find('list',
                                                array(
                                                    'conditions' => array(
                                                                        'Organisation.batch_organisation_id' => $organisationunit['Organisation']['batch_organisation_id'],
                                                                        'Organisation.is_active' => 1,
                                                                        'Organisation.is_deleted' => 99,
                                                                    ),
                                                ));

        $this->request->data = $employee;

        $path = Router::url('/app/webroot/documents/'.$employee['Employee']['employee_no'].'/MEDIAS/', true);

        $disabled = 'disabled';

        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // login
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '1'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['created'] = date('Y-m-d H:i:s');
        $logs['Log']['modified_by'] = $employee['Employee']['id'];
        $logs['Log']['modified'] = date('Y-m-d H:i:s');

        $this->Log->create();
        $this->Log->save($logs);

		$this->set(compact('employee', 'path', 'disabled', 'states', 'genders', 'ethnics', 'marital', 'jobdesignations', 'jobgrades', 'positions', 'organisations', 'personalareas', 'personalsubareas', 'employeegroups', 'employeesubgroups', 'roles'));
	}

	public function password()
	{
        $this->loadModel('Personal');
        $this->loadModel('Employee');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
		$employee = $this->Utility->getUserInformation($person['id']);
		
		$detail = $this->User->find('first', array(
												'conditions' => array(
																	'User.is_active' => 1, 
																	'User.id'=> $person['id']
																)
											));

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect(array('controller' => 'Notifications', 'action' => 'index'));
		}
		
		if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $data['User']['id'] = $detail['User']['id'];
            
            $this->User->set($data);
            if($this->User->validates())
            {
                $this->User->create();
                $this->User->save($data);

                $logs = array();
                $logs['Log']['employee_id'] = $employee['Employee']['id'];
                $logs['Log']['action_id'] = '4'; // edit
                $logs['Log']['path'] = $this->here; //get current path
                $logs['Log']['project_id'] = '1'; //set project id
                $logs['Log']['created_by'] = $employee['Employee']['id'];
                $logs['Log']['created'] = date('Y-m-d H:i:s');
                $logs['Log']['modified_by'] = $employee['Employee']['id'];
                $logs['Log']['modified'] = date('Y-m-d H:i:s');
                
                $this->Log->create();
                $this->Log->save($logs);

                $this->Session->setFlash('Your new password successfully updated.', 'success');
                $this->redirect(array('action' => '/password'));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully saved. Please try again!', 'error');
            }
        }

        $path = Router::url('/app/webroot/documents/'.$employee['Employee']['employee_no'].'/MEDIAS/', true);

        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // view
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '1'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['created'] = date('Y-m-d H:i:s');
        $logs['Log']['modified_by'] = $employee['Employee']['id'];
        $logs['Log']['modified'] = date('Y-m-d H:i:s');
        
        $this->Log->create();
        $this->Log->save($logs);

		$this->set(compact('employee', 'path'));
    }
}