<?php
App::uses('Controller', 'Controller');

class AppController extends Controller 
{
    public $components = array
    (
		// disabled directory
	    'Session',
        'Auth' => array
                    (
	        		'loginRedirect' => array('controller' => 'Dashboards', 'action' => 'index'),
					'logoutRedirect' => array('controller' => 'Users', 'action' => 'login'),
	        		'authError' => 'You must login to access this system.',
					'loginError' => 'Invalid username and password. Please try again!',
                    'authenticate' => array
                                        (
                                            'Form' => array
                                            (
												'scope' => array('User.is_active' => 1, 'User.status_id' => 10)
											),
										),
					),
    );


    public function beforeFilter() 
	{	
	    $user = $this->Auth->user();
	    if($user)
	    {
	    	$this->isAuthorized($user);
		}
    }
    
    public function isAuthorized($user) 
	{
		// Here is where we should verify the role and give access based on role
		if($user)
		{			
			$this->loadModel('Project');
			$this->loadModel('Utility');

			$project_core_id = 1;
			$project = $this->Project->findById($project_core_id);

			$path = $project['Project']['url'];
			
			$project_id = 2;

			$session = $this->Utility->getUserInformation($user['id'], $project_id);
			$session['Sidebar'] = $this->Utility->getSidebar($user['id'], $project_id);
			$session['Project'] = $this->Utility->getProject($user['id']);
			$session['Path']['url'] = $path;

			$this->set("session", $session);

		    return true;
		} 
		else 
		{
			return false;
		}
	}

}
