<?php
class ContactsController extends AppController 
{

	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter() 
    {
        parent::beforeFilter();
    }
    
    public function upload()
    {
        $this->loadModel('Personal');
        $this->loadModel('Employee');
        $this->loadModel('Contact');
        $this->loadModel('State');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        ini_set('memory_limit', '256M');
        ini_set('max_execution_time', '0');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        if($this->request->is('post') || $this->request->is('put'))
        {
            App::import('Vendor', 'Spreadsheet_Excel_Reader', array('file' => 'excelreader'.DS.'excel_reader.php'));
            $excel = new PhpExcelReader; 

            $data = $this->request->data;

            if($data['Contact']['attachment']['error'] == 0)
            {
                $excel->read($data['Contact']['attachment']['tmp_name']);
                $cells = $excel->sheets[0]['cells'];
                $counter = 0;
                for ($i=2; $i <= count($cells); $i++) 
                {
                    $employee_no = isset($cells[$i][1]) ? $cells[$i][1] : '';
                    $address_1 = isset($cells[$i][2]) ? $cells[$i][2] : '';
                    $address_2 = isset($cells[$i][3]) ? $cells[$i][3] : '';
                    $address_3 = isset($cells[$i][4]) ? $cells[$i][4] : '';
                    $postcode = isset($cells[$i][5]) ? $cells[$i][5] : '';
                    $states = isset($cells[$i][6]) ? $cells[$i][6] : '';
                    $mobile_no = isset($cells[$i][7]) ? $cells[$i][7] : '';

                    $employee_information = $this->Employee->find('first',
                                                array(
                                                    'conditions' => array(
                                                                        'Employee.employee_no' => $employee_no,
                                                                        'Employee.is_active' => 1,
                                                                    ),
                                                ));

                    if(!empty($employee_information))
                    {
                        $contact = array();
                        $contact['Contact']['personal_id'] = $employee_information['Employee']['personal_id'];
                        $contact['Contact']['is_type'] = 1;
                        $contact['Contact']['is_flag'] = 1;
                        $contact['Contact']['is_active'] = 1;
                        $contact['Contact']['status_id'] = 10;

                        if(!empty($address_1))
                        {
                            $contact['Contact']['current_address_1'] = $address_1;
                        }

                        if(!empty($address_2))
                        {
                            $contact['Contact']['current_address_2'] = $address_2;
                        }

                        if(!empty($address_3))
                        {
                            $contact['Contact']['current_address_3'] = $address_3;
                        }

                        if(!empty($postcode))
                        {
                            $contact['Contact']['current_postcode'] = $postcode;
                        }

                        if(!empty($states))
                        {
                            $state_information = $this->State->find('first',
                                                                            array(
                                                                                'conditions' => array(
                                                                                                    'State.name' => $states,
                                                                                                ),
                                                                            ));
                            if(!empty($state_information))
                            {
                                $contact['Contact']['current_states_id'] = $state_information['State']['id'];
                            }
                        }
                        
                        if(!empty($mobile_no))
                        {
                            $contact['Contact']['mobile_no'] = $mobile_no;
                        }

                        $contact['Contact']['created'] = date('Y-m-d H:i:s');
                        $contact['Contact']['modified'] = date('Y-m-d H:i:s');

                        $this->Contact->create();
                        $this->Contact->save($contact);
                    }
                }


                $logs = array();
                $logs['Log']['employee_id'] = $employee['Employee']['id'];
                $logs['Log']['action_id'] = '3'; // add
                $logs['Log']['path'] = $this->here; //get current path
                $logs['Log']['project_id'] = '2'; //set project id
                $logs['Log']['created_by'] = $employee['Employee']['id'];
                $logs['Log']['created'] = date('Y-m-d H:i:s');
                $logs['Log']['modified_by'] = $employee['Employee']['id'];
                $logs['Log']['modified'] = date('Y-m-d H:i:s');

                $this->Log->create();
                $this->Log->save($logs);

                $this->Session->setFlash('Information successfully upload.', 'success');
                $this->redirect(array('action' => 'upload'));
            }
        }
    }
}