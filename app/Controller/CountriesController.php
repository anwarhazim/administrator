<?php
class CountriesController extends AppController 
{

	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter() 
    {
        parent::beforeFilter();
    }
    
    public function upload()
    {
        $this->loadModel('Personal');
        $this->loadModel('Employee');
        $this->loadModel('Country');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        if($this->request->is('post') || $this->request->is('put'))
        {
            App::import('Vendor', 'Spreadsheet_Excel_Reader', array('file' => 'excelreader'.DS.'excel_reader.php'));
            $excel = new PhpExcelReader; 

            $data = $this->request->data;

            if($data['Country']['attachment']['error'] == 0)
            {
                $excel->read($data['Country']['attachment']['tmp_name']);
                $cells = $excel->sheets[0]['cells'];
                $counter = 0;
                for ($i=2; $i <= count($cells); $i++) 
                {
                    $name = isset($cells[$i][1]) ? $cells[$i][1] : '';
                    $code = isset($cells[$i][2]) ? $cells[$i][2] : '';
                    $shortname = isset($cells[$i][3]) ? $cells[$i][3] : '';
                    
                    $country = array();
                    $country['Country']['name'] = $name;
                    $country['Country']['code'] = $code;
                    $country['Country']['shortname'] = $shortname;
                    $country['Country']['created_by'] = $employee['Employee']['id'];
                    $country['Country']['created'] = date('Y-m-d H:i:s');
                    $country['Country']['modified_by'] = $employee['Employee']['id'];
                    $country['Country']['modified'] = date('Y-m-d H:i:s');

                    $this->Country->create();
                    $this->Country->save($country);
                }


                $logs = array();
                $logs['Log']['employee_id'] = $employee['Employee']['id'];
                $logs['Log']['action_id'] = '3'; // add
                $logs['Log']['path'] = $this->here; //get current path
                $logs['Log']['project_id'] = '2'; //set project id
                $logs['Log']['created_by'] = $employee['Employee']['id'];
                $logs['Log']['created'] = date('Y-m-d H:i:s');
                $logs['Log']['modified_by'] = $employee['Employee']['id'];
                $logs['Log']['modified'] = date('Y-m-d H:i:s');

                $this->Log->create();
                $this->Log->save($logs);

                $this->Session->setFlash('Information successfully upload.', 'success');
                $this->redirect(array('action' => 'upload'));
            }
        }
    }
}