<?php
App::uses('AppController', 'Controller');

class BatchStaffsController extends AppController 
{
	public $components = array('RequestHandler', 'Paginator', 'Session');
	public $helpers = array('Html', 'Form', 'Session');
	
	public function index()
	{
        $this->loadModel('Employee');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        $conditions = array();

        $conditions['order'] = array('BatchStaff.id'=> 'DESC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['BatchStaff'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters,
            // we'll redirect to that page
            return $this->redirect($filter_url);
        }
        else
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "search")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('BatchStaff.name LIKE' => '%' . $value . '%')
                        );
                    }

					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(BatchStaff.modified) >=' => date("Y-m-d", strtotime($value))
                        );

                    }

                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(BatchStaff.modified) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc
                    $this->request->data['BatchStaff'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate();

        for ($i=0; $i < count($details); $i++)
        {
            $details[$i]['BatchStaff']['status_in'] = '<span class="badge badge-default">No Complete</span>';
            if($details[$i]['BatchStaff']['is_in'] == 1)
            {
                $details[$i]['BatchStaff']['status_in'] = '<span class="badge badge-success">Complete</span>';
            }

            $details[$i]['BatchStaff']['status_execute'] = '<span class="badge badge-default">No Execute</span>';
            if($details[$i]['BatchStaff']['execute'] == 1)
            {
                $details[$i]['BatchStaff']['status_execute'] = '<span class="badge badge-success">Execute</span>';
            }

            $details[$i]['BatchStaff']['modified'] = date("d-m-Y",strtotime($details[$i]['BatchStaff']['modified']));

            $details[$i]['BatchStaff']['created'] = date("d-m-Y",strtotime($details[$i]['BatchStaff']['created']));

            $details[$i]['BatchStaff']['key'] = $this->Utility->encrypt($details[$i]['BatchStaff']['id'], 'BatchStaff');
        }

        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // view
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '2'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['created'] = date('Y-m-d H:i:s');
        $logs['Log']['modified_by'] = $employee['Employee']['id'];
        $logs['Log']['modified'] = date('Y-m-d H:i:s');

        $this->Log->create();
        $this->Log->save($logs);

        $this->set(compact('details'));
    }

	public function add()
	{
        $this->loadModel('Employee');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);
        
		if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

			$this->BatchStaff->set($data);
            if($this->BatchStaff->validates())
            {
                // flag START
				$data['BatchStaff']['is_in'] = 99;
				$data['BatchStaff']['is_flag'] = 99;
                $data['BatchStaff']['execute'] = 99;
                // flag END

                $data['BatchStaff']['success'] = 0;
                $data['BatchStaff']['fail'] = 0;
                $data['BatchStaff']['total'] = 0;
                
                $data['BatchStaff']['created_by'] = $employee['Employee']['id'];
				$data['BatchStaff']['modified_by'] = $employee['Employee']['id'];

                $this->BatchStaff->create();
                $this->BatchStaff->save($data);
                
                $logs = array();
                $logs['Log']['employee_id'] = $employee['Employee']['id'];
                $logs['Log']['action_id'] = '3'; // add
                $logs['Log']['path'] = $this->here; //get current path
                $logs['Log']['project_id'] = '2'; //set project id
                $logs['Log']['created_by'] = $employee['Employee']['id'];
                $logs['Log']['modified_by'] = $employee['Employee']['id'];

                $this->Log->create();
                $this->Log->save($logs);

                $this->Session->setFlash('Information successfully saved.', 'success');
                $this->redirect(array('action' => 'add'));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully saved.', 'error');
            }
        }

        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // view
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '2'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['modified_by'] = $employee['Employee']['id'];

        $this->Log->create();
        $this->Log->save($logs);

	}

	public function view($key = null)
	{
        $this->loadModel('Employee');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'BatchStaff');

        $detail = $this->BatchStaff->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

		$detail['BatchStaff']['modified'] = date("d-m-Y",strtotime($detail['BatchStaff']['modified']));
		$detail['BatchStaff']['created'] = date("d-m-Y",strtotime($detail['BatchStaff']['created']));

		$this->request->data = $detail;
		
        $disabled = "disabled";
        
        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // view
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '2'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['modified_by'] = $employee['Employee']['id'];

        $this->Log->create();
        $this->Log->save($logs);

        $this->set(compact('key', 'disabled'));
	}

	public function edit($key = null)
	{
		$this->loadModel('Employee');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'BatchStaff');

        $detail = $this->BatchStaff->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
		}

		if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $data['BatchStaff']['id'] = $id;

			$this->BatchStaff->set($data);
            if($this->BatchStaff->validates())
            {
				$data['BatchStaff']['modified_by'] = $employee['Employee']['id'];

                $this->BatchStaff->create();
                $this->BatchStaff->save($data);
                
                $logs = array();
                $logs['Log']['employee_id'] = $employee['Employee']['id'];
                $logs['Log']['action_id'] = '4'; // edit
                $logs['Log']['path'] = $this->here; //get current path
                $logs['Log']['project_id'] = '2'; //set project id
                $logs['Log']['created_by'] = $employee['Employee']['id'];
                $logs['Log']['modified_by'] = $employee['Employee']['id'];

                $this->Log->create();
                $this->Log->save($logs);

                $this->Session->setFlash('Information successfully updated.', 'success');
                $this->redirect(array('action' => 'edit/'.$key));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully updated.', 'error');
            }
        }
        else
        {
            $this->request->data = $detail;
        }
		
        $disabled = "";
        
        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // view
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '2'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['modified_by'] = $employee['Employee']['id'];

        $this->Log->create();
        $this->Log->save($logs);

        $this->set(compact('key', 'disabled'));
		
    }
    
    public function listing()
    {
        $this->loadModel('Employee');
        $this->loadModel('InStaff');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        $filter_url['action'] = $this->request->params['action'];

        $key = "";

        if(!empty($this->params['named']['key']))
        {
            $key = $this->params['named']['key'];
        }

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        $id = $this->Utility->decrypt($key, 'BatchStaff');

        $detail = $this->BatchStaff->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        $conditions = array();

        $conditions['conditions'][] = array(
                                            'InStaff.batch_staff_id' => $detail['BatchStaff']['id'],
                                        );

        $conditions['order'] = array('InStaff.order'=> 'ASC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['InStaff'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters, 
            // we'll redirect to that page
            return $this->redirect($filter_url);
        } 
        else 
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "search")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('InStaff.employee_no LIKE' => '%' . $value . '%')
                        );

                        $conditions['conditions']['OR'][] = array(
                            array('InStaff.complete_name LIKE' => '%' . $value . '%')
                        );
                    } 
                    
					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(InStaff.modified) >=' => date("Y-m-d", strtotime($value))
                        );

                    }
					
                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(InStaff.modified) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc                 
                    $this->request->data['InStaff'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate('InStaff');

        for ($i=0; $i < count($details); $i++) 
        {

            $details[$i]['InStaff']['status_flag'] = '<span class="badge badge-default">No Complete</span>';
            if($details[$i]['InStaff']['is_flag'] == 1)
            {
                $details[$i]['InStaff']['status_flag'] = '<span class="badge badge-success">Complete</span>';
            }

            $details[$i]['InStaff']['status_error'] = '<span class="badge badge-success">No Error</span>';

            if($details[$i]['InStaff']['is_error'] == 1)
            {
                $details[$i]['InStaff']['status_error'] = '<span class="badge badge-danger">Error</span>';
            }
           
            if(!empty($details[$i]['InStaff']['executed']))
            {
                $details[$i]['InStaff']['executed'] = date("d-m-Y",strtotime($details[$i]['InStaff']['executed']));
            }
            else
            {
                $details[$i]['InStaff']['executed'] = '-';
            }

            $details[$i]['ExecuteBy'] = array();

            if(!empty($details[$i]['InStaff']['executed_by']))
            {
                $executed_by = $this->Utility->getEmployeeSummary($details[$i]['InStaff']['executed_by']);

                $details[$i]['ExecuteBy'] = $executed_by['Employee']; 
            }

            $details[$i]['InStaff']['key'] = $this->Utility->encrypt($details[$i]['InStaff']['id'], 'InStaff');
        }
        
        
        $this->set(compact('key', 'detail', 'details'));
    }

    public function error()
    {
        $this->loadModel('Employee');
        $this->loadModel('InStaff');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        $filter_url['action'] = $this->request->params['action'];

        $key = "";

        if(!empty($this->params['named']['key']))
        {
            $key = $this->params['named']['key'];
        }

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        $id = $this->Utility->decrypt($key, 'BatchStaff');

        $detail = $this->BatchStaff->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        $conditions = array();

        $conditions['conditions'][] = array(
                                            'InStaff.batch_staff_id' => $detail['BatchStaff']['id'],
                                            'InStaff.is_error' => 1,
                                        );

        $conditions['order'] = array('InStaff.order'=> 'ASC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['InStaff'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters, 
            // we'll redirect to that page
            return $this->redirect($filter_url);
        } 
        else 
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "search")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('InStaff.employee_no LIKE' => '%' . $value . '%')
                        );

                        $conditions['conditions']['OR'][] = array(
                            array('InStaff.complete_name LIKE' => '%' . $value . '%')
                        );
                    } 
                    
					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(InStaff.modified) >=' => date("Y-m-d", strtotime($value))
                        );

                    }
					
                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(InStaff.modified) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc                 
                    $this->request->data['InStaff'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate('InStaff');

        for ($i=0; $i < count($details); $i++) 
        {

            $details[$i]['InStaff']['status_flag'] = '<span class="badge badge-default">No Complete</span>';
            if($details[$i]['InStaff']['is_flag'] == 1)
            {
                $details[$i]['InStaff']['status_flag'] = '<span class="badge badge-success">Complete</span>';
            }

            $details[$i]['InStaff']['status_error'] = '<span class="badge badge-success">No Error</span>';
            if($details[$i]['InStaff']['is_error'] == 1)
            {
                $details[$i]['InStaff']['status_error'] = '<span class="badge badge-danger">Error</span>';
            }
           
            if(!empty($details[$i]['InStaff']['executed']))
            {
                $details[$i]['InStaff']['executed'] = date("d-m-Y",strtotime($details[$i]['InStaff']['executed']));
            }
            else
            {
                $details[$i]['InStaff']['executed'] = '-';
            }

            $details[$i]['ExecuteBy'] = array();

            if(!empty($details[$i]['InStaff']['executed_by']))
            {
                $executed_by = $this->Utility->getEmployeeSummary($details[$i]['InStaff']['executed_by']);

                $details[$i]['ExecuteBy'] = $executed_by['Employee']; 
            }

            $details[$i]['InStaff']['key'] = $this->Utility->encrypt($details[$i]['InStaff']['id'], 'InStaff');
        }
        
        $this->set(compact('key', 'detail', 'details'));
    }

    public function view_error($batchkey = null, $instaffkey = null)
    {
        $this->loadModel('InStaff');
        $this->loadModel('Employee');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        if(empty($batchkey))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $batch_staff_id = $this->Utility->decrypt($batchkey, 'BatchStaff');

        $batchStaff = $this->BatchStaff->findById($batch_staff_id);

        if(empty($batchStaff))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }
        
        if(empty($instaffkey))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $in_staff_id = $this->Utility->decrypt($instaffkey, 'InStaff');

        $inStaff = $this->InStaff->findById($in_staff_id);

        if(empty($inStaff))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $disabled = 'disabled';
        

        $this->set(compact('batchkey', 'instaffkey', 'batchStaff', 'inStaff', 'disabled'));
    }

    public function upload($key = null)
    {
        $this->loadModel('InStaff');
        $this->loadModel('Employee');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $batch_staff_id = $this->Utility->decrypt($key, 'BatchStaff');

        $detail = $this->BatchStaff->findById($batch_staff_id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            if($data['BatchStaff']['attachment']['error'] == 0)
            {
                switch ($data['BatchStaff']['type_id']) 
                {
                    case 1://Insert New 
                        
                        $file = file_get_contents($data['BatchStaff']['attachment']["tmp_name"], true);
                
                        $files = explode("\n", $file);
                        
                        foreach ($files as $file_value) 
                        {
                            $staff = array();
                            $staff['InStaff']['batch_staff_id'] = $detail['BatchStaff']['id'];
                            $staff['InStaff']['is_flag'] = 99;
                            $staff['InStaff']['is_error'] = 99;

                            $variables = array(
                                                'employee_no',
                                                'complete_name',
                                                'personal_area',
                                                'personal_subarea',
                                                'employee_group',
                                                'employee_subgroup',
                                                'payroll_area',
                                                'employment_status',
                                                'entry_date',
                                                'leaving_date',
                                                'seniority_date',
                                                'organizational_unit',
                                                'organizational_unit_id',
                                                'cost_center',
                                                'cost_center_code',
                                                'position',
                                                'position_id',
                                                'date_of_birth',
                                                'age',
                                                'gender',
                                                'ethnic',
                                                'years_of_services',
                                                'primary_school',
                                                'secondary_school',
                                                'university',
                                                'professional_body_1',
                                                'profesional_body_2',
                                                'group',
                                                'grp_id',
                                                'division',
                                                'div_id',
                                                'department',
                                                'dept_id',
                                                'section',
                                                'sec_id',
                                                'unit',
                                                'unit_id',
                                                'subunit',
                                                'subunit_id',
                                                'payscale_group',
                                                'payscale_level',
                                                'marital_status_code',
                                                'reporting_to',
                                                'ic_no',
                                                'socso_no',
                                                'er_socso_no',
                                                'epf_no',
                                                'er_epf_no',
                                                'tax_no',
                                                'er_tax_no',
                                                'date_of_married'
                                                );


                            if(!empty($file_value))
                            {
                                $file_contents = explode("|", $file_value);     
                                for ($i=0; $i < count($file_contents) ; $i++) 
                                {
                                    if(!empty($file_contents[$i]))
                                    { 
                                        switch ($file_contents[$i]) 
                                        {
                                            case 'Exec–Technical':
                                                $staff['InStaff'][$variables[$i]] = 'Exec Technical';
                                                break;
                                            case 'Exec–Non Technical':
                                                $staff['InStaff'][$variables[$i]] = 'Exec Non Technical';
                                                break;
                                            case 'Non Exec–Non Tech':
                                                $staff['InStaff'][$variables[$i]] = 'Non Exec Non Tech';
                                                break;
                                            case 'Non Exec–Technical':
                                                $staff['InStaff'][$variables[$i]] = 'Non Exec Technical';
                                                break;
                                            default:
                                                $staff['InStaff'][$variables[$i]] = $file_contents[$i];
                                                break;
                                        }
                                    }
                                }
                            }

                            $staff['InStaff']['created_by'] = $employee['Employee']['id'];
                            $staff['InStaff']['modified_by'] = $employee['Employee']['id'];

                            $this->InStaff->create();
                            $this->InStaff->save($staff); 

                        }

                        $success = $this->InStaff->find('count', array(
                                                            'conditions' => array(
                                                                array(
                                                                        'InStaff.batch_staff_id' => $detail['BatchStaff']['id'],
                                                                        'InStaff.is_flag' => 1,
                                                                        'InStaff.is_error' => 99,
                                                                    ),
                                                            )
                                                        ));

                        $fail = $this->InStaff->find('count', array(
                                                                'conditions' => array(
                                                                    array(
                                                                            'InStaff.batch_staff_id' => $detail['BatchStaff']['id'],
                                                                            'InStaff.is_flag' => 1,
                                                                            'InStaff.is_error' => 1,
                                                                        ),
                                                                )
                                                            ));

                        $total = $this->InStaff->find('count', array(
                                                                'conditions' => array(
                                                                    array(
                                                                            'InStaff.batch_staff_id' => $detail['BatchStaff']['id'],
                                                                        ),
                                                                )
                                                            ));

                        $edit = array();

                        $edit['BatchStaff']['id'] = $detail['BatchStaff']['id'];
                        $edit['BatchStaff']['success'] = $success;
                        $edit['BatchStaff']['fail'] = $fail;
                        $edit['BatchStaff']['total'] = $total;
                        $edit['BatchStaff']['execute'] = 99;

                        $this->BatchStaff->create();
                        $this->BatchStaff->save($edit);

                        $this->Session->setFlash('Information successfully uploaded.', 'success');
                        $this->redirect(array('action' => 'upload/'.$key));

                        break;
                    case 2://Update data
                        
                        $file = file_get_contents($data['BatchStaff']['attachment']["tmp_name"], true);
                
                        $files = explode("\n", $file);
                        
                        foreach ($files as $file_value) 
                        {
                            $staff = array();
                            $staff['InStaff']['batch_staff_id'] = $detail['BatchStaff']['id'];
        
                            $variables = array(
                                                'employee_no',
                                                'complete_name',
                                                'personal_area',
                                                'personal_subarea',
                                                'employee_group',
                                                'employee_subgroup',
                                                'payroll_area',
                                                'employment_status',
                                                'entry_date',
                                                'leaving_date',
                                                'seniority_date',
                                                'organizational_unit',
                                                'organizational_unit_id',
                                                'cost_center',
                                                'cost_center_code',
                                                'position',
                                                'position_id',
                                                'date_of_birth',
                                                'age',
                                                'gender',
                                                'ethnic',
                                                'years_of_services',
                                                'primary_school',
                                                'secondary_school',
                                                'university',
                                                'professional_body_1',
                                                'profesional_body_2',
                                                'group',
                                                'grp_id',
                                                'division',
                                                'div_id',
                                                'department',
                                                'dept_id',
                                                'section',
                                                'sec_id',
                                                'unit',
                                                'unit_id',
                                                'subunit',
                                                'subunit_id',
                                                'payscale_group',
                                                'payscale_level',
                                                'marital_status_code',
                                                'reporting_to',
                                                'ic_no',
                                                'socso_no',
                                                'er_socso_no',
                                                'epf_no',
                                                'er_epf_no',
                                                'tax_no',
                                                'er_tax_no',
                                                'date_of_married'
                                                );


                            if(!empty($file_value))
                            {
                                $file_contents = explode("|", $file_value);     
                                for ($i=0; $i < count($file_contents) ; $i++) 
                                {
                                    if(!empty($file_contents[$i]))
                                    { 
                                        switch ($file_contents[$i]) 
                                        {
                                            case 'Exec–Technical':
                                                $staff['InStaff'][$variables[$i]] = 'Exec Technical';
                                                break;
                                            case 'Exec–Non Technical':
                                                $staff['InStaff'][$variables[$i]] = 'Exec Non Technical';
                                                break;
                                            case 'Non Exec–Non Tech':
                                                $staff['InStaff'][$variables[$i]] = 'Non Exec Non Tech';
                                                break;
                                            case 'Non Exec–Technical':
                                                $staff['InStaff'][$variables[$i]] = 'Non Exec Technical';
                                                break;
                                            default:
                                                $staff['InStaff'][$variables[$i]] = $file_contents[$i];
                                                break;
                                        }
                                    }
                                }
                            }

                            $staff['InStaff']['created_by'] = $employee['Employee']['id'];
                            $staff['InStaff']['modified_by'] = $employee['Employee']['id'];

                            if(!empty($staff['InStaff']['employee_no']))
                            {
                                $check = $this->InStaff->find('first',
                                                                    array(
                                                                        'conditions' => array(
                                                                                            'InStaff.batch_staff_id' => $detail['BatchStaff']['id'], 
                                                                                            'InStaff.employee_no' => $staff['InStaff']['employee_no'],
                                                                                        ),
                                                                    ));
                                    
                                if(!empty($check))
                                {
                                    $staff['InStaff']['id'] = $check['InStaff']['id'];  

                                    $this->InStaff->create();
                                    $this->InStaff->save($staff); 
                                }
                            }

                        }

                        $success = $this->InStaff->find('count', array(
                                                            'conditions' => array(
                                                                array(
                                                                        'InStaff.batch_staff_id' => $detail['BatchStaff']['id'],
                                                                        'InStaff.is_flag' => 1,
                                                                        'InStaff.is_error' => 99,
                                                                    ),
                                                            )
                                                        ));

                        $fail = $this->InStaff->find('count', array(
                                                                'conditions' => array(
                                                                    array(
                                                                            'InStaff.batch_staff_id' => $detail['BatchStaff']['id'],
                                                                            'InStaff.is_flag' => 1,
                                                                            'InStaff.is_error' => 1,
                                                                        ),
                                                                )
                                                            ));

                        $total = $this->InStaff->find('count', array(
                                                                'conditions' => array(
                                                                    array(
                                                                            'InStaff.batch_staff_id' => $detail['BatchStaff']['id'],
                                                                        ),
                                                                )
                                                            ));

                        $edit = array();

                        $edit['BatchStaff']['id'] = $detail['BatchStaff']['id'];
                        $edit['BatchStaff']['success'] = $success;
                        $edit['BatchStaff']['fail'] = $fail;
                        $edit['BatchStaff']['total'] = $total;
                        $edit['BatchStaff']['execute'] = 99;

                        $this->BatchStaff->create();
                        $this->BatchStaff->save($edit);

                        $this->Session->setFlash('Information successfully uploaded.', 'success');
                        $this->redirect(array('action' => 'upload/'.$key));

                        break;
                }
            }
        }

        $is_types = array(1 => 'New Data', 2 => 'Update Data');
        
        $this->set(compact('key', 'detail', 'is_types'));
    }

    public function executeAll($key = null)
    {
        $this->loadModel('Employee');
        $this->loadModel('InStaff');
        $this->loadModel('PersonalArea');
        $this->loadModel('PersonalSubarea');
        $this->loadModel('EmployeeGroup');
        $this->loadModel('EmployeeSubgroup');
        $this->loadModel('PayrollArea');
        $this->loadModel('StaffStatus');
        $this->loadModel('Organisation');
        $this->loadModel('CostCenter');
        $this->loadModel('Position');
        $this->loadModel('Gender');
        $this->loadModel('Ethnic');
        $this->loadModel('Marital');
        $this->loadModel('JobGrade');
        $this->loadModel('Notification');
        $this->loadModel('User');
        $this->loadModel('UserRole');
        $this->loadModel('Employee');
        $this->loadModel('Personal');
        $this->loadModel('PersonalMarital');
        $this->loadModel('Log');
        $this->loadModel('Utility');
        
        $person = $this->Auth->user();
        $session = $this->Utility->getUserInformation($person['id']);

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        $id = $this->Utility->decrypt($key, 'BatchStaff');

        $detail = $this->BatchStaff->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        $update = array();
        $update['BatchStaff']['id'] = $detail['BatchStaff']['id'];
        $update['BatchStaff']['executed_by'] = $session['Employee']['id'];
        $update['BatchStaff']['executed_start'] = date('Y-m-d H:i:s');

        $this->BatchStaff->create();
        $this->BatchStaff->save($update);

        $instaffs = $this->InStaff->find('all',
                                            array(
                                                'conditions' => array(
                                                                    'InStaff.batch_staff_id' => $detail['BatchStaff']['id'],
                                                                    'InStaff.is_flag' => 99,
                                                                ),
                                            ));
        
        if(empty($instaffs))
        {
            $this->Session->setFlash('No staff information avaiable.', 'info');
            $this->redirect(array('action' => '/list_sync/'.$batchkey));
        }

        foreach ($instaffs as $instaff) 
        {
            $check = $this->Employee->find('count',
                                            array(
                                                'conditions' => array(
                                                                    'Employee.employee_no' => $instaff['InStaff']['employee_no'],
                                                                ),
                                            ));

            if($check > 0)
            {
                $current = $this->Employee->find('first',
                                                        array(
                                                            'conditions' => array(
                                                                                'Employee.employee_no' => $instaff['InStaff']['employee_no'],
                                                                            ),
                                                            'order' => array('Employee.id' => 'DESC'),
                                                        ));

                if(!empty($current))
                {
                    //if employee exist and still active
                    if($current['Employee']['employment_status_id'] == 1)
                    {
                        //check if data from SAP say employee status is active
                        if($instaff['InStaff']['employment_status'] == 'Active')
                        {
                            //check the jobgrade
                            $jobgrade = $this->JobGrade->findByName($instaff['InStaff']['payscale_group']);

                            /*
                                if employee :
                                1.  difference organisation unit from SAP than in DB
                                    OR
                                2.  difference job designation unit from SAP than in DB
                                    OR
                                3. difference job grade from SAP than in DB
                            */
                            if(($instaff['InStaff']['organizational_unit_id'] != $current['Organisation']['code']) || ($jobgrade['JobGrade']['job_designation_id'] != $current['JobDesignation']['id']) || ($jobgrade['JobGrade']['id'] != $current['JobGrade']['id']))
                            {
                                //Disabled employee 
                                $update = array();
                                $update['Employee']['id'] = $current['Employee']['id'];
                                $update['Employee']['is_active'] = 99;
                                $update['Employee']['modified_by'] = $session['Employee']['id'];
        
                                $this->Employee->create();
                                $this->Employee->save($update);

                                //set all employee information
                                $employee = array();

                                $employee['Employee']['id'] = NULL;
                                $employee['Employee']['batch_staff_id'] = $detail['BatchStaff']['id'];
                                $employee['Employee']['employee_no'] = $instaff['InStaff']['employee_no'];
                                $employee['Employee']['complete_name'] = $instaff['InStaff']['complete_name'];
                                $employee['Employee']['is_type'] = 1; // 1 for data from SAP, 99 for data from backend
                                $employee['Employee']['is_flag'] = 1; // triger for SAP, if 99 no need to send to SAP
                                $employee['Employee']['is_active'] = 1;
                                $employee['Employee']['status_id'] = 10;
                                $employee['Employee']['parent_id'] = $current['Employee']['id'];
                                $employee['Employee']['personal_id'] = $current['Employee']['personal_id'];
            
                                $employee['Employee']['personal_area_id'] = NULL;
            
                                $personalarea = $this->PersonalArea->findByName($instaff['InStaff']['personal_area']);
                                if(!empty($personalarea))
                                {
                                    $employee['Employee']['personal_area_id'] = $personalarea['PersonalArea']['id'];
                                }
                                
                                $employee['Employee']['personal_subarea_id'] = NULL;
            
                                $personalsubarea = $this->PersonalSubarea->find('first',
                                                                                        array(
                                                                                            'conditions' => array(
                                                                                                                'PersonalSubarea.area' => $personalarea['PersonalArea']['area'],
                                                                                                                'PersonalSubarea.name' => $instaff['InStaff']['personal_subarea'],
                                                                                                            ),
                                                                                        ));
            
                                if(!empty($personalsubarea))
                                {
                                    $employee['Employee']['personal_subarea_id'] = $personalsubarea['PersonalSubarea']['id'];
                                }
                                
                                $employee['Employee']['employee_group_id'] = NULL;
            
                                $employeegroup = $this->EmployeeGroup->findByName($instaff['InStaff']['employee_group']);
                                if(!empty($employeegroup))
                                {
                                    $employee['Employee']['employee_group_id'] = $employeegroup['EmployeeGroup']['id'];
                                }
            
                                $employee['Employee']['employee_subgroup_id'] = NULL;
            
                                $employeesubgroup = $this->EmployeeSubgroup->find('first',
                                                                                        array(
                                                                                            'conditions' => array(
                                                                                                                'EmployeeSubgroup.EEGrp' => $employeegroup['EmployeeGroup']['code'],
                                                                                                                'EmployeeSubgroup.name' => $instaff['InStaff']['employee_subgroup'],
                                                                                                            ),
                                                                                        ));
                                                                                        
                                if(!empty($employeesubgroup))
                                {
                                    $employee['Employee']['employee_subgroup_id'] = $employeesubgroup['EmployeeSubgroup']['id'];
                                }
            
                                $employee['Employee']['payroll_area_id'] = NULL;
            
                                $payrollarea = $this->PayrollArea->findByName($instaff['InStaff']['payroll_area']);
                                if(!empty($payrollarea))
                                {
                                    $employee['Employee']['payroll_area_id'] = $payrollarea['PayrollArea']['id'];
                                }
            
                                $employee['Employee']['employment_status_id'] = NULL;
            
                                $staffstatus = $this->StaffStatus->findByName($instaff['InStaff']['employment_status']);
                                if(!empty($staffstatus))
                                {
                                    $employee['Employee']['employment_status_id'] = $staffstatus['StaffStatus']['id'];
                                }
            
                                $employee['Employee']['entry_date'] = NULL;
            
                                if(!empty($instaff['InStaff']['entry_date']))
                                {
                                    $employee['Employee']['entry_date'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['entry_date']);
                                }
            
                                if(!empty($instaff['InStaff']['leaving_date']))
                                {
                                    $employee['Employee']['leaving_date'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['leaving_date']);
                                }
            
                                $employee['Employee']['seniority_date'] = NULL;
            
                                if(!empty($instaff['InStaff']['seniority_date']))
                                {
                                    $employee['Employee']['seniority_date'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['seniority_date']);
                                }
            
                                $employee['Employee']['organizational_unit_id'] = NULL;
                                $employee['Employee']['organizational_unit_code'] = NULL;
            
                                if(!empty($instaff['InStaff']['organizational_unit_id']))
                                {
                                    $organisationunit = $this->Organisation->find('first',
                                                                                        array(
                                                                                            'conditions' => array(
                                                                                                                'Organisation.code' => $instaff['InStaff']['organizational_unit_id'],
                                                                                                                'Organisation.is_active' => 1,
                                                                                                                'Organisation.is_deleted' => 99,
                                                                                                                'BatchOrganisation.is_active' => 1
                                                                                                            ),
                                                                                        ));
                                    if(!empty($organisationunit))
                                    {
                                        $employee['Employee']['organizational_unit_id'] = $organisationunit['Organisation']['id'];
                                        $employee['Employee']['organizational_unit_code'] = $organisationunit['Organisation']['code'];
                                    }
                                }
            
                                $employee['Employee']['cost_center_id'] = NULL;
                                $employee['Employee']['cost_center_code'] = NULL;
            
                                if(!empty($instaff['InStaff']['cost_center_code']))
                                {
                                    $costcenter = $this->CostCenter->findByCode($instaff['InStaff']['cost_center_code']);
            
                                    if(!empty($costcenter))
                                    {
                                        $employee['Employee']['cost_center_id'] = $costcenter['CostCenter']['id'];
                                        $employee['Employee']['cost_center_code'] = $costcenter['CostCenter']['code'];
                                    }
                                }
            
                                $employee['Employee']['position_id'] = NULL;
                                $employee['Employee']['position_code'] = NULL;
            
                                if(!empty($instaff['InStaff']['position_id']))
                                {
                                    $position = $this->Position->findByCode($instaff['InStaff']['position_id']);
            
                                    if(!empty($position))
                                    {
                                        $employee['Employee']['position_id'] = $position['Position']['id'];
                                        $employee['Employee']['position_code'] = $position['Position']['code'];
                                    }
                                }
            
                                if(!empty($instaff['InStaff']['grp_id']))
                                {
                                    $employee['Employee']['group_code'] = $instaff['InStaff']['grp_id'];
                                }
            
                                if(!empty($instaff['InStaff']['div_id']))
                                {
                                    $employee['Employee']['division_code'] = $instaff['InStaff']['div_id'];
                                }
            
                                if(!empty($instaff['InStaff']['dept_id']))
                                {
                                    $employee['Employee']['department_code'] = $instaff['InStaff']['dept_id'];
                                }
            
                                if(!empty($instaff['InStaff']['sec_id']))
                                {
                                    $employee['Employee']['section_code'] = $instaff['InStaff']['sec_id'];
                                }
            
                                if(!empty($instaff['InStaff']['unit_id']))
                                {
                                    $employee['Employee']['unit_code'] = $instaff['InStaff']['unit_id'];
                                }
                                
                                if(!empty($instaff['InStaff']['subunit_id']))
                                {
                                    $employee['Employee']['subunit_code'] = $instaff['InStaff']['subunit_id'];
                                }
            
                                $employee['Employee']['job_grade_id'] = NULL;
            
                                if(!empty($instaff['InStaff']['payscale_group']))
                                {
                                    $jobgrade = $this->JobGrade->findByName($instaff['InStaff']['payscale_group']);
            
                                    if(!empty($jobgrade))
                                    {
                                        $employee['Employee']['job_grade_id'] = $jobgrade['JobGrade']['id'];
                                        $employee['Employee']['job_designation_id'] = $jobgrade['JobGrade']['job_designation_id'];
                                    }
                                }
            
                                $employee['Employee']['payscale_level'] = $instaff['InStaff']['payscale_level'];
                                $employee['Employee']['executed'] = date('Y-m-d H:i:s');
                                

                                // set and validate the employee information 
                                $this->Employee->set($employee);
                                if($this->Employee->validates())
                                {
                                    // insert staff if success insert
                                    $this->Employee->create();
                                    $this->Employee->save($employee);
            
                                    $employee = $this->Employee->read(null,$this->Employee->id);
            
                                    // update instaff if success insert
                                    $instaff['InStaff']['is_flag'] = 1;
                                    $instaff['InStaff']['is_error'] = 99;
                                    $instaff['InStaff']['note'] = 'Have changes';
            
                                    $this->InStaff->create();
                                    $this->InStaff->save($instaff);
            
                                    $notification = array();
            
                                    $subject = "Welcome to PRESS ".$employee['Personal']['complete_name'];
                                    $body = "<p style='font-size:12px; font-family:arial;'>There are changes on your employee information. Could I kindly ask you to check and verify.</p>";
                                    $body = "<br/>";
                                    $body .= "Click <a href='".Router::url('/Users/profile' , true)."'>here</a> to view your profile.";
                                    $body = "<br/>";
                                    $body = "<p style='font-size:12px; font-family:arial;'>let ask know if there is any mistake.</p>";
            
                                    $notification = array();
            
                                    //$notification['Notification']['sender_id'] = $staff['Employee']['id'];
                                    $notification['Notification']['recipient_id'] = $employee['Employee']['id'];
                                    $notification['Notification']['is_read'] = 99;
                                    $notification['Notification']['subject'] = $subject;
                                    $notification['Notification']['body'] = $body;
                                    // $notification['Notification']['created_by'] = $staff['Employee']['id'];
                                    // $notification['Notification']['modified_by'] = $staff['Employee']['id'];
            
                                    $this->Notification->create();
                                    $this->Notification->save($notification);
                                }
                                else
                                {
                                    //catch error validation if the information complete
                                    $instaff['InStaff']['is_flag'] = 1;
                                    $instaff['InStaff']['is_error'] = 1;
                                    $instaff['InStaff']['note'] = json_encode($this->Employee->validationErrors);
                                    $instaff['InStaff']['executed_by'] = $session['Employee']['id'];
                                    $instaff['InStaff']['executed'] = date('Y-m-d H:i:s');
        
                                    $this->InStaff->create();
                                    $this->InStaff->save($instaff);
                                }
                            }
                            else
                            {
                                // if not, just bypass the information and flag as check
                                $instaff['InStaff']['is_flag'] = 1;
                                $instaff['InStaff']['is_error'] = 99;
                                $instaff['InStaff']['note'] = 'Nothing change';
                                $instaff['InStaff']['executed_by'] = $session['Employee']['id'];
                                $instaff['InStaff']['executed'] = date('Y-m-d H:i:s');

                                $this->InStaff->create();
                                $this->InStaff->save($instaff);
                            }
                        }
                        else
                        {
                            //check if data from SAP say employee status is not active

                            //update employee status
                            $update = array();
                            $update['Employee']['id'] = $current['Employee']['id'];
                            $update['Employee']['employment_status_id'] = 3;
                            switch ($instaff['InStaff']['employment_status']) 
                            {
                                case 'Inactive':
                                    $update['Employee']['employment_status_id'] = 2;
                                    break;
                                case 'Withdrawn':
                                    $update['Employee']['employment_status_id'] = 3;
                                    break;
                            }
                            $update['Employee']['modified_by'] = $session['Employee']['id'];
                            $this->Employee->create();
                            $this->Employee->save($update);

                            //disabled user login
                            $user_id = $current['Personal']['user_id'];

                            $update = array();
                            $update['User']['id'] = $user_id;
                            $update['User']['is_active'] = 99;
                            $update['User']['modified_by'] = $session['Employee']['id'];
                            $this->User->create();
                            $this->User->save($update);


                            // update instaff if success insert
                            $instaff['InStaff']['is_flag'] = 1;
                            $instaff['InStaff']['is_error'] = 99;
                            $instaff['InStaff']['note'] = 'Disabled employee';
    
                            $this->InStaff->create();
                            $this->InStaff->save($instaff);
                        }
                    }
                    else
                    {// if employee exist but not active
                        
                        //check if data from SAP say employee status is active
                        if($instaff['InStaff']['employment_status'] == 'Active')
                        {
                            //check the jobgrade
                            $jobgrade = $this->JobGrade->findByName($instaff['InStaff']['payscale_group']);

                            /*
                                if employee :
                                1.  difference organisation unit from SAP than in DB
                                    OR
                                2.  difference job designation unit from SAP than in DB
                                    OR
                                3. difference job grade from SAP than in DB
                            */
                            if(($instaff['InStaff']['organizational_unit_id'] != $current['Organisation']['code']) || ($jobgrade['JobGrade']['job_designation_id'] != $current['JobDesignation']['id']) || ($jobgrade['JobGrade']['id'] != $current['JobGrade']['id']))
                            {
                                //Disabled employee 
                                $update = array();
                                $update['Employee']['id'] = $current['Employee']['id'];
                                $update['Employee']['is_active'] = 99;
                                $update['Employee']['modified_by'] = $session['Employee']['id'];
        
                                $this->Employee->create();
                                $this->Employee->save($update);

                                //set all employee information
                                $employee = array();

                                $employee['Employee']['id'] = NULL;
                                $employee['Employee']['batch_staff_id'] = $detail['BatchStaff']['id'];
                                $employee['Employee']['employee_no'] = $instaff['InStaff']['employee_no'];
                                $employee['Employee']['complete_name'] = $instaff['InStaff']['complete_name'];
                                $employee['Employee']['is_type'] = 1; // 1 for data from SAP, 99 for data from backend
                                $employee['Employee']['is_flag'] = 1; // triger for SAP, if 99 no need to send to SAP
                                $employee['Employee']['is_active'] = 1;
                                $employee['Employee']['status_id'] = 10;
                                $employee['Employee']['parent_id'] = $current['Employee']['id'];
                                $employee['Employee']['personal_id'] = $current['Employee']['personal_id'];
            
                                $employee['Employee']['personal_area_id'] = NULL;
            
                                $personalarea = $this->PersonalArea->findByName($instaff['InStaff']['personal_area']);
                                if(!empty($personalarea))
                                {
                                    $employee['Employee']['personal_area_id'] = $personalarea['PersonalArea']['id'];
                                }
                                
                                $employee['Employee']['personal_subarea_id'] = NULL;
            
                                $personalsubarea = $this->PersonalSubarea->find('first',
                                                                                        array(
                                                                                            'conditions' => array(
                                                                                                                'PersonalSubarea.area' => $personalarea['PersonalArea']['area'],
                                                                                                                'PersonalSubarea.name' => $instaff['InStaff']['personal_subarea'],
                                                                                                            ),
                                                                                        ));
            
                                if(!empty($personalsubarea))
                                {
                                    $employee['Employee']['personal_subarea_id'] = $personalsubarea['PersonalSubarea']['id'];
                                }
                                
                                $employee['Employee']['employee_group_id'] = NULL;
            
                                $employeegroup = $this->EmployeeGroup->findByName($instaff['InStaff']['employee_group']);
                                if(!empty($employeegroup))
                                {
                                    $employee['Employee']['employee_group_id'] = $employeegroup['EmployeeGroup']['id'];
                                }
            
                                $employee['Employee']['employee_subgroup_id'] = NULL;
            
                                $employeesubgroup = $this->EmployeeSubgroup->find('first',
                                                                                        array(
                                                                                            'conditions' => array(
                                                                                                                'EmployeeSubgroup.EEGrp' => $employeegroup['EmployeeGroup']['code'],
                                                                                                                'EmployeeSubgroup.name' => $instaff['InStaff']['employee_subgroup'],
                                                                                                            ),
                                                                                        ));
                                                                                        
                                if(!empty($employeesubgroup))
                                {
                                    $employee['Employee']['employee_subgroup_id'] = $employeesubgroup['EmployeeSubgroup']['id'];
                                }
            
                                $employee['Employee']['payroll_area_id'] = NULL;
            
                                $payrollarea = $this->PayrollArea->findByName($instaff['InStaff']['payroll_area']);
                                if(!empty($payrollarea))
                                {
                                    $employee['Employee']['payroll_area_id'] = $payrollarea['PayrollArea']['id'];
                                }
            
                                $employee['Employee']['employment_status_id'] = NULL;
            
                                $staffstatus = $this->StaffStatus->findByName($instaff['InStaff']['employment_status']);
                                if(!empty($staffstatus))
                                {
                                    $employee['Employee']['employment_status_id'] = $staffstatus['StaffStatus']['id'];
                                }
            
                                $employee['Employee']['entry_date'] = NULL;
            
                                if(!empty($instaff['InStaff']['entry_date']))
                                {
                                    $employee['Employee']['entry_date'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['entry_date']);
                                }
            
                                if(!empty($instaff['InStaff']['leaving_date']))
                                {
                                    $employee['Employee']['leaving_date'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['leaving_date']);
                                }
            
                                $employee['Employee']['seniority_date'] = NULL;
            
                                if(!empty($instaff['InStaff']['seniority_date']))
                                {
                                    $employee['Employee']['seniority_date'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['seniority_date']);
                                }
            
                                $employee['Employee']['organizational_unit_id'] = NULL;
                                $employee['Employee']['organizational_unit_code'] = NULL;
            
                                if(!empty($instaff['InStaff']['organizational_unit_id']))
                                {
                                    $organisationunit = $this->Organisation->find('first',
                                                                                        array(
                                                                                            'conditions' => array(
                                                                                                                'Organisation.code' => $instaff['InStaff']['organizational_unit_id'],
                                                                                                                'Organisation.is_active' => 1,
                                                                                                                'Organisation.is_deleted' => 99,
                                                                                                                'BatchOrganisation.is_active' => 1
                                                                                                            ),
                                                                                        ));
                                    if(!empty($organisationunit))
                                    {
                                        $employee['Employee']['organizational_unit_id'] = $organisationunit['Organisation']['id'];
                                        $employee['Employee']['organizational_unit_code'] = $organisationunit['Organisation']['code'];
                                    }
                                }
            
                                $employee['Employee']['cost_center_id'] = NULL;
                                $employee['Employee']['cost_center_code'] = NULL;
            
                                if(!empty($instaff['InStaff']['cost_center_code']))
                                {
                                    $costcenter = $this->CostCenter->findByCode($instaff['InStaff']['cost_center_code']);
            
                                    if(!empty($costcenter))
                                    {
                                        $employee['Employee']['cost_center_id'] = $costcenter['CostCenter']['id'];
                                        $employee['Employee']['cost_center_code'] = $costcenter['CostCenter']['code'];
                                    }
                                }
            
                                $employee['Employee']['position_id'] = NULL;
                                $employee['Employee']['position_code'] = NULL;
            
                                if(!empty($instaff['InStaff']['position_id']))
                                {
                                    $position = $this->Position->findByCode($instaff['InStaff']['position_id']);
            
                                    if(!empty($position))
                                    {
                                        $employee['Employee']['position_id'] = $position['Position']['id'];
                                        $employee['Employee']['position_code'] = $position['Position']['code'];
                                    }
                                }
            
                                if(!empty($instaff['InStaff']['grp_id']))
                                {
                                    $employee['Employee']['group_code'] = $instaff['InStaff']['grp_id'];
                                }
            
                                if(!empty($instaff['InStaff']['div_id']))
                                {
                                    $employee['Employee']['division_code'] = $instaff['InStaff']['div_id'];
                                }
            
                                if(!empty($instaff['InStaff']['dept_id']))
                                {
                                    $employee['Employee']['department_code'] = $instaff['InStaff']['dept_id'];
                                }
            
                                if(!empty($instaff['InStaff']['sec_id']))
                                {
                                    $employee['Employee']['section_code'] = $instaff['InStaff']['sec_id'];
                                }
            
                                if(!empty($instaff['InStaff']['unit_id']))
                                {
                                    $employee['Employee']['unit_code'] = $instaff['InStaff']['unit_id'];
                                }
                                
                                if(!empty($instaff['InStaff']['subunit_id']))
                                {
                                    $employee['Employee']['subunit_code'] = $instaff['InStaff']['subunit_id'];
                                }
            
                                $employee['Employee']['job_grade_id'] = NULL;
            
                                if(!empty($instaff['InStaff']['payscale_group']))
                                {
                                    $jobgrade = $this->JobGrade->findByName($instaff['InStaff']['payscale_group']);
            
                                    if(!empty($jobgrade))
                                    {
                                        $employee['Employee']['job_grade_id'] = $jobgrade['JobGrade']['id'];
                                        $employee['Employee']['job_designation_id'] = $jobgrade['JobGrade']['job_designation_id'];
                                    }
                                }
            
                                $employee['Employee']['payscale_level'] = $instaff['InStaff']['payscale_level'];
                                $employee['Employee']['executed'] = date('Y-m-d H:i:s');
                                

                                // set and validate the employee information 
                                $this->Employee->set($employee);
                                if($this->Employee->validates())
                                {
                                    // insert staff if success insert
                                    $this->Employee->create();
                                    $this->Employee->save($employee);
            
                                    $employee = $this->Employee->read(null,$this->Employee->id);
            
                                    // update instaff if success insert
                                    $instaff['InStaff']['is_flag'] = 1;
                                    $instaff['InStaff']['is_error'] = 99;
                                    $instaff['InStaff']['note'] = 'Have changes';
            
                                    $this->InStaff->create();
                                    $this->InStaff->save($instaff);
            
                                    $notification = array();
            
                                    $subject = "Welcome to PRESS ".$employee['Personal']['complete_name'];
                                    $body = "<p style='font-size:12px; font-family:arial;'>There are changes on your employee information. Could I kindly ask you to check and verify.</p>";
                                    $body = "<br/>";
                                    $body .= "Click <a href='".Router::url('/Users/profile' , true)."'>here</a> to view your profile.";
                                    $body = "<br/>";
                                    $body = "<p style='font-size:12px; font-family:arial;'>let ask know if there is any mistake.</p>";
            
                                    $notification = array();
            
                                    //$notification['Notification']['sender_id'] = $staff['Employee']['id'];
                                    $notification['Notification']['recipient_id'] = $employee['Employee']['id'];
                                    $notification['Notification']['is_read'] = 99;
                                    $notification['Notification']['subject'] = $subject;
                                    $notification['Notification']['body'] = $body;
                                    // $notification['Notification']['created_by'] = $staff['Employee']['id'];
                                    // $notification['Notification']['modified_by'] = $staff['Employee']['id'];
            
                                    $this->Notification->create();
                                    $this->Notification->save($notification);
                                }
                                else
                                {
                                    //catch error validation if the information complete
                                    $instaff['InStaff']['is_error'] = 1;
                                    $instaff['InStaff']['is_flag'] = 1;
                                    $instaff['InStaff']['note'] = json_encode($this->Employee->validationErrors);
                                    $instaff['InStaff']['executed_by'] = $session['Employee']['id'];
                                    $instaff['InStaff']['executed'] = date('Y-m-d H:i:s');
        
                                    $this->InStaff->create();
                                    $this->InStaff->save($instaff);
                                }
                            }
                            else
                            {
                                //update employee status
                                //set all employee information
                                $employee = array();
                                $employee['Employee']['id'] = $current['Employee']['id'];
                                $employee['Employee']['batch_staff_id'] = $detail['BatchStaff']['id'];
                                $employee['Employee']['employee_no'] = $instaff['InStaff']['employee_no'];
                                $employee['Employee']['complete_name'] = $instaff['InStaff']['complete_name'];
                                $employee['Employee']['is_type'] = 1; // 1 for data from SAP, 99 for data from backend
                                $employee['Employee']['is_flag'] = 1; // triger for SAP, if 99 no need to send to SAP
                                $employee['Employee']['is_active'] = 1;
                                $employee['Employee']['status_id'] = 10;
            
                                $employee['Employee']['personal_area_id'] = NULL;
            
                                $personalarea = $this->PersonalArea->findByName($instaff['InStaff']['personal_area']);
                                if(!empty($personalarea))
                                {
                                    $employee['Employee']['personal_area_id'] = $personalarea['PersonalArea']['id'];
                                }
                                
                                $employee['Employee']['personal_subarea_id'] = NULL;
            
                                $personalsubarea = $this->PersonalSubarea->find('first',
                                                                                        array(
                                                                                            'conditions' => array(
                                                                                                                'PersonalSubarea.area' => $personalarea['PersonalArea']['area'],
                                                                                                                'PersonalSubarea.name' => $instaff['InStaff']['personal_subarea'],
                                                                                                            ),
                                                                                        ));
            
                                if(!empty($personalsubarea))
                                {
                                    $employee['Employee']['personal_subarea_id'] = $personalsubarea['PersonalSubarea']['id'];
                                }
                                
                                $employee['Employee']['employee_group_id'] = NULL;
            
                                $employeegroup = $this->EmployeeGroup->findByName($instaff['InStaff']['employee_group']);
                                if(!empty($employeegroup))
                                {
                                    $employee['Employee']['employee_group_id'] = $employeegroup['EmployeeGroup']['id'];
                                }
            
                                $employee['Employee']['employee_subgroup_id'] = NULL;
            
                                $employeesubgroup = $this->EmployeeSubgroup->find('first',
                                                                                        array(
                                                                                            'conditions' => array(
                                                                                                                'EmployeeSubgroup.EEGrp' => $employeegroup['EmployeeGroup']['code'],
                                                                                                                'EmployeeSubgroup.name' => $instaff['InStaff']['employee_subgroup'],
                                                                                                            ),
                                                                                        ));
                                                                                        
                                if(!empty($employeesubgroup))
                                {
                                    $employee['Employee']['employee_subgroup_id'] = $employeesubgroup['EmployeeSubgroup']['id'];
                                }
            
                                $employee['Employee']['payroll_area_id'] = NULL;
            
                                $payrollarea = $this->PayrollArea->findByName($instaff['InStaff']['payroll_area']);
                                if(!empty($payrollarea))
                                {
                                    $employee['Employee']['payroll_area_id'] = $payrollarea['PayrollArea']['id'];
                                }
            
                                $employee['Employee']['employment_status_id'] = NULL;
            
                                $staffstatus = $this->StaffStatus->findByName($instaff['InStaff']['employment_status']);
                                if(!empty($staffstatus))
                                {
                                    $employee['Employee']['employment_status_id'] = $staffstatus['StaffStatus']['id'];
                                }
            
                                $employee['Employee']['entry_date'] = NULL;
            
                                if(!empty($instaff['InStaff']['entry_date']))
                                {
                                    $employee['Employee']['entry_date'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['entry_date']);
                                }
            
                                if(!empty($instaff['InStaff']['leaving_date']))
                                {
                                    $employee['Employee']['leaving_date'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['leaving_date']);
                                }
            
                                $employee['Employee']['seniority_date'] = NULL;
            
                                if(!empty($instaff['InStaff']['seniority_date']))
                                {
                                    $employee['Employee']['seniority_date'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['seniority_date']);
                                }
            
                                $employee['Employee']['organizational_unit_id'] = NULL;
                                $employee['Employee']['organizational_unit_code'] = NULL;
            
                                if(!empty($instaff['InStaff']['organizational_unit_id']))
                                {
                                    $organisationunit = $this->Organisation->find('first',
                                                                                        array(
                                                                                            'conditions' => array(
                                                                                                                'Organisation.code' => $instaff['InStaff']['organizational_unit_id'],
                                                                                                                'Organisation.is_active' => 1,
                                                                                                                'Organisation.is_deleted' => 99,
                                                                                                                'BatchOrganisation.is_active' => 1
                                                                                                            ),
                                                                                        ));
                                    if(!empty($organisationunit))
                                    {
                                        $employee['Employee']['organizational_unit_id'] = $organisationunit['Organisation']['id'];
                                        $employee['Employee']['organizational_unit_code'] = $organisationunit['Organisation']['code'];
                                    }
                                }
            
                                $employee['Employee']['cost_center_id'] = NULL;
                                $employee['Employee']['cost_center_code'] = NULL;
            
                                if(!empty($instaff['InStaff']['cost_center_code']))
                                {
                                    $costcenter = $this->CostCenter->findByCode($instaff['InStaff']['cost_center_code']);
            
                                    if(!empty($costcenter))
                                    {
                                        $employee['Employee']['cost_center_id'] = $costcenter['CostCenter']['id'];
                                        $employee['Employee']['cost_center_code'] = $costcenter['CostCenter']['code'];
                                    }
                                }
            
                                $employee['Employee']['position_id'] = NULL;
                                $employee['Employee']['position_code'] = NULL;
            
                                if(!empty($instaff['InStaff']['position_id']))
                                {
                                    $position = $this->Position->findByCode($instaff['InStaff']['position_id']);
            
                                    if(!empty($position))
                                    {
                                        $employee['Employee']['position_id'] = $position['Position']['id'];
                                        $employee['Employee']['position_code'] = $position['Position']['code'];
                                    }
                                }
            
                                if(!empty($instaff['InStaff']['grp_id']))
                                {
                                    $employee['Employee']['group_code'] = $instaff['InStaff']['grp_id'];
                                }
            
                                if(!empty($instaff['InStaff']['div_id']))
                                {
                                    $employee['Employee']['division_code'] = $instaff['InStaff']['div_id'];
                                }
            
                                if(!empty($instaff['InStaff']['dept_id']))
                                {
                                    $employee['Employee']['department_code'] = $instaff['InStaff']['dept_id'];
                                }
            
                                if(!empty($instaff['InStaff']['sec_id']))
                                {
                                    $employee['Employee']['section_code'] = $instaff['InStaff']['sec_id'];
                                }
            
                                if(!empty($instaff['InStaff']['unit_id']))
                                {
                                    $employee['Employee']['unit_code'] = $instaff['InStaff']['unit_id'];
                                }
                                
                                if(!empty($instaff['InStaff']['subunit_id']))
                                {
                                    $employee['Employee']['subunit_code'] = $instaff['InStaff']['subunit_id'];
                                }
            
                                $employee['Employee']['job_grade_id'] = NULL;
            
                                if(!empty($instaff['InStaff']['payscale_group']))
                                {
                                    $jobgrade = $this->JobGrade->findByName($instaff['InStaff']['payscale_group']);
            
                                    if(!empty($jobgrade))
                                    {
                                        $employee['Employee']['job_grade_id'] = $jobgrade['JobGrade']['id'];
                                        $employee['Employee']['job_designation_id'] = $jobgrade['JobGrade']['job_designation_id'];
                                    }
                                }
            
                                $employee['Employee']['payscale_level'] = $instaff['InStaff']['payscale_level'];
                                $employee['Employee']['executed'] = date('Y-m-d H:i:s');
                                

                                // set and validate the employee information 
                                $this->Employee->set($employee);
                                if($this->Employee->validates())
                                {
                                    // insert staff if success insert
                                    $this->Employee->create();
                                    $this->Employee->save($employee);
            
                                    $employee = $this->Employee->read(null,$this->Employee->id);
            
                                    // update instaff if success insert
                                    //enabled user login
                                    $user_id = $current['Personal']['user_id'];

                                    $update = array();
                                    $update['User']['id'] = $user_id;
                                    $update['User']['is_active'] = 1;
                                    $update['User']['modified_by'] = $session['Employee']['id'];
                                    $this->User->create();
                                    $this->User->save($update);

                                    // update instaff if success insert
                                    $instaff['InStaff']['is_flag'] = 1;
                                    $instaff['InStaff']['is_error'] = 99;
                                    $instaff['InStaff']['note'] = 'Enabled employee';
            
                                    $this->InStaff->create();
                                    $this->InStaff->save($instaff);
            
                                    $notification = array();
            
                                    $subject = "Welcome to PRESS ".$employee['Personal']['complete_name'];
                                    $body = "<p style='font-size:12px; font-family:arial;'>There are changes on your employee information. Could I kindly ask you to check and verify.</p>";
                                    $body = "<br/>";
                                    $body .= "Click <a href='".Router::url('/Users/profile' , true)."'>here</a> to view your profile.";
                                    $body = "<br/>";
                                    $body = "<p style='font-size:12px; font-family:arial;'>let ask know if there is any mistake.</p>";
            
                                    $notification = array();
            
                                    //$notification['Notification']['sender_id'] = $staff['Employee']['id'];
                                    $notification['Notification']['recipient_id'] = $employee['Employee']['id'];
                                    $notification['Notification']['is_read'] = 99;
                                    $notification['Notification']['subject'] = $subject;
                                    $notification['Notification']['body'] = $body;
                                    // $notification['Notification']['created_by'] = $staff['Employee']['id'];
                                    // $notification['Notification']['modified_by'] = $staff['Employee']['id'];
            
                                    $this->Notification->create();
                                    $this->Notification->save($notification);
                                }
                                else
                                {
                                    //catch error validation if the information complete
                                    $instaff['InStaff']['is_flag'] = 1;
                                    $instaff['InStaff']['is_error'] = 1;
                                    $instaff['InStaff']['note'] = json_encode($this->Employee->validationErrors);
                                    $instaff['InStaff']['executed_by'] = $session['Employee']['id'];
                                    $instaff['InStaff']['executed'] = date('Y-m-d H:i:s');
        
                                    $this->InStaff->create();
                                    $this->InStaff->save($instaff);
                                }
                            }
                        }
                        else
                        {
                            //check if data from SAP say employee status is not active 
                            // update instaff if success insert
                            $instaff['InStaff']['is_flag'] = 1;
                            $instaff['InStaff']['is_error'] = 99;
                            $instaff['InStaff']['note'] = 'Inactive in SAP and also in DB';
    
                            $this->InStaff->create();
                            $this->InStaff->save($instaff);
                        }
                    }
                }
            }
            else
            {
                if($instaff['InStaff']['employment_status'] == 'Active')
                {
                    $employee = array();

                    $employee['Employee']['id'] = NULL;
                    $employee['Employee']['batch_staff_id'] = $detail['BatchStaff']['id'];
                    $employee['Employee']['employee_no'] = $instaff['InStaff']['employee_no'];
                    $employee['Employee']['is_type'] = 1; // 1 for data from SAP, 99 for data from backend
                    $employee['Employee']['is_flag'] = 99; // triger for SAP, if 99 no need to send to SAP
                    $employee['Employee']['is_active'] = 1;
                    $employee['Employee']['status_id'] = 10;

                    $employee['Employee']['personal_area_id'] = NULL;

                    $personalarea = $this->PersonalArea->findByName($instaff['InStaff']['personal_area']);
                    if(!empty($personalarea))
                    {
                        $employee['Employee']['personal_area_id'] = $personalarea['PersonalArea']['id'];
                    }
                    
                    $employee['Employee']['personal_subarea_id'] = NULL;

                    $personalsubarea = $this->PersonalSubarea->find('first',
                                                                            array(
                                                                                'conditions' => array(
                                                                                                    'PersonalSubarea.area' => $personalarea['PersonalArea']['area'],
                                                                                                    'PersonalSubarea.name' => $instaff['InStaff']['personal_subarea'],
                                                                                                ),
                                                                            ));

                    if(!empty($personalsubarea))
                    {
                        $employee['Employee']['personal_subarea_id'] = $personalsubarea['PersonalSubarea']['id'];
                    }
                    
                    $employee['Employee']['employee_group_id'] = NULL;

                    $employeegroup = $this->EmployeeGroup->findByName($instaff['InStaff']['employee_group']);
                    if(!empty($employeegroup))
                    {
                        $employee['Employee']['employee_group_id'] = $employeegroup['EmployeeGroup']['id'];
                    }

                    $employee['Employee']['employee_subgroup_id'] = NULL;

                    $employeesubgroup = $this->EmployeeSubgroup->find('first',
                                                                            array(
                                                                                'conditions' => array(
                                                                                                    'EmployeeSubgroup.EEGrp' => $employeegroup['EmployeeGroup']['code'],
                                                                                                    'EmployeeSubgroup.name' => $instaff['InStaff']['employee_subgroup'],
                                                                                                ),
                                                                            ));
                                                                            
                    if(!empty($employeesubgroup))
                    {
                        $employee['Employee']['employee_subgroup_id'] = $employeesubgroup['EmployeeSubgroup']['id'];
                    }

                    $employee['Employee']['payroll_area_id'] = NULL;

                    $payrollarea = $this->PayrollArea->findByName($instaff['InStaff']['payroll_area']);
                    if(!empty($payrollarea))
                    {
                        $employee['Employee']['payroll_area_id'] = $payrollarea['PayrollArea']['id'];
                    }

                    $employee['Employee']['employment_status_id'] = NULL;

                    $staffstatus = $this->StaffStatus->findByName($instaff['InStaff']['employment_status']);
                    if(!empty($staffstatus))
                    {
                        $employee['Employee']['employment_status_id'] = $staffstatus['StaffStatus']['id'];
                    }

                    $employee['Employee']['entry_date'] = NULL;

                    if(!empty($instaff['InStaff']['entry_date']))
                    {
                        $employee['Employee']['entry_date'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['entry_date']);
                    }

                    if(!empty($instaff['InStaff']['leaving_date']))
                    {
                        $employee['Employee']['leaving_date'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['leaving_date']);
                    }

                    $employee['Employee']['seniority_date'] = NULL;

                    if(!empty($instaff['InStaff']['seniority_date']))
                    {
                        $employee['Employee']['seniority_date'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['seniority_date']);
                    }

                    $employee['Employee']['organizational_unit_id'] = NULL;
                    $employee['Employee']['organizational_unit_code'] = NULL;

                    if(!empty($instaff['InStaff']['organizational_unit_id']))
                    {
                        $organisationunit = $this->Organisation->find('first',
                                                                            array(
                                                                                'conditions' => array(
                                                                                                    'Organisation.code' => $instaff['InStaff']['organizational_unit_id'],
                                                                                                    'Organisation.is_active' => 1,
                                                                                                    'Organisation.is_deleted' => 99,
                                                                                                    'BatchOrganisation.is_active' => 1
                                                                                                ),
                                                                            ));
                        if(!empty($organisationunit))
                        {
                            $employee['Employee']['organizational_unit_id'] = $organisationunit['Organisation']['id'];
                            $employee['Employee']['organizational_unit_code'] = $organisationunit['Organisation']['code'];
                        }
                    }

                    $employee['Employee']['cost_center_id'] = NULL;
                    $employee['Employee']['cost_center_code'] = NULL;

                    if(!empty($instaff['InStaff']['cost_center_code']))
                    {
                        $costcenter = $this->CostCenter->findByCode($instaff['InStaff']['cost_center_code']);

                        if(!empty($costcenter))
                        {
                            $employee['Employee']['cost_center_id'] = $costcenter['CostCenter']['id'];
                            $employee['Employee']['cost_center_code'] = $costcenter['CostCenter']['code'];
                        }
                    }

                    $employee['Employee']['position_id'] = NULL;
                    $employee['Employee']['position_code'] = NULL;

                    if(!empty($instaff['InStaff']['position_id']))
                    {
                        $position = $this->Position->findByCode($instaff['InStaff']['position_id']);

                        if(!empty($position))
                        {
                            $employee['Employee']['position_id'] = $position['Position']['id'];
                            $employee['Employee']['position_code'] = $position['Position']['code'];
                        }
                    }

                    if(!empty($instaff['InStaff']['grp_id']))
                    {
                        $employee['Employee']['group_code'] = $instaff['InStaff']['grp_id'];
                    }

                    if(!empty($instaff['InStaff']['div_id']))
                    {
                        $employee['Employee']['division_code'] = $instaff['InStaff']['div_id'];
                    }

                    if(!empty($instaff['InStaff']['dept_id']))
                    {
                        $employee['Employee']['department_code'] = $instaff['InStaff']['dept_id'];
                    }

                    if(!empty($instaff['InStaff']['sec_id']))
                    {
                        $employee['Employee']['section_code'] = $instaff['InStaff']['sec_id'];
                    }

                    if(!empty($instaff['InStaff']['unit_id']))
                    {
                        $employee['Employee']['unit_code'] = $instaff['InStaff']['unit_id'];
                    }
                    
                    if(!empty($instaff['InStaff']['subunit_id']))
                    {
                        $employee['Employee']['subunit_code'] = $instaff['InStaff']['subunit_id'];
                    }

                    $employee['Employee']['job_grade_id'] = NULL;

                    if(!empty($instaff['InStaff']['payscale_group']))
                    {
                        $jobgrade = $this->JobGrade->findByName($instaff['InStaff']['payscale_group']);

                        if(!empty($jobgrade))
                        {
                            $employee['Employee']['job_grade_id'] = $jobgrade['JobGrade']['id'];
                            $employee['Employee']['job_designation_id'] = $jobgrade['JobGrade']['job_designation_id'];
                        }
                    }

                    $employee['Employee']['payscale_level'] = $instaff['InStaff']['payscale_level'];

                    $this->Employee->set($employee);
                    if($this->Employee->validates())
                    {
                        $password = 'pass@word1';

                        $user = array();

                        $user['User']['username'] = $instaff['InStaff']['employee_no'];
                        $user['User']['password'] = $password;
                        $user['User']['status_id'] = 10;
                        $user['User']['is_active'] = 1;
                        $user['User']['modified_by'] = $session['Employee']['id'];
                        $user['User']['created_by'] = $session['Employee']['id'];

                        //insert user if success validate
                        $this->User->create();
                        $this->User->save($user);

                        $user = $this->User->read(null,$this->User->id);

                        $userrole = array();

                        $userrole['UserRole']['user_id'] = $user['User']['id'];
                        $userrole['UserRole']['role_id'] = 2;

                        // insert user role if success insert
                        $this->UserRole->create();
                        $this->UserRole->save($userrole);

                        $personal = array();
                        $personal['Personal']['complete_name'] = $instaff['InStaff']['complete_name'];
                        $personal['Personal']['ic_no'] = $instaff['InStaff']['ic_no'];
                        $personal['Personal']['batch_staff_id'] = $detail['BatchStaff']['id'];
                        $personal['Personal']['is_type'] = 1;
                        $personal['Personal']['is_flag'] = 1;
                        $personal['Personal']['is_active'] = 1;
                        $personal['Personal']['status_id'] = 10;
                        $personal['Personal']['first_time'] = 1;
                        $personal['Personal']['user_id'] = $user['User']['id'];

                        $personal['Personal']['date_of_birth'] = NULL;
                        if(!empty($instaff['InStaff']['date_of_birth']))
                        {
                            $personal['Personal']['date_of_birth'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['date_of_birth']);
                        }

                        $personal['Personal']['gender_id'] = NULL;
                        if(!empty($instaff['InStaff']['gender']))
                        {
                            $gender = $this->Gender->findByName($instaff['InStaff']['gender']);

                            if(!empty($gender))
                            {
                                $personal['Personal']['gender_id'] = $gender['Gender']['id'];
                            }
                        }

                        $personal['Personal']['ethnic_id'] = NULL;

                        if(!empty($instaff['InStaff']['ethnic']))
                        {
                            $ethnic = $this->Ethnic->findByName($instaff['InStaff']['ethnic']);

                            if(!empty($ethnic))
                            {
                                $personal['Personal']['ethnic_id'] = $ethnic['Ethnic']['id'];
                            }
                        }

                        // insert personal if success insert
                        $this->Personal->create();
                        $this->Personal->save($personal);

                        $personal = $this->Personal->read(null,$this->Personal->id);

                        // insert staff if success insert
                        $employee['Employee']['personal_id'] = $personal['Personal']['id'];

                        $this->Employee->create();
                        $this->Employee->save($employee);

                        $personal_marital = array();
                        $personal_marital['PersonalMarital']['batch_staff_id'] = $detail['BatchStaff']['id'];
                        $personal_marital['PersonalMarital']['personal_id'] = $personal['Personal']['id'];

                        $marital = array();
                        $marital = $this->Marital->findByCode($instaff['InStaff']['marital_status_code']);
                        
                        $personal_marital['PersonalMarital']['marital_id'] = 1;
    
                        if(!empty($marital))
                        {
                            $personal_marital['PersonalMarital']['marital_id'] = $marital['Marital']['id'];
                        }

                        $personal_marital['PersonalMarital']['is_checked'] = 1;
                        $personal_marital['PersonalMarital']['is_type'] = 1;
                        $personal_marital['PersonalMarital']['is_flag'] = 1;
                        $personal_marital['PersonalMarital']['is_active'] = 1;
                        $personal_marital['PersonalMarital']['status_id'] = 10;

                        // insert personal if success insert
                        $this->PersonalMarital->create();
                        $this->PersonalMarital->save($personal_marital);

                        $employee = $this->Employee->read(null,$this->Employee->id);

                        // update instaff if success insert
                        $instaff['InStaff']['is_flag'] = 1;
                        $instaff['InStaff']['is_error'] = 99;
                        $instaff['InStaff']['note'] = 'New employee';

                        $this->InStaff->create();
                        $this->InStaff->save($instaff);

                        $notification = array();

                        $subject = "Welcome to PRESS ".$employee['Personal']['complete_name'];
                        //$body = "<p style='font-size:12px; font-family:arial;'>Here we put some text to read. So assign someone to do this.</p>";
                        $body = "<br/>";
                        $body .= "Click <a href='".Router::url('/Users/profile' , true)."'>here</a> to view your profile.";

                        $notification = array();

                        //$notification['Notification']['sender_id'] = $staff['Employee']['id'];
                        $notification['Notification']['recipient_id'] = $employee['Employee']['id'];
                        $notification['Notification']['is_read'] = 99;
                        $notification['Notification']['subject'] = $subject;
                        $notification['Notification']['body'] = $body;
                        // $notification['Notification']['created_by'] = $staff['Employee']['id'];
                        // $notification['Notification']['modified_by'] = $staff['Employee']['id'];

                        $this->Notification->create();
                        $this->Notification->save($notification);
                    }
                    else
                    {
                        $instaff['InStaff']['is_flag'] = 1;
                        $instaff['InStaff']['is_error'] = 1;
                        $instaff['InStaff']['note'] = json_encode($this->Employee->validationErrors);
                        $instaff['InStaff']['executed_by'] = $session['Employee']['id'];
                        $instaff['InStaff']['executed'] = date('Y-m-d H:i:s');

                        $this->InStaff->create();
                        $this->InStaff->save($instaff);
                    }
                }
                else
                {
                    $instaff['InStaff']['is_flag'] = 1;
                    $instaff['InStaff']['is_error'] = 1;
                    $instaff['InStaff']['note'] = 'Employee not active but dont have record in database';
                    $instaff['InStaff']['executed_by'] = $session['Employee']['id'];
                    $instaff['InStaff']['executed'] = date('Y-m-d H:i:s');

                    $this->InStaff->create();
                    $this->InStaff->save($instaff);
                }
            }
        }

        $update = array();
        $update['BatchStaff']['id'] = $detail['BatchStaff']['id'];
        $update['BatchStaff']['executed_end'] = date('Y-m-d H:i:s');

        $this->BatchStaff->create();
        $this->BatchStaff->save($update);

        $success = $this->InStaff->find('count', array(
                                                    'conditions' => array(
                                                        array(
                                                                'InStaff.batch_staff_id' => $detail['BatchStaff']['id'],
                                                                'InStaff.is_flag' => 1,
                                                                'InStaff.is_error' => 99,
                                                            ),
                                                    )
                                                ));

        $fail = $this->InStaff->find('count', array(
                                                'conditions' => array(
                                                    array(
                                                            'InStaff.batch_staff_id' => $detail['BatchStaff']['id'],
                                                            'InStaff.is_flag' => 1,
                                                            'InStaff.is_error' => 1,
                                                        ),
                                                )
                                            ));

        $is_in = 99;

        $check = $this->InStaff->find('count', array(
                                                'conditions' => array(
                                                    array(
                                                            'InStaff.batch_staff_id' => $detail['BatchStaff']['id'],
                                                            'InStaff.is_flag' => 99,
                                                        ),
                                                )
                                            ));

        if($check == 0)
        {
            $is_in = 1;
        }


        $edit = array();

        $edit['BatchStaff']['id'] = $detail['BatchStaff']['id'];
        $edit['BatchStaff']['success'] = $success;
        $edit['BatchStaff']['fail'] = $fail;
        $edit['BatchStaff']['is_in'] = $is_in;
        $edit['BatchStaff']['execute'] = 1;
        $edit['BatchStaff']['is_flag'] = 1;
        $edit['BatchStaff']['note'] = "Executed by cronjob";

        $this->BatchStaff->create();
        $this->BatchStaff->save($edit);

        $this->Session->setFlash('Staff information successfully sync', 'success');
        $this->redirect(array('action' => 'error/key:'.$key));

        $this->autoRender = false;
      
    }

    public function pushAll($key = null)
    {
        $this->loadModel('Employee');
        $this->loadModel('InStaff');
        $this->loadModel('PersonalArea');
        $this->loadModel('PersonalSubarea');
        $this->loadModel('EmployeeGroup');
        $this->loadModel('EmployeeSubgroup');
        $this->loadModel('PayrollArea');
        $this->loadModel('StaffStatus');
        $this->loadModel('Organisation');
        $this->loadModel('CostCenter');
        $this->loadModel('Position');
        $this->loadModel('Gender');
        $this->loadModel('Ethnic');
        $this->loadModel('Marital');
        $this->loadModel('JobGrade');
        $this->loadModel('Notification');
        $this->loadModel('User');
        $this->loadModel('UserRole');
        $this->loadModel('Employee');
        $this->loadModel('Personal');
        $this->loadModel('PersonalMarital');
        $this->loadModel('Log');
        $this->loadModel('Utility');
        
        $person = $this->Auth->user();
        $session = $this->Utility->getUserInformation($person['id']);

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        $id = $this->Utility->decrypt($key, 'BatchStaff');

        $detail = $this->BatchStaff->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        $update = array();
        $update['BatchStaff']['id'] = $detail['BatchStaff']['id'];
        $update['BatchStaff']['executed_by'] = $session['Employee']['id'];
        $update['BatchStaff']['executed_start'] = date('Y-m-d H:i:s');

        $this->BatchStaff->create();
        $this->BatchStaff->save($update);

        $instaffs = $this->InStaff->find('all',
                                            array(
                                                'conditions' => array(
                                                                    'InStaff.batch_staff_id' => $detail['BatchStaff']['id'],
                                                                    'InStaff.is_error' => 1,
                                                                ),
                                            ));
        
        if(empty($instaffs))
        {
            $this->Session->setFlash('No staff information avaiable.', 'info');
            $this->redirect(array('action' => '/list_sync/'.$key));
        }

        foreach ($instaffs as $instaff) 
        {
            $check = $this->Employee->find('count',
                                            array(
                                                'conditions' => array(
                                                                    'Employee.employee_no' => $instaff['InStaff']['employee_no'],
                                                                ),
                                            ));

            if($check > 0)
            {
                $current = $this->Employee->find('first',
                                                        array(
                                                            'conditions' => array(
                                                                                'Employee.employee_no' => $instaff['InStaff']['employee_no'],
                                                                            ),
                                                            'order' => array('Employee.id' => 'DESC'),
                                                        ));

                if(!empty($current))
                {
                    //if employee exist and still active
                    if($current['Employee']['employment_status_id'] == 1)
                    {
                        //check if data from SAP say employee status is active
                        if($instaff['InStaff']['employment_status'] == 'Active')
                        {
                            //check the jobgrade
                            $jobgrade = $this->JobGrade->findByName($instaff['InStaff']['payscale_group']);

                            /*
                                if employee :
                                1.  difference organisation unit from SAP than in DB
                                    OR
                                2.  difference job designation unit from SAP than in DB
                                    OR
                                3. difference job grade from SAP than in DB
                            */
                            if(($instaff['InStaff']['organizational_unit_id'] != $current['Organisation']['code']) || ($jobgrade['JobGrade']['job_designation_id'] != $current['JobDesignation']['id']) || ($jobgrade['JobGrade']['id'] != $current['JobGrade']['id']))
                            {
                                //Disabled employee 
                                $update = array();
                                $update['Employee']['id'] = $current['Employee']['id'];
                                $update['Employee']['is_active'] = 99;
                                $update['Employee']['modified_by'] = $session['Employee']['id'];
        
                                $this->Employee->create();
                                $this->Employee->save($update);

                                //set all employee information
                                $employee = array();

                                $employee['Employee']['id'] = NULL;
                                $employee['Employee']['batch_staff_id'] = $detail['BatchStaff']['id'];
                                $employee['Employee']['employee_no'] = $instaff['InStaff']['employee_no'];
                                $employee['Employee']['complete_name'] = $instaff['InStaff']['complete_name'];
                                $employee['Employee']['is_type'] = 1; // 1 for data from SAP, 99 for data from backend
                                $employee['Employee']['is_flag'] = 1; // triger for SAP, if 99 no need to send to SAP
                                $employee['Employee']['is_active'] = 1;
                                $employee['Employee']['status_id'] = 10;
                                $employee['Employee']['parent_id'] = $current['Employee']['id'];
                                $employee['Employee']['personal_id'] = $current['Employee']['personal_id'];
            
                                $employee['Employee']['personal_area_id'] = NULL;
            
                                $personalarea = $this->PersonalArea->findByName($instaff['InStaff']['personal_area']);
                                if(!empty($personalarea))
                                {
                                    $employee['Employee']['personal_area_id'] = $personalarea['PersonalArea']['id'];
                                }
                                
                                $employee['Employee']['personal_subarea_id'] = NULL;
            
                                $personalsubarea = $this->PersonalSubarea->find('first',
                                                                                        array(
                                                                                            'conditions' => array(
                                                                                                                'PersonalSubarea.area' => $personalarea['PersonalArea']['area'],
                                                                                                                'PersonalSubarea.name' => $instaff['InStaff']['personal_subarea'],
                                                                                                            ),
                                                                                        ));
            
                                if(!empty($personalsubarea))
                                {
                                    $employee['Employee']['personal_subarea_id'] = $personalsubarea['PersonalSubarea']['id'];
                                }
                                
                                $employee['Employee']['employee_group_id'] = NULL;
            
                                $employeegroup = $this->EmployeeGroup->findByName($instaff['InStaff']['employee_group']);
                                if(!empty($employeegroup))
                                {
                                    $employee['Employee']['employee_group_id'] = $employeegroup['EmployeeGroup']['id'];
                                }
            
                                $employee['Employee']['employee_subgroup_id'] = NULL;
            
                                $employeesubgroup = $this->EmployeeSubgroup->find('first',
                                                                                        array(
                                                                                            'conditions' => array(
                                                                                                                'EmployeeSubgroup.EEGrp' => $employeegroup['EmployeeGroup']['code'],
                                                                                                                'EmployeeSubgroup.name' => $instaff['InStaff']['employee_subgroup'],
                                                                                                            ),
                                                                                        ));
                                                                                        
                                if(!empty($employeesubgroup))
                                {
                                    $employee['Employee']['employee_subgroup_id'] = $employeesubgroup['EmployeeSubgroup']['id'];
                                }
            
                                $employee['Employee']['payroll_area_id'] = NULL;
            
                                $payrollarea = $this->PayrollArea->findByName($instaff['InStaff']['payroll_area']);
                                if(!empty($payrollarea))
                                {
                                    $employee['Employee']['payroll_area_id'] = $payrollarea['PayrollArea']['id'];
                                }
            
                                $employee['Employee']['employment_status_id'] = NULL;
            
                                $staffstatus = $this->StaffStatus->findByName($instaff['InStaff']['employment_status']);
                                if(!empty($staffstatus))
                                {
                                    $employee['Employee']['employment_status_id'] = $staffstatus['StaffStatus']['id'];
                                }
            
                                $employee['Employee']['entry_date'] = NULL;
            
                                if(!empty($instaff['InStaff']['entry_date']))
                                {
                                    $employee['Employee']['entry_date'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['entry_date']);
                                }
            
                                if(!empty($instaff['InStaff']['leaving_date']))
                                {
                                    $employee['Employee']['leaving_date'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['leaving_date']);
                                }
            
                                $employee['Employee']['seniority_date'] = NULL;
            
                                if(!empty($instaff['InStaff']['seniority_date']))
                                {
                                    $employee['Employee']['seniority_date'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['seniority_date']);
                                }
            
                                $employee['Employee']['organizational_unit_id'] = NULL;
                                $employee['Employee']['organizational_unit_code'] = NULL;
            
                                if(!empty($instaff['InStaff']['organizational_unit_id']))
                                {
                                    $organisationunit = $this->Organisation->find('first',
                                                                                        array(
                                                                                            'conditions' => array(
                                                                                                                'Organisation.code' => $instaff['InStaff']['organizational_unit_id'],
                                                                                                                'Organisation.is_active' => 1,
                                                                                                                'Organisation.is_deleted' => 99,
                                                                                                                'BatchOrganisation.is_active' => 1
                                                                                                            ),
                                                                                        ));
                                    if(!empty($organisationunit))
                                    {
                                        $employee['Employee']['organizational_unit_id'] = $organisationunit['Organisation']['id'];
                                        $employee['Employee']['organizational_unit_code'] = $organisationunit['Organisation']['code'];
                                    }
                                }
            
                                $employee['Employee']['cost_center_id'] = NULL;
                                $employee['Employee']['cost_center_code'] = NULL;
            
                                if(!empty($instaff['InStaff']['cost_center_code']))
                                {
                                    $costcenter = $this->CostCenter->findByCode($instaff['InStaff']['cost_center_code']);
            
                                    if(!empty($costcenter))
                                    {
                                        $employee['Employee']['cost_center_id'] = $costcenter['CostCenter']['id'];
                                        $employee['Employee']['cost_center_code'] = $costcenter['CostCenter']['code'];
                                    }
                                }
            
                                $employee['Employee']['position_id'] = NULL;
                                $employee['Employee']['position_code'] = NULL;
            
                                if(!empty($instaff['InStaff']['position_id']))
                                {
                                    $position = $this->Position->findByCode($instaff['InStaff']['position_id']);
            
                                    if(!empty($position))
                                    {
                                        $employee['Employee']['position_id'] = $position['Position']['id'];
                                        $employee['Employee']['position_code'] = $position['Position']['code'];
                                    }
                                }
            
                                if(!empty($instaff['InStaff']['grp_id']))
                                {
                                    $employee['Employee']['group_code'] = $instaff['InStaff']['grp_id'];
                                }
            
                                if(!empty($instaff['InStaff']['div_id']))
                                {
                                    $employee['Employee']['division_code'] = $instaff['InStaff']['div_id'];
                                }
            
                                if(!empty($instaff['InStaff']['dept_id']))
                                {
                                    $employee['Employee']['department_code'] = $instaff['InStaff']['dept_id'];
                                }
            
                                if(!empty($instaff['InStaff']['sec_id']))
                                {
                                    $employee['Employee']['section_code'] = $instaff['InStaff']['sec_id'];
                                }
            
                                if(!empty($instaff['InStaff']['unit_id']))
                                {
                                    $employee['Employee']['unit_code'] = $instaff['InStaff']['unit_id'];
                                }
                                
                                if(!empty($instaff['InStaff']['subunit_id']))
                                {
                                    $employee['Employee']['subunit_code'] = $instaff['InStaff']['subunit_id'];
                                }
            
                                $employee['Employee']['job_grade_id'] = NULL;
            
                                if(!empty($instaff['InStaff']['payscale_group']))
                                {
                                    $jobgrade = $this->JobGrade->findByName($instaff['InStaff']['payscale_group']);
            
                                    if(!empty($jobgrade))
                                    {
                                        $employee['Employee']['job_grade_id'] = $jobgrade['JobGrade']['id'];
                                        $employee['Employee']['job_designation_id'] = $jobgrade['JobGrade']['job_designation_id'];
                                    }
                                }
            
                                $employee['Employee']['payscale_level'] = $instaff['InStaff']['payscale_level'];
                                $employee['Employee']['executed'] = date('Y-m-d H:i:s');
                                

                                // set and validate the employee information 
                                $this->Employee->set($employee);
                                if($this->Employee->validates())
                                {
                                    // insert staff if success insert
                                    $this->Employee->create();
                                    $this->Employee->save($employee);
            
                                    $employee = $this->Employee->read(null,$this->Employee->id);
            
                                    // update instaff if success insert
                                    $instaff['InStaff']['is_flag'] = 1;
                                    $instaff['InStaff']['is_error'] = 99;
                                    $instaff['InStaff']['note'] = 'Have changes';
            
                                    $this->InStaff->create();
                                    $this->InStaff->save($instaff);
            
                                    $notification = array();
            
                                    $subject = "Welcome to PRESS ".$employee['Personal']['complete_name'];
                                    $body = "<p style='font-size:12px; font-family:arial;'>There are changes on your employee information. Could I kindly ask you to check and verify.</p>";
                                    $body = "<br/>";
                                    $body .= "Click <a href='".Router::url('/Users/profile' , true)."'>here</a> to view your profile.";
                                    $body = "<br/>";
                                    $body = "<p style='font-size:12px; font-family:arial;'>let ask know if there is any mistake.</p>";
            
                                    $notification = array();
            
                                    //$notification['Notification']['sender_id'] = $staff['Employee']['id'];
                                    $notification['Notification']['recipient_id'] = $employee['Employee']['id'];
                                    $notification['Notification']['is_read'] = 99;
                                    $notification['Notification']['subject'] = $subject;
                                    $notification['Notification']['body'] = $body;
                                    // $notification['Notification']['created_by'] = $staff['Employee']['id'];
                                    // $notification['Notification']['modified_by'] = $staff['Employee']['id'];
            
                                    $this->Notification->create();
                                    $this->Notification->save($notification);
                                }
                                else
                                {
                                    //catch error validation if the information complete
                                    $instaff['InStaff']['is_error'] = 1;
                                    $instaff['InStaff']['note'] = json_encode($this->Employee->validationErrors);
                                    $instaff['InStaff']['executed_by'] = $session['Employee']['id'];
                                    $instaff['InStaff']['executed'] = date('Y-m-d H:i:s');
        
                                    $this->InStaff->create();
                                    $this->InStaff->save($instaff);
                                }
                            }
                            else
                            {
                                // if not, just bypass the information and flag as check
                                $instaff['InStaff']['is_flag'] = 1;
                                $instaff['InStaff']['is_error'] = 99;
                                $instaff['InStaff']['note'] = 'Nothing change';
                                $instaff['InStaff']['executed_by'] = $session['Employee']['id'];
                                $instaff['InStaff']['executed'] = date('Y-m-d H:i:s');

                                $this->InStaff->create();
                                $this->InStaff->save($instaff);
                            }
                        }
                        else
                        {
                            //check if data from SAP say employee status is not active

                            //update employee status
                            $update = array();
                            $update['Employee']['id'] = $current['Employee']['id'];
                            $update['Employee']['employment_status_id'] = 3;
                            switch ($instaff['InStaff']['employment_status']) 
                            {
                                case 'Inactive':
                                    $update['Employee']['employment_status_id'] = 2;
                                    break;
                                case 'Withdrawn':
                                    $update['Employee']['employment_status_id'] = 3;
                                    break;
                            }
                            $update['Employee']['modified_by'] = $session['Employee']['id'];
                            $this->Employee->create();
                            $this->Employee->save($update);

                            //disabled user login
                            $user_id = $current['Personal']['user_id'];

                            $update = array();
                            $update['User']['id'] = $user_id;
                            $update['User']['is_active'] = 99;
                            $update['User']['modified_by'] = $session['Employee']['id'];
                            $this->User->create();
                            $this->User->save($update);


                            // update instaff if success insert
                            $instaff['InStaff']['is_flag'] = 1;
                            $instaff['InStaff']['is_error'] = 99;
                            $instaff['InStaff']['note'] = 'Disabled employee';
    
                            $this->InStaff->create();
                            $this->InStaff->save($instaff);
                        }
                    }
                    else
                    {// if employee exist but not active
                        
                        //check if data from SAP say employee status is active
                        if($instaff['InStaff']['employment_status'] == 'Active')
                        {
                            //check the jobgrade
                            $jobgrade = $this->JobGrade->findByName($instaff['InStaff']['payscale_group']);

                            /*
                                if employee :
                                1.  difference organisation unit from SAP than in DB
                                    OR
                                2.  difference job designation unit from SAP than in DB
                                    OR
                                3. difference job grade from SAP than in DB
                            */
                            if(($instaff['InStaff']['organizational_unit_id'] != $current['Organisation']['code']) || ($jobgrade['JobGrade']['job_designation_id'] != $current['JobDesignation']['id']) || ($jobgrade['JobGrade']['id'] != $current['JobGrade']['id']))
                            {
                                //Disabled employee 
                                $update = array();
                                $update['Employee']['id'] = $current['Employee']['id'];
                                $update['Employee']['is_active'] = 99;
                                $update['Employee']['modified_by'] = $session['Employee']['id'];
        
                                $this->Employee->create();
                                $this->Employee->save($update);

                                //set all employee information
                                $employee = array();

                                $employee['Employee']['id'] = NULL;
                                $employee['Employee']['batch_staff_id'] = $detail['BatchStaff']['id'];
                                $employee['Employee']['employee_no'] = $instaff['InStaff']['employee_no'];
                                $employee['Employee']['complete_name'] = $instaff['InStaff']['complete_name'];
                                $employee['Employee']['is_type'] = 1; // 1 for data from SAP, 99 for data from backend
                                $employee['Employee']['is_flag'] = 1; // triger for SAP, if 99 no need to send to SAP
                                $employee['Employee']['is_active'] = 1;
                                $employee['Employee']['status_id'] = 10;
                                $employee['Employee']['parent_id'] = $current['Employee']['id'];
                                $employee['Employee']['personal_id'] = $current['Employee']['personal_id'];
            
                                $employee['Employee']['personal_area_id'] = NULL;
            
                                $personalarea = $this->PersonalArea->findByName($instaff['InStaff']['personal_area']);
                                if(!empty($personalarea))
                                {
                                    $employee['Employee']['personal_area_id'] = $personalarea['PersonalArea']['id'];
                                }
                                
                                $employee['Employee']['personal_subarea_id'] = NULL;
            
                                $personalsubarea = $this->PersonalSubarea->find('first',
                                                                                        array(
                                                                                            'conditions' => array(
                                                                                                                'PersonalSubarea.area' => $personalarea['PersonalArea']['area'],
                                                                                                                'PersonalSubarea.name' => $instaff['InStaff']['personal_subarea'],
                                                                                                            ),
                                                                                        ));
            
                                if(!empty($personalsubarea))
                                {
                                    $employee['Employee']['personal_subarea_id'] = $personalsubarea['PersonalSubarea']['id'];
                                }
                                
                                $employee['Employee']['employee_group_id'] = NULL;
            
                                $employeegroup = $this->EmployeeGroup->findByName($instaff['InStaff']['employee_group']);
                                if(!empty($employeegroup))
                                {
                                    $employee['Employee']['employee_group_id'] = $employeegroup['EmployeeGroup']['id'];
                                }
            
                                $employee['Employee']['employee_subgroup_id'] = NULL;
            
                                $employeesubgroup = $this->EmployeeSubgroup->find('first',
                                                                                        array(
                                                                                            'conditions' => array(
                                                                                                                'EmployeeSubgroup.EEGrp' => $employeegroup['EmployeeGroup']['code'],
                                                                                                                'EmployeeSubgroup.name' => $instaff['InStaff']['employee_subgroup'],
                                                                                                            ),
                                                                                        ));
                                                                                        
                                if(!empty($employeesubgroup))
                                {
                                    $employee['Employee']['employee_subgroup_id'] = $employeesubgroup['EmployeeSubgroup']['id'];
                                }
            
                                $employee['Employee']['payroll_area_id'] = NULL;
            
                                $payrollarea = $this->PayrollArea->findByName($instaff['InStaff']['payroll_area']);
                                if(!empty($payrollarea))
                                {
                                    $employee['Employee']['payroll_area_id'] = $payrollarea['PayrollArea']['id'];
                                }
            
                                $employee['Employee']['employment_status_id'] = NULL;
            
                                $staffstatus = $this->StaffStatus->findByName($instaff['InStaff']['employment_status']);
                                if(!empty($staffstatus))
                                {
                                    $employee['Employee']['employment_status_id'] = $staffstatus['StaffStatus']['id'];
                                }
            
                                $employee['Employee']['entry_date'] = NULL;
            
                                if(!empty($instaff['InStaff']['entry_date']))
                                {
                                    $employee['Employee']['entry_date'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['entry_date']);
                                }
            
                                if(!empty($instaff['InStaff']['leaving_date']))
                                {
                                    $employee['Employee']['leaving_date'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['leaving_date']);
                                }
            
                                $employee['Employee']['seniority_date'] = NULL;
            
                                if(!empty($instaff['InStaff']['seniority_date']))
                                {
                                    $employee['Employee']['seniority_date'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['seniority_date']);
                                }
            
                                $employee['Employee']['organizational_unit_id'] = NULL;
                                $employee['Employee']['organizational_unit_code'] = NULL;
            
                                if(!empty($instaff['InStaff']['organizational_unit_id']))
                                {
                                    $organisationunit = $this->Organisation->find('first',
                                                                                        array(
                                                                                            'conditions' => array(
                                                                                                                'Organisation.code' => $instaff['InStaff']['organizational_unit_id'],
                                                                                                                'Organisation.is_active' => 1,
                                                                                                                'Organisation.is_deleted' => 99,
                                                                                                                'BatchOrganisation.is_active' => 1
                                                                                                            ),
                                                                                        ));
                                    if(!empty($organisationunit))
                                    {
                                        $employee['Employee']['organizational_unit_id'] = $organisationunit['Organisation']['id'];
                                        $employee['Employee']['organizational_unit_code'] = $organisationunit['Organisation']['code'];
                                    }
                                }
            
                                $employee['Employee']['cost_center_id'] = NULL;
                                $employee['Employee']['cost_center_code'] = NULL;
            
                                if(!empty($instaff['InStaff']['cost_center_code']))
                                {
                                    $costcenter = $this->CostCenter->findByCode($instaff['InStaff']['cost_center_code']);
            
                                    if(!empty($costcenter))
                                    {
                                        $employee['Employee']['cost_center_id'] = $costcenter['CostCenter']['id'];
                                        $employee['Employee']['cost_center_code'] = $costcenter['CostCenter']['code'];
                                    }
                                }
            
                                $employee['Employee']['position_id'] = NULL;
                                $employee['Employee']['position_code'] = NULL;
            
                                if(!empty($instaff['InStaff']['position_id']))
                                {
                                    $position = $this->Position->findByCode($instaff['InStaff']['position_id']);
            
                                    if(!empty($position))
                                    {
                                        $employee['Employee']['position_id'] = $position['Position']['id'];
                                        $employee['Employee']['position_code'] = $position['Position']['code'];
                                    }
                                }
            
                                if(!empty($instaff['InStaff']['grp_id']))
                                {
                                    $employee['Employee']['group_code'] = $instaff['InStaff']['grp_id'];
                                }
            
                                if(!empty($instaff['InStaff']['div_id']))
                                {
                                    $employee['Employee']['division_code'] = $instaff['InStaff']['div_id'];
                                }
            
                                if(!empty($instaff['InStaff']['dept_id']))
                                {
                                    $employee['Employee']['department_code'] = $instaff['InStaff']['dept_id'];
                                }
            
                                if(!empty($instaff['InStaff']['sec_id']))
                                {
                                    $employee['Employee']['section_code'] = $instaff['InStaff']['sec_id'];
                                }
            
                                if(!empty($instaff['InStaff']['unit_id']))
                                {
                                    $employee['Employee']['unit_code'] = $instaff['InStaff']['unit_id'];
                                }
                                
                                if(!empty($instaff['InStaff']['subunit_id']))
                                {
                                    $employee['Employee']['subunit_code'] = $instaff['InStaff']['subunit_id'];
                                }
            
                                $employee['Employee']['job_grade_id'] = NULL;
            
                                if(!empty($instaff['InStaff']['payscale_group']))
                                {
                                    $jobgrade = $this->JobGrade->findByName($instaff['InStaff']['payscale_group']);
            
                                    if(!empty($jobgrade))
                                    {
                                        $employee['Employee']['job_grade_id'] = $jobgrade['JobGrade']['id'];
                                        $employee['Employee']['job_designation_id'] = $jobgrade['JobGrade']['job_designation_id'];
                                    }
                                }
            
                                $employee['Employee']['payscale_level'] = $instaff['InStaff']['payscale_level'];
                                $employee['Employee']['executed'] = date('Y-m-d H:i:s');
                                

                                // set and validate the employee information 
                                $this->Employee->set($employee);
                                if($this->Employee->validates())
                                {
                                    // insert staff if success insert
                                    $this->Employee->create();
                                    $this->Employee->save($employee);
            
                                    $employee = $this->Employee->read(null,$this->Employee->id);
            
                                    // update instaff if success insert
                                    $instaff['InStaff']['is_flag'] = 1;
                                    $instaff['InStaff']['is_error'] = 99;
                                    $instaff['InStaff']['note'] = 'Have changes';
            
                                    $this->InStaff->create();
                                    $this->InStaff->save($instaff);
            
                                    $notification = array();
            
                                    $subject = "Welcome to PRESS ".$employee['Personal']['complete_name'];
                                    $body = "<p style='font-size:12px; font-family:arial;'>There are changes on your employee information. Could I kindly ask you to check and verify.</p>";
                                    $body = "<br/>";
                                    $body .= "Click <a href='".Router::url('/Users/profile' , true)."'>here</a> to view your profile.";
                                    $body = "<br/>";
                                    $body = "<p style='font-size:12px; font-family:arial;'>let ask know if there is any mistake.</p>";
            
                                    $notification = array();
            
                                    //$notification['Notification']['sender_id'] = $staff['Employee']['id'];
                                    $notification['Notification']['recipient_id'] = $employee['Employee']['id'];
                                    $notification['Notification']['is_read'] = 99;
                                    $notification['Notification']['subject'] = $subject;
                                    $notification['Notification']['body'] = $body;
                                    // $notification['Notification']['created_by'] = $staff['Employee']['id'];
                                    // $notification['Notification']['modified_by'] = $staff['Employee']['id'];
            
                                    $this->Notification->create();
                                    $this->Notification->save($notification);
                                }
                                else
                                {
                                    //catch error validation if the information complete
                                    $instaff['InStaff']['is_error'] = 1;
                                    $instaff['InStaff']['note'] = json_encode($this->Employee->validationErrors);
                                    $instaff['InStaff']['executed_by'] = $session['Employee']['id'];
                                    $instaff['InStaff']['executed'] = date('Y-m-d H:i:s');
        
                                    $this->InStaff->create();
                                    $this->InStaff->save($instaff);
                                }
                            }
                            else
                            {
                                //update employee status
                                //set all employee information
                                $employee = array();
                                $employee['Employee']['id'] = $current['Employee']['id'];
                                $employee['Employee']['batch_staff_id'] = $detail['BatchStaff']['id'];
                                $employee['Employee']['employee_no'] = $instaff['InStaff']['employee_no'];
                                $employee['Employee']['complete_name'] = $instaff['InStaff']['complete_name'];
                                $employee['Employee']['is_type'] = 1; // 1 for data from SAP, 99 for data from backend
                                $employee['Employee']['is_flag'] = 1; // triger for SAP, if 99 no need to send to SAP
                                $employee['Employee']['is_active'] = 1;
                                $employee['Employee']['status_id'] = 10;
            
                                $employee['Employee']['personal_area_id'] = NULL;
            
                                $personalarea = $this->PersonalArea->findByName($instaff['InStaff']['personal_area']);
                                if(!empty($personalarea))
                                {
                                    $employee['Employee']['personal_area_id'] = $personalarea['PersonalArea']['id'];
                                }
                                
                                $employee['Employee']['personal_subarea_id'] = NULL;
            
                                $personalsubarea = $this->PersonalSubarea->find('first',
                                                                                        array(
                                                                                            'conditions' => array(
                                                                                                                'PersonalSubarea.area' => $personalarea['PersonalArea']['area'],
                                                                                                                'PersonalSubarea.name' => $instaff['InStaff']['personal_subarea'],
                                                                                                            ),
                                                                                        ));
            
                                if(!empty($personalsubarea))
                                {
                                    $employee['Employee']['personal_subarea_id'] = $personalsubarea['PersonalSubarea']['id'];
                                }
                                
                                $employee['Employee']['employee_group_id'] = NULL;
            
                                $employeegroup = $this->EmployeeGroup->findByName($instaff['InStaff']['employee_group']);
                                if(!empty($employeegroup))
                                {
                                    $employee['Employee']['employee_group_id'] = $employeegroup['EmployeeGroup']['id'];
                                }
            
                                $employee['Employee']['employee_subgroup_id'] = NULL;
            
                                $employeesubgroup = $this->EmployeeSubgroup->find('first',
                                                                                        array(
                                                                                            'conditions' => array(
                                                                                                                'EmployeeSubgroup.EEGrp' => $employeegroup['EmployeeGroup']['code'],
                                                                                                                'EmployeeSubgroup.name' => $instaff['InStaff']['employee_subgroup'],
                                                                                                            ),
                                                                                        ));
                                                                                        
                                if(!empty($employeesubgroup))
                                {
                                    $employee['Employee']['employee_subgroup_id'] = $employeesubgroup['EmployeeSubgroup']['id'];
                                }
            
                                $employee['Employee']['payroll_area_id'] = NULL;
            
                                $payrollarea = $this->PayrollArea->findByName($instaff['InStaff']['payroll_area']);
                                if(!empty($payrollarea))
                                {
                                    $employee['Employee']['payroll_area_id'] = $payrollarea['PayrollArea']['id'];
                                }
            
                                $employee['Employee']['employment_status_id'] = NULL;
            
                                $staffstatus = $this->StaffStatus->findByName($instaff['InStaff']['employment_status']);
                                if(!empty($staffstatus))
                                {
                                    $employee['Employee']['employment_status_id'] = $staffstatus['StaffStatus']['id'];
                                }
            
                                $employee['Employee']['entry_date'] = NULL;
            
                                if(!empty($instaff['InStaff']['entry_date']))
                                {
                                    $employee['Employee']['entry_date'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['entry_date']);
                                }
            
                                if(!empty($instaff['InStaff']['leaving_date']))
                                {
                                    $employee['Employee']['leaving_date'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['leaving_date']);
                                }
            
                                $employee['Employee']['seniority_date'] = NULL;
            
                                if(!empty($instaff['InStaff']['seniority_date']))
                                {
                                    $employee['Employee']['seniority_date'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['seniority_date']);
                                }
            
                                $employee['Employee']['organizational_unit_id'] = NULL;
                                $employee['Employee']['organizational_unit_code'] = NULL;
            
                                if(!empty($instaff['InStaff']['organizational_unit_id']))
                                {
                                    $organisationunit = $this->Organisation->find('first',
                                                                                        array(
                                                                                            'conditions' => array(
                                                                                                                'Organisation.code' => $instaff['InStaff']['organizational_unit_id'],
                                                                                                                'Organisation.is_active' => 1,
                                                                                                                'Organisation.is_deleted' => 99,
                                                                                                                'BatchOrganisation.is_active' => 1
                                                                                                            ),
                                                                                        ));
                                    if(!empty($organisationunit))
                                    {
                                        $employee['Employee']['organizational_unit_id'] = $organisationunit['Organisation']['id'];
                                        $employee['Employee']['organizational_unit_code'] = $organisationunit['Organisation']['code'];
                                    }
                                }
            
                                $employee['Employee']['cost_center_id'] = NULL;
                                $employee['Employee']['cost_center_code'] = NULL;
            
                                if(!empty($instaff['InStaff']['cost_center_code']))
                                {
                                    $costcenter = $this->CostCenter->findByCode($instaff['InStaff']['cost_center_code']);
            
                                    if(!empty($costcenter))
                                    {
                                        $employee['Employee']['cost_center_id'] = $costcenter['CostCenter']['id'];
                                        $employee['Employee']['cost_center_code'] = $costcenter['CostCenter']['code'];
                                    }
                                }
            
                                $employee['Employee']['position_id'] = NULL;
                                $employee['Employee']['position_code'] = NULL;
            
                                if(!empty($instaff['InStaff']['position_id']))
                                {
                                    $position = $this->Position->findByCode($instaff['InStaff']['position_id']);
            
                                    if(!empty($position))
                                    {
                                        $employee['Employee']['position_id'] = $position['Position']['id'];
                                        $employee['Employee']['position_code'] = $position['Position']['code'];
                                    }
                                }
            
                                if(!empty($instaff['InStaff']['grp_id']))
                                {
                                    $employee['Employee']['group_code'] = $instaff['InStaff']['grp_id'];
                                }
            
                                if(!empty($instaff['InStaff']['div_id']))
                                {
                                    $employee['Employee']['division_code'] = $instaff['InStaff']['div_id'];
                                }
            
                                if(!empty($instaff['InStaff']['dept_id']))
                                {
                                    $employee['Employee']['department_code'] = $instaff['InStaff']['dept_id'];
                                }
            
                                if(!empty($instaff['InStaff']['sec_id']))
                                {
                                    $employee['Employee']['section_code'] = $instaff['InStaff']['sec_id'];
                                }
            
                                if(!empty($instaff['InStaff']['unit_id']))
                                {
                                    $employee['Employee']['unit_code'] = $instaff['InStaff']['unit_id'];
                                }
                                
                                if(!empty($instaff['InStaff']['subunit_id']))
                                {
                                    $employee['Employee']['subunit_code'] = $instaff['InStaff']['subunit_id'];
                                }
            
                                $employee['Employee']['job_grade_id'] = NULL;
            
                                if(!empty($instaff['InStaff']['payscale_group']))
                                {
                                    $jobgrade = $this->JobGrade->findByName($instaff['InStaff']['payscale_group']);
            
                                    if(!empty($jobgrade))
                                    {
                                        $employee['Employee']['job_grade_id'] = $jobgrade['JobGrade']['id'];
                                        $employee['Employee']['job_designation_id'] = $jobgrade['JobGrade']['job_designation_id'];
                                    }
                                }
            
                                $employee['Employee']['payscale_level'] = $instaff['InStaff']['payscale_level'];
                                $employee['Employee']['executed'] = date('Y-m-d H:i:s');
                                

                                // set and validate the employee information 
                                $this->Employee->set($employee);
                                if($this->Employee->validates())
                                {
                                    // insert staff if success insert
                                    $this->Employee->create();
                                    $this->Employee->save($employee);
            
                                    $employee = $this->Employee->read(null,$this->Employee->id);
            
                                    // update instaff if success insert
                                    //enabled user login
                                    $user_id = $current['Personal']['user_id'];

                                    $update = array();
                                    $update['User']['id'] = $user_id;
                                    $update['User']['is_active'] = 1;
                                    $update['User']['modified_by'] = $session['Employee']['id'];
                                    $this->User->create();
                                    $this->User->save($update);

                                    // update instaff if success insert
                                    $instaff['InStaff']['is_flag'] = 1;
                                    $instaff['InStaff']['is_error'] = 99;
                                    $instaff['InStaff']['note'] = 'Enabled employee';
            
                                    $this->InStaff->create();
                                    $this->InStaff->save($instaff);
            
                                    $notification = array();
            
                                    $subject = "Welcome to PRESS ".$employee['Personal']['complete_name'];
                                    $body = "<p style='font-size:12px; font-family:arial;'>There are changes on your employee information. Could I kindly ask you to check and verify.</p>";
                                    $body = "<br/>";
                                    $body .= "Click <a href='".Router::url('/Users/profile' , true)."'>here</a> to view your profile.";
                                    $body = "<br/>";
                                    $body = "<p style='font-size:12px; font-family:arial;'>let ask know if there is any mistake.</p>";
            
                                    $notification = array();
            
                                    //$notification['Notification']['sender_id'] = $staff['Employee']['id'];
                                    $notification['Notification']['recipient_id'] = $employee['Employee']['id'];
                                    $notification['Notification']['is_read'] = 99;
                                    $notification['Notification']['subject'] = $subject;
                                    $notification['Notification']['body'] = $body;
                                    // $notification['Notification']['created_by'] = $staff['Employee']['id'];
                                    // $notification['Notification']['modified_by'] = $staff['Employee']['id'];
            
                                    $this->Notification->create();
                                    $this->Notification->save($notification);
                                }
                                else
                                {
                                    //catch error validation if the information complete
                                    $instaff['InStaff']['is_error'] = 1;
                                    $instaff['InStaff']['note'] = json_encode($this->Employee->validationErrors);
                                    $instaff['InStaff']['executed_by'] = $session['Employee']['id'];
                                    $instaff['InStaff']['executed'] = date('Y-m-d H:i:s');
        
                                    $this->InStaff->create();
                                    $this->InStaff->save($instaff);
                                }
                            }
                        }
                        else
                        {
                            //check if data from SAP say employee status is not active 
                            // update instaff if success insert
                            $instaff['InStaff']['is_flag'] = 1;
                            $instaff['InStaff']['is_error'] = 99;
                            $instaff['InStaff']['note'] = 'Inactive in SAP and also in DB';
    
                            $this->InStaff->create();
                            $this->InStaff->save($instaff);
                        }
                    }
                }
            }
            else
            {
                if($instaff['InStaff']['employment_status'] == 'Active')
                {
                    $employee = array();

                    $employee['Employee']['id'] = NULL;
                    $employee['Employee']['batch_staff_id'] = $detail['BatchStaff']['id'];
                    $employee['Employee']['employee_no'] = $instaff['InStaff']['employee_no'];
                    $employee['Employee']['is_type'] = 1; // 1 for data from SAP, 99 for data from backend
                    $employee['Employee']['is_flag'] = 99; // triger for SAP, if 99 no need to send to SAP
                    $employee['Employee']['is_active'] = 1;
                    $employee['Employee']['status_id'] = 10;

                    $employee['Employee']['personal_area_id'] = NULL;

                    $personalarea = $this->PersonalArea->findByName($instaff['InStaff']['personal_area']);
                    if(!empty($personalarea))
                    {
                        $employee['Employee']['personal_area_id'] = $personalarea['PersonalArea']['id'];
                    }
                    
                    $employee['Employee']['personal_subarea_id'] = NULL;

                    $personalsubarea = $this->PersonalSubarea->find('first',
                                                                            array(
                                                                                'conditions' => array(
                                                                                                    'PersonalSubarea.area' => $personalarea['PersonalArea']['area'],
                                                                                                    'PersonalSubarea.name' => $instaff['InStaff']['personal_subarea'],
                                                                                                ),
                                                                            ));

                    if(!empty($personalsubarea))
                    {
                        $employee['Employee']['personal_subarea_id'] = $personalsubarea['PersonalSubarea']['id'];
                    }
                    
                    $employee['Employee']['employee_group_id'] = NULL;

                    $employeegroup = $this->EmployeeGroup->findByName($instaff['InStaff']['employee_group']);
                    if(!empty($employeegroup))
                    {
                        $employee['Employee']['employee_group_id'] = $employeegroup['EmployeeGroup']['id'];
                    }

                    $employee['Employee']['employee_subgroup_id'] = NULL;

                    $employeesubgroup = $this->EmployeeSubgroup->find('first',
                                                                            array(
                                                                                'conditions' => array(
                                                                                                    'EmployeeSubgroup.EEGrp' => $employeegroup['EmployeeGroup']['code'],
                                                                                                    'EmployeeSubgroup.name' => $instaff['InStaff']['employee_subgroup'],
                                                                                                ),
                                                                            ));
                                                                            
                    if(!empty($employeesubgroup))
                    {
                        $employee['Employee']['employee_subgroup_id'] = $employeesubgroup['EmployeeSubgroup']['id'];
                    }

                    $employee['Employee']['payroll_area_id'] = NULL;

                    $payrollarea = $this->PayrollArea->findByName($instaff['InStaff']['payroll_area']);
                    if(!empty($payrollarea))
                    {
                        $employee['Employee']['payroll_area_id'] = $payrollarea['PayrollArea']['id'];
                    }

                    $employee['Employee']['employment_status_id'] = NULL;

                    $staffstatus = $this->StaffStatus->findByName($instaff['InStaff']['employment_status']);
                    if(!empty($staffstatus))
                    {
                        $employee['Employee']['employment_status_id'] = $staffstatus['StaffStatus']['id'];
                    }

                    $employee['Employee']['entry_date'] = NULL;

                    if(!empty($instaff['InStaff']['entry_date']))
                    {
                        $employee['Employee']['entry_date'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['entry_date']);
                    }

                    if(!empty($instaff['InStaff']['leaving_date']))
                    {
                        $employee['Employee']['leaving_date'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['leaving_date']);
                    }

                    $employee['Employee']['seniority_date'] = NULL;

                    if(!empty($instaff['InStaff']['seniority_date']))
                    {
                        $employee['Employee']['seniority_date'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['seniority_date']);
                    }

                    $employee['Employee']['organizational_unit_id'] = NULL;
                    $employee['Employee']['organizational_unit_code'] = NULL;

                    if(!empty($instaff['InStaff']['organizational_unit_id']))
                    {
                        $organisationunit = $this->Organisation->find('first',
                                                                            array(
                                                                                'conditions' => array(
                                                                                                    'Organisation.code' => $instaff['InStaff']['organizational_unit_id'],
                                                                                                    'Organisation.is_active' => 1,
                                                                                                    'Organisation.is_deleted' => 99,
                                                                                                    'BatchOrganisation.is_active' => 1
                                                                                                ),
                                                                            ));
                        if(!empty($organisationunit))
                        {
                            $employee['Employee']['organizational_unit_id'] = $organisationunit['Organisation']['id'];
                            $employee['Employee']['organizational_unit_code'] = $organisationunit['Organisation']['code'];
                        }
                    }

                    $employee['Employee']['cost_center_id'] = NULL;
                    $employee['Employee']['cost_center_code'] = NULL;

                    if(!empty($instaff['InStaff']['cost_center_code']))
                    {
                        $costcenter = $this->CostCenter->findByCode($instaff['InStaff']['cost_center_code']);

                        if(!empty($costcenter))
                        {
                            $employee['Employee']['cost_center_id'] = $costcenter['CostCenter']['id'];
                            $employee['Employee']['cost_center_code'] = $costcenter['CostCenter']['code'];
                        }
                    }

                    $employee['Employee']['position_id'] = NULL;
                    $employee['Employee']['position_code'] = NULL;

                    if(!empty($instaff['InStaff']['position_id']))
                    {
                        $position = $this->Position->findByCode($instaff['InStaff']['position_id']);

                        if(!empty($position))
                        {
                            $employee['Employee']['position_id'] = $position['Position']['id'];
                            $employee['Employee']['position_code'] = $position['Position']['code'];
                        }
                    }

                    if(!empty($instaff['InStaff']['grp_id']))
                    {
                        $employee['Employee']['group_code'] = $instaff['InStaff']['grp_id'];
                    }

                    if(!empty($instaff['InStaff']['div_id']))
                    {
                        $employee['Employee']['division_code'] = $instaff['InStaff']['div_id'];
                    }

                    if(!empty($instaff['InStaff']['dept_id']))
                    {
                        $employee['Employee']['department_code'] = $instaff['InStaff']['dept_id'];
                    }

                    if(!empty($instaff['InStaff']['sec_id']))
                    {
                        $employee['Employee']['section_code'] = $instaff['InStaff']['sec_id'];
                    }

                    if(!empty($instaff['InStaff']['unit_id']))
                    {
                        $employee['Employee']['unit_code'] = $instaff['InStaff']['unit_id'];
                    }
                    
                    if(!empty($instaff['InStaff']['subunit_id']))
                    {
                        $employee['Employee']['subunit_code'] = $instaff['InStaff']['subunit_id'];
                    }

                    $employee['Employee']['job_grade_id'] = NULL;

                    if(!empty($instaff['InStaff']['payscale_group']))
                    {
                        $jobgrade = $this->JobGrade->findByName($instaff['InStaff']['payscale_group']);

                        if(!empty($jobgrade))
                        {
                            $employee['Employee']['job_grade_id'] = $jobgrade['JobGrade']['id'];
                            $employee['Employee']['job_designation_id'] = $jobgrade['JobGrade']['job_designation_id'];
                        }
                    }

                    $employee['Employee']['payscale_level'] = $instaff['InStaff']['payscale_level'];

                    $this->Employee->set($employee);
                    if($this->Employee->validates())
                    {
                        $password = 'pass@word1';

                        $user = array();

                        $user['User']['username'] = $instaff['InStaff']['employee_no'];
                        $user['User']['password'] = $password;
                        $user['User']['status_id'] = 10;
                        $user['User']['is_active'] = 1;
                        $user['User']['modified_by'] = $session['Employee']['id'];
                        $user['User']['created_by'] = $session['Employee']['id'];

                        //insert user if success validate
                        $this->User->create();
                        $this->User->save($user);

                        $user = $this->User->read(null,$this->User->id);

                        $userrole = array();

                        $userrole['UserRole']['user_id'] = $user['User']['id'];
                        $userrole['UserRole']['role_id'] = 2;

                        // insert user role if success insert
                        $this->UserRole->create();
                        $this->UserRole->save($userrole);

                        $personal = array();
                        $personal['Personal']['complete_name'] = $instaff['InStaff']['complete_name'];
                        $personal['Personal']['ic_no'] = $instaff['InStaff']['ic_no'];
                        $personal['Personal']['batch_staff_id'] = $detail['BatchStaff']['id'];
                        $personal['Personal']['is_type'] = 1;
                        $personal['Personal']['is_flag'] = 1;
                        $personal['Personal']['is_active'] = 1;
                        $personal['Personal']['status_id'] = 10;
                        $personal['Personal']['first_time'] = 1;
                        $personal['Personal']['user_id'] = $user['User']['id'];

                        $personal['Personal']['date_of_birth'] = NULL;
                        if(!empty($instaff['InStaff']['date_of_birth']))
                        {
                            $personal['Personal']['date_of_birth'] = $this->Utility->getDateFormatFromString($instaff['InStaff']['date_of_birth']);
                        }

                        $personal['Personal']['gender_id'] = NULL;
                        if(!empty($instaff['InStaff']['gender']))
                        {
                            $gender = $this->Gender->findByName($instaff['InStaff']['gender']);

                            if(!empty($gender))
                            {
                                $personal['Personal']['gender_id'] = $gender['Gender']['id'];
                            }
                        }

                        $personal['Personal']['ethnic_id'] = NULL;

                        if(!empty($instaff['InStaff']['ethnic']))
                        {
                            $ethnic = $this->Ethnic->findByName($instaff['InStaff']['ethnic']);

                            if(!empty($ethnic))
                            {
                                $personal['Personal']['ethnic_id'] = $ethnic['Ethnic']['id'];
                            }
                        }

                        // insert personal if success insert
                        $this->Personal->create();
                        $this->Personal->save($personal);

                        $personal = $this->Personal->read(null,$this->Personal->id);

                        // insert staff if success insert
                        $employee['Employee']['personal_id'] = $personal['Personal']['id'];

                        $this->Employee->create();
                        $this->Employee->save($employee);

                        $personal_marital = array();
                        $personal_marital['PersonalMarital']['batch_staff_id'] = $detail['BatchStaff']['id'];
                        $personal_marital['PersonalMarital']['personal_id'] = $personal['Personal']['id'];

                        $marital = array();
                        $marital = $this->Marital->findByCode($instaff['InStaff']['marital_status_code']);
                        
                        $personal_marital['PersonalMarital']['marital_id'] = 1;
    
                        if(!empty($marital))
                        {
                            $personal_marital['PersonalMarital']['marital_id'] = $marital['Marital']['id'];
                        }

                        $personal_marital['PersonalMarital']['is_checked'] = 1;
                        $personal_marital['PersonalMarital']['is_type'] = 1;
                        $personal_marital['PersonalMarital']['is_flag'] = 1;
                        $personal_marital['PersonalMarital']['is_active'] = 1;
                        $personal_marital['PersonalMarital']['status_id'] = 10;

                        // insert personal if success insert
                        $this->PersonalMarital->create();
                        $this->PersonalMarital->save($personal_marital);

                        $employee = $this->Employee->read(null,$this->Employee->id);

                        // update instaff if success insert
                        $instaff['InStaff']['is_flag'] = 1;
                        $instaff['InStaff']['is_error'] = 99;
                        $instaff['InStaff']['note'] = 'New employee';

                        $this->InStaff->create();
                        $this->InStaff->save($instaff);

                        $notification = array();

                        $subject = "Welcome to PRESS ".$employee['Personal']['complete_name'];
                        //$body = "<p style='font-size:12px; font-family:arial;'>Here we put some text to read. So assign someone to do this.</p>";
                        $body = "<br/>";
                        $body .= "Click <a href='".Router::url('/Users/profile' , true)."'>here</a> to view your profile.";

                        $notification = array();

                        //$notification['Notification']['sender_id'] = $staff['Employee']['id'];
                        $notification['Notification']['recipient_id'] = $employee['Employee']['id'];
                        $notification['Notification']['is_read'] = 99;
                        $notification['Notification']['subject'] = $subject;
                        $notification['Notification']['body'] = $body;
                        // $notification['Notification']['created_by'] = $staff['Employee']['id'];
                        // $notification['Notification']['modified_by'] = $staff['Employee']['id'];

                        $this->Notification->create();
                        $this->Notification->save($notification);
                    }
                    else
                    {
                        $instaff['InStaff']['is_flag'] = 1;
                        $instaff['InStaff']['is_error'] = 1;
                        $instaff['InStaff']['note'] = json_encode($this->Employee->validationErrors);
                        $instaff['InStaff']['executed_by'] = $session['Employee']['id'];
                        $instaff['InStaff']['executed'] = date('Y-m-d H:i:s');

                        $this->InStaff->create();
                        $this->InStaff->save($instaff);
                    }
                }
            }
        }

        $update = array();
        $update['BatchStaff']['id'] = $detail['BatchStaff']['id'];
        $update['BatchStaff']['executed_end'] = date('Y-m-d H:i:s');

        $this->BatchStaff->create();
        $this->BatchStaff->save($update);

        $check = $this->InStaff->find('count',
                                            array(
                                                'conditions' => array(
                                                                    'InStaff.batch_staff_id' => $detail['BatchStaff']['id'],
                                                                    'InStaff.is_flag' => 99,
                                                                ),
                                            ));

        if($check == 0)
        {
            $update = array();

            $update['BatchStaff']['id'] = $detail['BatchStaff']['id'];
            $update['BatchStaff']['is_flag'] = 1;

            $this->BatchStaff->create();
            $this->BatchStaff->save($update);
        }

        $this->Session->setFlash('Staff information successfully sync', 'success');
        $this->redirect(array('action' => 'error/key:'.$key));

        $this->autoRender = false;
      
    }

    public function push($instaff_id)
    {
        $this->loadModel('InStaff');
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        
        if(empty($instaff_id))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $instaff = $this->InStaff->findById($instaff_id);

        if(empty($instaff))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $batchstaff = $this->BatchStaff->findById($instaff['InStaff']['batch_staff_id']);

        $update = array();
        $update['BatchStaff']['id'] = $batchstaff['BatchStaff']['id'];
        $update['BatchStaff']['executed_end'] = date('Y-m-d H:i:s');

        $this->BatchStaff->create();
        $this->BatchStaff->save($update);

        $check = $this->InStaff->find('count',
                                            array(
                                                'conditions' => array(
                                                                    'InStaff.batch_staff_id' => $detail['BatchStaff']['id'],
                                                                    'InStaff.is_flag' => 99,
                                                                ),
                                            ));

        if($check == 0)
        {
            $update = array();

            $update['BatchStaff']['id'] = $detail['BatchStaff']['id'];
            $update['BatchStaff']['is_flag'] = 1;

            $this->BatchStaff->create();
            $this->BatchStaff->save($update);
        }
    }
    
    public function execute()
    {
        $this->loadModel('InStaff');
        $this->loadModel('Utility');

        $this->layout = false;
        $this->autoRender = false;

        $batchstaffs = $this->BatchStaff->find('all');

        foreach ($batchstaffs as $batchstaff) 
        {
            $instaffs = $this->InStaff->find('all', array(
                                                            'conditions' => array(
                                                                array(
                                                                        'InStaff.batch_staff_id' => $batchstaff['BatchStaff']['id']
                                                                    ),
                                                            )
                                                ));

            foreach ($instaffs as $instaff) 
            {
                $update = array();

                $update['InStaff']['id'] = $instaff['InStaff']['id'];
                $update['InStaff']['is_flag'] = 1;
                $update['InStaff']['is_error'] = 99;

                if(!empty($update['InStaff']['is_error']))
                {
                    $update['InStaff']['is_error'] = 1;
                }

                $word = "{";
                if(strpos($instaff['InStaff']['note'], $word) !== false)
                {
                    $update['InStaff']['is_error'] = 1;
                }
                else
                {
                    $update['InStaff']['is_error'] = 99;
                }

                $this->InStaff->create();
                $this->InStaff->save($update);
            }

            $success = $this->InStaff->find('count', array(
                                                            'conditions' => array(
                                                                array(
                                                                        'InStaff.batch_staff_id' => $batchstaff['BatchStaff']['id'],
                                                                        'InStaff.is_flag' => 1,
                                                                        'InStaff.is_error' => 99,
                                                                    ),
                                                            )
                                                        ));

            $fail = $this->InStaff->find('count', array(
                                                    'conditions' => array(
                                                        array(
                                                                'InStaff.batch_staff_id' => $batchstaff['BatchStaff']['id'],
                                                                'InStaff.is_flag' => 1,
                                                                'InStaff.is_error' => 1,
                                                            ),
                                                    )
                                                ));

            $count = $this->InStaff->find('count', array(
                                                    'conditions' => array(
                                                        array(
                                                                'InStaff.batch_staff_id' => $batchstaff['BatchStaff']['id'],
                                                            ),
                                                    )
                                                ));

            $edit = array();

            $edit['BatchStaff']['id'] = $batchstaff['BatchStaff']['id'];
            $edit['BatchStaff']['success'] = $success;
            $edit['BatchStaff']['fail'] = $fail;
            $edit['BatchStaff']['count'] = $count;
            $edit['BatchStaff']['execute'] = 1;
            $edit['BatchStaff']['note'] = "Executed by cronjob";

            $this->BatchStaff->create();
            $this->BatchStaff->save($edit);

        }

        
        debug('Done!');
        die;
    }
}
