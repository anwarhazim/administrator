<?php
class ModulsController extends AppController 
{
	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter() 
    {
        parent::beforeFilter();
	}
	
	public function index()
	{
        $this->loadModel('Employee');
        $this->loadModel('Project');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);
        
        $conditions = array();

        $conditions['order'] = array('Project.id'=> 'DESC');


        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Project'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters, 
            // we'll redirect to that page
            return $this->redirect($filter_url);
        } 
        else 
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "search")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Project.name LIKE' => '%' . $value . '%')
                        );
                    } 
                    
					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Project.modified) >=' => date("Y-m-d", strtotime($value))
                        );

                    }
					
                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Project.modified) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc                 
                    $this->request->data['Project'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate('Project');

        for ($i=0; $i < count($details); $i++) 
        { 
            $details[$i]['Project']['modified'] = date("d-m-Y",strtotime($details[$i]['Project']['modified']));

            $details[$i]['Project']['created'] = date("d-m-Y",strtotime($details[$i]['Project']['created']));

            $details[$i]['Project']['key'] = $this->Utility->encrypt($details[$i]['Project']['id'], 'pRoJeCt');
        }

        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // view
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '2'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['created'] = date('Y-m-d H:i:s');
        $logs['Log']['modified_by'] = $employee['Employee']['id'];
        $logs['Log']['modified'] = date('Y-m-d H:i:s');

        $this->Log->create();
        $this->Log->save($logs);

        $this->set(compact('details'));
    }


    public function listing($key = null)
    {
        $this->loadModel('Personal');
        $this->loadModel('Employee');
        $this->loadModel('Project');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);
        
        $filter_url['action'] = $this->request->params['action'];

        $key = "";

        if(!empty($this->params['named']['key']))
        {
            $key = $this->params['named']['key'];
        }

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        $id = $this->Utility->decrypt($key, 'pRoJeCt');

        $detail = $this->Project->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        $conditions = array();

        $conditions['conditions'][] = array(
                                            'Modul.project_id' => $detail['Project']['id'],
                                        );

        $conditions['order'] = array('Modul.order'=> 'ASC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Modul'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters, 
            // we'll redirect to that page
            return $this->redirect($filter_url);
        } 
        else 
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "search")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('Modul.name LIKE' => '%' . $value . '%')
                        );
                    } 
                    
					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Modul.modified) >=' => date("Y-m-d", strtotime($value))
                        );

                    }
					
                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(Modul.modified) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc                 
                    $this->request->data['Modul'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate();

        for ($i=0; $i < count($details); $i++) 
        {
            if(!empty($details[$i]['Modul']['modified']))
            {
                $details[$i]['Modul']['modified'] = date("d-m-Y",strtotime($details[$i]['Modul']['modified']));
            }
            else
            {
                $details[$i]['Modul']['modified'] = '-';
            }

            $details[$i]['Modul']['id'] = $this->Utility->encrypt($details[$i]['Modul']['id'], 'm0Del');
        }

        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // view
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '2'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['created'] = date('Y-m-d H:i:s');
        $logs['Log']['modified_by'] = $employee['Employee']['id'];
        $logs['Log']['modified'] = date('Y-m-d H:i:s');

        $this->Log->create();
        $this->Log->save($logs);

        $is_active = array(
            1=>'Active',
            99=>'inactive'
        );

        $this->set(compact('key', 'detail', 'details', 'is_active'));
    }

    public function add($key = null)
    {
        $this->loadModel('Personal');
        $this->loadModel('Employee');
        $this->loadModel('Project');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        $id = $this->Utility->decrypt($key, 'pRoJeCt');

        $detail = $this->Project->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $data['Modul']['project_id'] = $detail['Project']['id'];
            $data['Modul']['is_active'] = 1;
            $this->Modul->create();
            if($this->Modul->save($data))
            {
                $logs = array();
                $logs['Log']['employee_id'] = $employee['Employee']['id'];
                $logs['Log']['action_id'] = '3'; // add
                $logs['Log']['path'] = $this->here; //get current path
                $logs['Log']['project_id'] = '2'; //set project id
                $logs['Log']['created_by'] = $employee['Employee']['id'];
                $logs['Log']['created'] = date('Y-m-d H:i:s');
                $logs['Log']['modified_by'] = $employee['Employee']['id'];
                $logs['Log']['modified'] = date('Y-m-d H:i:s');

                $this->Log->create();
                $this->Log->save($logs);

                $this->Session->setFlash('Information successfully saved.', 'success');
                $this->redirect(array('action' => 'add/'.$key));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully saved.', 'error');
            }
        }

        $is_nav = array(
                    1=>'YES',
                    99=>'NO'
                );

        $is_full = array(
                    1=>'YES',
                    99=>'NO'
                );

        $is_type = array(
                    1=>'HEADER',
                    99=>'MENU'
                );

        $is_dropdown = array(
                    1=>'YES',
                    99=>'NO'
                );

        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // view
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '2'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['created'] = date('Y-m-d H:i:s');
        $logs['Log']['modified_by'] = $employee['Employee']['id'];
        $logs['Log']['modified'] = date('Y-m-d H:i:s');

        $this->Log->create();
        $this->Log->save($logs);

        $this->set(compact('key', 'is_nav', 'is_full', 'is_type', 'is_dropdown'));
    }

    public function view($project_key = null, $modul_key = null)
    {
        $this->loadModel('Personal');
        $this->loadModel('Employee');
        $this->loadModel('Project');
        $this->loadModel('Modul');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        if(empty($project_key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        $id = $this->Utility->decrypt($project_key, 'pRoJeCt');

        $project = $this->Project->findById($id);

        if(empty($project))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        if(empty($modul_key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        $id = $this->Utility->decrypt($modul_key, 'm0Del');

        $modul = $this->Modul->findById($id);

        if(empty($modul))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        $modul['Modul']['modified'] = date("d-m-Y",strtotime($modul['Modul']['modified']));

        $modul['Modul']['created'] = date("d-m-Y",strtotime($modul['Modul']['created']));

        $this->request->data = $modul;

        $is_nav = array(
                    1=>'YES',
                    99=>'NO'
                );

        $is_full = array(
                    1=>'YES',
                    99=>'NO'
                );

        $is_type = array(
                    1=>'HEADER',
                    99=>'MENU'
                );

        $is_dropdown = array(
                    1=>'YES',
                    99=>'NO'
                );

        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // view
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '2'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['created'] = date('Y-m-d H:i:s');
        $logs['Log']['modified_by'] = $employee['Employee']['id'];
        $logs['Log']['modified'] = date('Y-m-d H:i:s');

        $this->Log->create();
        $this->Log->save($logs);

        $this->set(compact('project_key', 'modul_key', 'project', 'modul', 'is_nav', 'is_full', 'is_type', 'is_dropdown'));
    }

    public function edit($project_key = null, $modul_key = null)
    {
        $this->loadModel('Personal');
        $this->loadModel('Employee');
        $this->loadModel('Project');
        $this->loadModel('Modul');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        if(empty($project_key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        $id = $this->Utility->decrypt($project_key, 'pRoJeCt');

        $project = $this->Project->findById($id);

        if(empty($project))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        if(empty($modul_key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        $id = $this->Utility->decrypt($modul_key, 'm0Del');

        $modul = $this->Modul->findById($id);

        if(empty($modul))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $data['Modul']['id'] = $modul['Modul']['id'];
            $data['Modul']['project_id'] = $project['Project']['id'];
            $data['Modul']['is_active'] = 1;

            $this->Modul->create();
            if($this->Modul->save($data))
            {
                $logs = array();
                $logs['Log']['employee_id'] = $employee['Employee']['id'];
                $logs['Log']['action_id'] = '4'; // edit
                $logs['Log']['path'] = $this->here; //get current path
                $logs['Log']['project_id'] = '2'; //set project id
                $logs['Log']['created_by'] = $employee['Employee']['id'];
                $logs['Log']['created'] = date('Y-m-d H:i:s');
                $logs['Log']['modified_by'] = $employee['Employee']['id'];
                $logs['Log']['modified'] = date('Y-m-d H:i:s');

                $this->Log->create();
                $this->Log->save($logs);

                $this->Session->setFlash('Information successfully saved.', 'success');
                $this->redirect(array('action' => 'edit/'.$project_key.'/'.$modul_key));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully saved.', 'error');
            }

        }

        $modul['Modul']['modified'] = date("d-m-Y",strtotime($modul['Modul']['modified']));

        $modul['Modul']['created'] = date("d-m-Y",strtotime($modul['Modul']['created']));

        $this->request->data = $modul;

        $is_nav = array(
                    1=>'YES',
                    99=>'NO'
                );

        $is_full = array(
                    1=>'YES',
                    99=>'NO'
                );

        $is_type = array(
                    1=>'HEADER',
                    99=>'MENU'
                );

        $is_dropdown = array(
                    1=>'YES',
                    99=>'NO'
                );

        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // view
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '2'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['created'] = date('Y-m-d H:i:s');
        $logs['Log']['modified_by'] = $employee['Employee']['id'];
        $logs['Log']['modified'] = date('Y-m-d H:i:s');

        $this->Log->create();
        $this->Log->save($logs);

        $this->set(compact('project_key', 'modul_key', 'project', 'modul', 'is_nav', 'is_full', 'is_type', 'is_dropdown'));
    }

    public function coordination($key = null)
    {
        $this->loadModel('Personal');
        $this->loadModel('Employee');
        $this->loadModel('Project');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);
        
        $filter_url['action'] = $this->request->params['action'];

        $key = "";

        if(!empty($this->params['named']['key']))
        {
            $key = $this->params['named']['key'];
        }

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        $id = $this->Utility->decrypt($key, 'pRoJeCt');

        $detail = $this->Project->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect(array('action' => '/'));
        }

        if ($this->request->is('post') || $this->request->is('put')) 
        {
            $data = $this->request->data;
            $this->Utility->getNestableSave($data, count($data), 'Modul', '', 1);

            $logs = array();
            $logs['Log']['employee_id'] = $employee['Employee']['id'];
            $logs['Log']['action_id'] = '4'; // edit
            $logs['Log']['path'] = $this->here; //get current path
            $logs['Log']['project_id'] = '2'; //set project id
            $logs['Log']['created_by'] = $employee['Employee']['id'];
            $logs['Log']['created'] = date('Y-m-d H:i:s');
            $logs['Log']['modified_by'] = $employee['Employee']['id'];
            $logs['Log']['modified'] = date('Y-m-d H:i:s');

            $this->Log->create();
            $this->Log->save($logs);
        }

        $moduls = $this->Modul->find('threaded',
                                            array(
                                                'conditions' => array(
                                                                    'Modul.is_active' => 1,
                                                                    'Modul.project_id' => $detail['Project']['id']
                                                                ),
                                                'contain' => false,
                                                'order' => array('Modul.order ASC'),
                                        ));
		
        $nestable = "";
      
        $nestable = $this->Utility->getNestable($moduls, count($moduls), 'Modul', "", 'name');

        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // view
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '2'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['created'] = date('Y-m-d H:i:s');
        $logs['Log']['modified_by'] = $employee['Employee']['id'];
        $logs['Log']['modified'] = date('Y-m-d H:i:s');

        $this->Log->create();
        $this->Log->save($logs);
    
        $this->set(compact('nestable'));
    }
}