<?php
class InstitutionsController extends AppController 
{

	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter() 
    {
        parent::beforeFilter();
    }
    
    public function upload()
    {
        $this->loadModel('Personal');
        $this->loadModel('Employee');
        $this->loadModel('Institution');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        if($this->request->is('post') || $this->request->is('put'))
        {
            App::import('Vendor', 'Spreadsheet_Excel_Reader', array('file' => 'excelreader'.DS.'excel_reader.php'));
            $excel = new PhpExcelReader; 

            $data = $this->request->data;

            if($data['Institution']['attachment']['error'] == 0)
            {
                $excel->read($data['Institution']['attachment']['tmp_name']);
                $cells = $excel->sheets[0]['cells'];
                $counter = 0;
                for ($i=2; $i <= count($cells); $i++) 
                {
                    $code = isset($cells[$i][1]) ? $cells[$i][1] : '';
                    $name = isset($cells[$i][2]) ? $cells[$i][2] : '';
                    $state = isset($cells[$i][3]) ? $cells[$i][3] : '';
                    $category = isset($cells[$i][4]) ? $cells[$i][4] : '';
                    $type = isset($cells[$i][5]) ? $cells[$i][5] : '';
                    
                    $institution = array();
                    $institution['Institution']['name'] = $name;
                    $institution['Institution']['code'] = $code;
                    $institution['Institution']['state'] = $state;
                    $institution['Institution']['category'] = $category;
                    $institution['Institution']['type'] = $type;
                    $institution['Institution']['created_by'] = $employee['Employee']['id'];
                    $institution['Institution']['created'] = date('Y-m-d H:i:s');
                    $institution['Institution']['modified_by'] = $employee['Employee']['id'];
                    $institution['Institution']['modified'] = date('Y-m-d H:i:s');

                    $this->Institution->create();
                    $this->Institution->save($institution);
                }


                $logs = array();
                $logs['Log']['employee_id'] = $employee['Employee']['id'];
                $logs['Log']['action_id'] = '3'; // add
                $logs['Log']['path'] = $this->here; //get current path
                $logs['Log']['project_id'] = '2'; //set project id
                $logs['Log']['created_by'] = $employee['Employee']['id'];
                $logs['Log']['created'] = date('Y-m-d H:i:s');
                $logs['Log']['modified_by'] = $employee['Employee']['id'];
                $logs['Log']['modified'] = date('Y-m-d H:i:s');

                $this->Log->create();
                $this->Log->save($logs);

                $this->Session->setFlash('Information successfully upload.', 'success');
                $this->redirect(array('action' => 'upload'));
            }
        }
    }
}