<?php
App::uses('AppController', 'Controller');

class CostCentersController extends AppController 
{
	public $components = array('RequestHandler', 'Paginator', 'Session');
	public $helpers = array('Html', 'Form', 'Session');
	
	public function index()
	{
        $this->loadModel('Employee');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        $conditions = array();

        $conditions['order'] = array('CostCenter.id'=> 'ASC');

        //Transform POST into GET
        if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;;

            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            // We need to overwrite the page every time we change the parameters
            $filter_url['page'] = 1;

            // for each filter we will add a GET parameter for the generated url
            foreach($data['Role'] as $name => $value)
            {
                if($value)
                {
                    // You might want to sanitize the $value here
                    // or even do a urlencode to be sure
                    $filter_url[$name] = $value;
                }
            }
            // now that we have generated an url with GET parameters,
            // we'll redirect to that page
            return $this->redirect($filter_url);
        }
        else
        {
            // Inspect all the named parameters to apply the filters
            foreach($this->params['named'] as $param_name => $value)
            {
                // Don't apply the default named parameters used for pagination
                if(!in_array($param_name, array('page','sort','direction','limit')))
                {
                    if($param_name == "search")
                    {
                        $conditions['conditions']['OR'][] = array(
                            array('CostCenter.name LIKE' => '%' . $value . '%')
                        );

                        $conditions['conditions']['OR'][] = array(
                            array('CostCenter.code LIKE' => '%' . $value . '%')
                        );

                        $conditions['conditions']['OR'][] = array(
                            array('CostCenter.description LIKE' => '%' . $value . '%')
                        );
                    }

					if($param_name == "start_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(CostCenter.modified) >=' => date("Y-m-d", strtotime($value))
                        );

                    }

                    if($param_name == "end_date")
                    {
                        $conditions['conditions'][] = array(
                            'date(CostCenter.modified) <=' => date("Y-m-d", strtotime($value))
                        );
                    }

                    // You may use a switch here to make special filters
                    // like "between dates", "greater than", etc
                    $this->request->data['CostCenter'][$param_name] = $value;
                }
            }
        }

        $this->Paginator->settings = $conditions;

        $details = $this->Paginator->paginate();

        for ($i=0; $i < count($details); $i++)
        {
            $details[$i]['CostCenter']['modified'] = date("d-m-Y",strtotime($details[$i]['CostCenter']['modified']));

            $details[$i]['CostCenter']['created'] = date("d-m-Y",strtotime($details[$i]['CostCenter']['created']));

            $details[$i]['CostCenter']['key'] = $this->Utility->encrypt($details[$i]['CostCenter']['id'], 'CostCenter');
        }

        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // view
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '2'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['created'] = date('Y-m-d H:i:s');
        $logs['Log']['modified_by'] = $employee['Employee']['id'];
        $logs['Log']['modified'] = date('Y-m-d H:i:s');

        $this->Log->create();
        $this->Log->save($logs);

        $this->set(compact('details'));
	}

	public function add()
	{
        $this->loadModel('Employee');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);
        
		if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

			$this->CostCenter->set($data);
            if($this->CostCenter->validates())
            {
				$data['CostCenter']['modified_by'] = $employee['Employee']['id'];
				$data['CostCenter']['created_by'] = $employee['Employee']['id'];

                $this->CostCenter->create();
                $this->CostCenter->save($data);
                
                $logs = array();
                $logs['Log']['employee_id'] = $employee['Employee']['id'];
                $logs['Log']['action_id'] = '3'; // add
                $logs['Log']['path'] = $this->here; //get current path
                $logs['Log']['project_id'] = '2'; //set project id
                $logs['Log']['created_by'] = $employee['Employee']['id'];
                $logs['Log']['created'] = date('Y-m-d H:i:s');
                $logs['Log']['modified_by'] = $employee['Employee']['id'];
                $logs['Log']['modified'] = date('Y-m-d H:i:s');

                $this->Log->create();
                $this->Log->save($logs);

                $this->Session->setFlash('Information successfully saved.', 'success');
                $this->redirect(array('action' => 'add'));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully saved.', 'error');
            }
        }

        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // view
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '2'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['created'] = date('Y-m-d H:i:s');
        $logs['Log']['modified_by'] = $employee['Employee']['id'];
        $logs['Log']['modified'] = date('Y-m-d H:i:s');

        $this->Log->create();
        $this->Log->save($logs);

	}

	public function view($key = null)
	{
        $this->loadModel('Employee');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'rOlE@');

        $detail = $this->CostCenter->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

		$detail['CostCenter']['modified'] = date("d-m-Y",strtotime($detail['CostCenter']['modified']));
		$detail['CostCenter']['created'] = date("d-m-Y",strtotime($detail['CostCenter']['created']));

		$this->request->data = $detail;
		
        $disabled = "disabled";
        
        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // view
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '2'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['created'] = date('Y-m-d H:i:s');
        $logs['Log']['modified_by'] = $employee['Employee']['id'];
        $logs['Log']['modified'] = date('Y-m-d H:i:s');

        $this->Log->create();
        $this->Log->save($logs);

        $this->set(compact('key', 'disabled'));
	}

	public function edit($key = null)
	{
		$this->loadModel('Employee');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        if(empty($key))
        {
            $this->Session->setFlash('Invalid input. Please contact system administrator for help.', 'error');
            $this->redirect('/');
        }

        $id = $this->Utility->decrypt($key, 'CostCenter');

        $detail = $this->CostCenter->findById($id);

        if(empty($detail))
        {
            $this->Session->setFlash('We cannot find any in our record. Please contact system administrator for help.', 'error');
            $this->redirect('/');
		}

		if($this->request->is('post') || $this->request->is('put'))
        {
            $data = $this->request->data;

            $data['CostCenter']['id'] = $id;

			$this->CostCenter->set($data);
            if($this->CostCenter->validates())
            {
				$data['CostCenter']['modified_by'] = $employee['Employee']['id'];

                $this->CostCenter->create();
                $this->CostCenter->save($data);
                
                $logs = array();
                $logs['Log']['employee_id'] = $employee['Employee']['id'];
                $logs['Log']['action_id'] = '4'; // edit
                $logs['Log']['path'] = $this->here; //get current path
                $logs['Log']['project_id'] = '2'; //set project id
                $logs['Log']['created_by'] = $employee['Employee']['id'];
                $logs['Log']['created'] = date('Y-m-d H:i:s');
                $logs['Log']['modified_by'] = $employee['Employee']['id'];
                $logs['Log']['modified'] = date('Y-m-d H:i:s');

                $this->Log->create();
                $this->Log->save($logs);

                $this->Session->setFlash('Information successfully updated.', 'success');
                $this->redirect(array('action' => 'edit/'.$key));
            }
            else
            {
                $this->Session->setFlash('Error! Information not successfully updated.', 'error');
            }
        }
        else
        {
            $this->request->data = $detail;
        }
		
        $disabled = "";
        
        $logs = array();
        $logs['Log']['employee_id'] = $employee['Employee']['id'];
        $logs['Log']['action_id'] = '2'; // view
        $logs['Log']['path'] = $this->here; //get current path
        $logs['Log']['project_id'] = '2'; //set project id
        $logs['Log']['created_by'] = $employee['Employee']['id'];
        $logs['Log']['created'] = date('Y-m-d H:i:s');
        $logs['Log']['modified_by'] = $employee['Employee']['id'];
        $logs['Log']['modified'] = date('Y-m-d H:i:s');

        $this->Log->create();
        $this->Log->save($logs);

        $this->set(compact('key', 'disabled'));
		
    }
    
    public function upload()
    {
        $this->loadModel('Employee');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);
        
        if($this->request->is('post') || $this->request->is('put'))
        {
            App::import('Vendor', 'Spreadsheet_Excel_Reader', array('file' => 'excelreader'.DS.'excel_reader.php'));
            $excel = new PhpExcelReader; 

            $data = $this->request->data;

            if($data['CostCenter']['attachment']['error'] == 0)
            {
                switch ($data['CostCenter']['type_id']) 
                {
                    case 1:
                        //MS Excel 2007
                        $excel->read($data['CostCenter']['attachment']['tmp_name']);
                        $cells = $excel->sheets[0]['cells'];
                        $counter = 0;
                        for ($i=2; $i <= count($cells); $i++) 
                        {
                            $code = isset($cells[$i][1]) ? $cells[$i][1] : '';
                            $company_code = isset($cells[$i][2]) ? $cells[$i][2] : '';
                            $name = isset($cells[$i][3]) ? $cells[$i][3] : '';
                            $description = isset($cells[$i][4]) ? $cells[$i][4] : '';


                            $costcenter = array();

                            $costcenter = $this->CostCenter->find('first',
                                                                        array(
                                                                            'conditions' => array(
                                                                                                'CostCenter.code' => $code,
                                                                                            ),
                                                                        ));

                            if(!empty($costcenter))
                            {
                                $value = array();

                                $value['CostCenter']['id'] = $costcenter['CostCenter']['id'];
                                $value['CostCenter']['company_code'] = $company_code;
                                $value['CostCenter']['name'] = $name;
                                $value['CostCenter']['description'] = $description;

                                $this->CostCenter->create();
                                $this->CostCenter->save($value);
                            }
                            else
                            {
                                $value = array();

                                $value['CostCenter']['code'] = $code;
                                $value['CostCenter']['company_code'] = $company_code;
                                $value['CostCenter']['name'] = $name;
                                $value['CostCenter']['description'] = $description;
                                $value['CostCenter']['created_by'] = $employee['Employee']['id'];
                                $value['CostCenter']['modified_by'] = $employee['Employee']['id'];

                                $this->CostCenter->create();
                                $this->CostCenter->save($value);
                            }
                        }

                        $this->Session->setFlash('Information successfully upload.', 'success');
                        $this->redirect(array('action' => 'upload'));
                        break;
                }
                
            }
        }

        $types = array(1 => 'Excel 97 - 2013 Workbook', 2 => 'DAT File');

        $this->set(compact('types'));
    }
}
