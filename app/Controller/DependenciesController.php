<?php
class DependenciesController extends AppController 
{

	public $components = array('RequestHandler', 'Paginator', 'Session');
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter() 
    {
        parent::beforeFilter();
    }
    
    public function upload()
    {
        $this->loadModel('Personal');
        $this->loadModel('Employee');
        $this->loadModel('Children');
        $this->loadModel('Spouse');
        $this->loadModel('Medical');
        $this->loadModel('FamilyPass');
        $this->loadModel('Log');
        $this->loadModel('Utility');

        ini_set('memory_limit', '256M');
        ini_set('max_execution_time', '0');

        $person = $this->Auth->user();
        $employee = $this->Utility->getUserInformation($person['id']);

        if($this->request->is('post') || $this->request->is('put'))
        {
            App::import('Vendor', 'Spreadsheet_Excel_Reader', array('file' => 'excelreader'.DS.'excel_reader.php'));
            $excel = new PhpExcelReader; 

            $data = $this->request->data;

            if($data['Spouse']['attachment']['error'] == 0)
            {
                $file = file_get_contents($data['Spouse']['attachment']["tmp_name"], true);
                
                $files = explode("\n", $file);
                $counter = 0;

                for ($i=1; $i < count($files) ; $i++) 
                {
                    // $temp = array();
                    // $temp['InStaff']['batch_staff_id'] = $detail['BatchStaff']['id'];
                    // $temp['InStaff']['is_flag'] = 99;

                    // if(!empty($files[$i]))
                    // {
                    //     $file_contents = explode("|", $files[$i]);     
                    //     for ($j=0; $j < count($file_contents) ; $j++) 
                    //     {
                    //         if(!empty($file_contents[$j]))
                    //         { 
                    //             switch ($j) 
                    //             {
                    //                 case 8:
                    //                     // debug($file_contents);
                    //                     // debug($file_contents[$j]);
                    //                     // die;
                    //                     break;
                    //             }
                    //         }
                    //     }
                    // }

                    $employee_no = isset($cells[$i][9]) ? $cells[$i][9] : '';
                    $type_id = isset($cells[$i][23]) ? $cells[$i][23] : '';
                    $dob = isset($cells[$i][24]) ? $cells[$i][24] : '';
                    $sex = isset($cells[$i][27]) ? $cells[$i][27] : '';
                    $cname = isset($cells[$i][35]) ? $cells[$i][35] : '';
                    $ic_no = isset($cells[$i][62]) ? $cells[$i][62] : '';

                    $employee_information = $this->Employee->find('first',
                                                array(
                                                    'conditions' => array(
                                                                        'Employee.employee_no' => $employee_no,
                                                                        'Employee.is_active' => 1,
                                                                    ),
                                                ));

                    if(!empty($employee_information))
                    {
                        switch ($type_id) 
                        {
                            case 1:
                                
                                $tmp = array();

                                $spouse = $this->Spouse->find('first',
                                                    array(
                                                        'conditions' => array(
                                                                            'Spouse.personal_id' => $employee_information['Employee']['personal_id'],
                                                                            'Spouse.ic_no' => $ic_no,
                                                                            'Spouse.is_active' => 1,
                                                                        ),
                                                    ));
                                if(!empty($spouse))
                                {
                                    $tmp['Spouse']['id'] = $spouse['Spouse']['id'];
                                }

                                $tmp['Spouse']['personal_id'] = $employee_information['Employee']['personal_id'];
                                $tmp['Spouse']['is_checked'] = 99;
                                $tmp['Spouse']['is_deleted'] = 99;
                                $tmp['Spouse']['is_type'] = 1;
                                $tmp['Spouse']['is_flag'] = 1;
                                $tmp['Spouse']['is_active'] = 1;
                                $tmp['Spouse']['status_id'] = 10;

                                if(!empty($cname))
                                {
                                    $tmp['Spouse']['name'] = $cname;
                                }

                                if(!empty($ic_no))
                                {
                                    $tmp['Spouse']['ic_no'] = $ic_no;
                                }

                                if(!empty($dob))
                                {
                                    $dob = $this->Utility->getDateFormatFromString($dob);

                                    $tmp['Spouse']['date_of_birth'] = date("Y-m-d", strtotime($dob));
                                }

                                if(!empty($sex))
                                {
                                    $tmp['Spouse']['gender_id'] = $sex;
                                }

                                $tmp['Spouse']['created'] = date('Y-m-d H:i:s');
                                $tmp['Spouse']['modified'] = date('Y-m-d H:i:s');

                                $this->Spouse->set($tmp);
                                if($this->Spouse->validates())
                                {
                                    $this->Spouse->create();
                                    $this->Spouse->save($tmp);

                                }

                                break;
                            
                            case 2:
                                
                                $tmp = array();

                                $children = $this->Children->find('first',
                                                    array(
                                                        'conditions' => array(
                                                                            'Children.personal_id' => $employee_information['Employee']['personal_id'],
                                                                            'Children.ic_no' => $ic_no,
                                                                            'Children.is_active' => 1,
                                                                        ),
                                                    ));
                                if(!empty($children))
                                {
                                    $tmp['Children']['id'] = $children['Children']['id'];
                                }

                                $tmp['Children']['personal_id'] = $employee_information['Employee']['personal_id'];
                                $tmp['Children']['is_checked'] = 99;
                                $tmp['Children']['is_deleted'] = 99;
                                $tmp['Children']['is_type'] = 1;
                                $tmp['Children']['is_flag'] = 1;
                                $tmp['Children']['is_active'] = 1;
                                $tmp['Children']['status_id'] = 10;

                                if(!empty($cname))
                                {
                                    $tmp['Children']['name'] = $cname;
                                }

                                if(!empty($ic_no))
                                {
                                    $tmp['Children']['ic_no'] = $ic_no;
                                }

                                if(!empty($dob))
                                {
                                    $dob = $this->Utility->getDateFormatFromString($dob);

                                    $tmp['Children']['date_of_birth'] = date("Y-m-d", strtotime($dob));
                                }

                                if(!empty($sex))
                                {
                                    $tmp['Children']['gender_id'] = $sex;
                                }

                                $tmp['Children']['created'] = date('Y-m-d H:i:s');
                                $tmp['Children']['modified'] = date('Y-m-d H:i:s');

                                $this->Children->set($tmp);
                                if($this->Children->validates())
                                {
                                    $this->Children->create();
                                    $this->Children->save($tmp);

                                }

                                break;
                        }
                    }
                }


                $logs = array();
                $logs['Log']['employee_id'] = $employee['Employee']['id'];
                $logs['Log']['action_id'] = '3'; // add
                $logs['Log']['path'] = $this->here; //get current path
                $logs['Log']['project_id'] = '2'; //set project id
                $logs['Log']['created_by'] = $employee['Employee']['id'];
                $logs['Log']['created'] = date('Y-m-d H:i:s');
                $logs['Log']['modified_by'] = $employee['Employee']['id'];
                $logs['Log']['modified'] = date('Y-m-d H:i:s');

                $this->Log->create();
                $this->Log->save($logs);

                $this->Session->setFlash('Information successfully upload.', 'success');
                $this->redirect(array('action' => 'upload'));
            }
        }
    }
}